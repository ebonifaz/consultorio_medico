-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.19 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando estructura para tabla proy_clinica_escuelita.antecedentes
CREATE TABLE IF NOT EXISTS `antecedentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_antecedente_categoria` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_antecedentes_antecedente_categoria` (`id_antecedente_categoria`),
  CONSTRAINT `FK_antecedentes_antecedente_categoria` FOREIGN KEY (`id_antecedente_categoria`) REFERENCES `antecedente_categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.antecedentes: ~14 rows (aproximadamente)
/*!40000 ALTER TABLE `antecedentes` DISABLE KEYS */;
INSERT INTO `antecedentes` (`id`, `nombre`, `id_antecedente_categoria`) VALUES
	(1, 'Alergias', 1),
	(2, 'Alertas de riesgo', 1),
	(3, 'Crecimiento y desarrollo', 1),
	(4, 'Diabetes', 1),
	(5, 'Enfermedad Renal', 1),
	(6, 'Farmacos', 1),
	(7, 'Alertas de riesgo', 2),
	(8, 'Crecimiento y desarrollo', 2),
	(9, 'Diabetes', 2),
	(10, 'Enfermedad Renal', 2),
	(11, 'Farmacos', 2),
	(12, 'Desarrollo', 3),
	(13, 'Generales', 3),
	(14, 'Métodos Anticonceptivos', 3),
	(15, 'Sexualidad', 3);
/*!40000 ALTER TABLE `antecedentes` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.antecedente_categoria
CREATE TABLE IF NOT EXISTS `antecedente_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.antecedente_categoria: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `antecedente_categoria` DISABLE KEYS */;
INSERT INTO `antecedente_categoria` (`id`, `nombre`) VALUES
	(1, 'Personales'),
	(2, 'Familiares'),
	(3, 'Andropológico'),
	(4, 'Vacunación');
/*!40000 ALTER TABLE `antecedente_categoria` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.cita
CREATE TABLE IF NOT EXISTS `cita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_solicitada` timestamp NOT NULL,
  `id_paciente` int(11) NOT NULL,
  `id_doctor` int(11) NOT NULL,
  `id_estado_cita` int(11) NOT NULL,
  `observacion` text COLLATE utf8_unicode_ci,
  `id_historia_clinica` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cita_usuarios_pacientes` (`id_paciente`),
  KEY `FK_cita_usuarios_doctor` (`id_doctor`),
  KEY `FK_cita_estado_cita` (`id_estado_cita`),
  KEY `FK_cita_historiaclinica` (`id_historia_clinica`),
  CONSTRAINT `FK_cita_estado_cita` FOREIGN KEY (`id_estado_cita`) REFERENCES `estado_cita` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cita_historiaclinica` FOREIGN KEY (`id_historia_clinica`) REFERENCES `historiaclinica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cita_usuarios_doctor` FOREIGN KEY (`id_doctor`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_cita_usuarios_pacientes` FOREIGN KEY (`id_paciente`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.cita: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `cita` DISABLE KEYS */;
INSERT INTO `cita` (`id`, `hora_solicitada`, `id_paciente`, `id_doctor`, `id_estado_cita`, `observacion`, `id_historia_clinica`) VALUES
	(2, '2021-04-17 09:00:00', 17, 3, 4, 'Se cancelo por problemas personales', NULL),
	(6, '2021-04-17 10:00:00', 15, 3, 4, NULL, NULL),
	(7, '2021-04-17 11:00:00', 13, 3, 4, NULL, NULL),
	(8, '2021-04-17 12:00:00', 12, 3, 4, NULL, NULL),
	(9, '2021-04-17 13:00:00', 17, 3, 4, NULL, NULL),
	(10, '2021-04-17 09:00:00', 17, 3, 4, NULL, NULL),
	(11, '2021-04-17 10:00:00', 21, 2, 3, '123', NULL),
	(13, '2021-04-17 22:04:02', 10, 3, 3, '', NULL);
/*!40000 ALTER TABLE `cita` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.condicion_diagnostico
CREATE TABLE IF NOT EXISTS `condicion_diagnostico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.condicion_diagnostico: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `condicion_diagnostico` DISABLE KEYS */;
INSERT INTO `condicion_diagnostico` (`id`, `nombre`) VALUES
	(2, 'Enrique Condicion Diagnostico ');
/*!40000 ALTER TABLE `condicion_diagnostico` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.cronologia
CREATE TABLE IF NOT EXISTS `cronologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.cronologia: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cronologia` DISABLE KEYS */;
INSERT INTO `cronologia` (`id`, `nombre`) VALUES
	(2, 'Cronología Enrique');
/*!40000 ALTER TABLE `cronologia` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.diagnosticos
CREATE TABLE IF NOT EXISTS `diagnosticos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo_cie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.diagnosticos: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `diagnosticos` DISABLE KEYS */;
INSERT INTO `diagnosticos` (`id`, `nombre`, `codigo_cie`) VALUES
	(16, 'Fiebre del dengue', 90),
	(17, 'Enfermedad por virus Zika', 925),
	(18, 'Neumocistosis', 485),
	(19, 'Trombocitemia', 473),
	(20, 'Sindrome del colon irritable con diarrea', 581),
	(21, 'Espondilitis anquilosante', 45),
	(22, 'Muerte Materna', 93),
	(23, 'Contacto traumatico con cuchillo', 260),
	(24, 'COVID-19', 71),
	(25, 'Aberturas Artificiales', 938),
	(26, 'Organos Y Tejidos Trasplantados', 942),
	(27, 'Resistencia a otros antibióticos', 838);
/*!40000 ALTER TABLE `diagnosticos` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.estado_cita
CREATE TABLE IF NOT EXISTS `estado_cita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.estado_cita: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `estado_cita` DISABLE KEYS */;
INSERT INTO `estado_cita` (`id`, `nombre`, `tipo`) VALUES
	(1, 'Suspendida', 'warning'),
	(2, 'Atendido', 'success'),
	(3, 'Agendada', 'primary'),
	(4, 'Cancelada', 'danger');
/*!40000 ALTER TABLE `estado_cita` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.grupo_prioritario
CREATE TABLE IF NOT EXISTS `grupo_prioritario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.grupo_prioritario: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `grupo_prioritario` DISABLE KEYS */;
INSERT INTO `grupo_prioritario` (`id`, `nombre`) VALUES
	(2, 'Grupo Prioritario 1 '),
	(3, 'GrupoPrioritario 3');
/*!40000 ALTER TABLE `grupo_prioritario` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.historiaclinica
CREATE TABLE IF NOT EXISTS `historiaclinica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `id_doctor` int(11) NOT NULL,
  `id_diagnosticos` int(11) DEFAULT NULL,
  `hora` timestamp NULL DEFAULT NULL,
  `peso` float DEFAULT NULL,
  `estatura` float DEFAULT NULL,
  `perimetro_abdominal` float DEFAULT NULL,
  `presion` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `saturacion` float DEFAULT NULL,
  `signos_vitales_sistolica` int(11) DEFAULT NULL,
  `signos_vitales_diastolica` int(11) DEFAULT NULL,
  `temperatura` int(11) DEFAULT NULL,
  `frecuencia_respiratoria` int(11) DEFAULT NULL,
  `frecuencia_cardiaca` int(11) DEFAULT NULL,
  `valor_hemoglobina` int(11) DEFAULT NULL,
  `glucosa_capilar` int(11) DEFAULT NULL,
  `motivo_consulta` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observacion_diagnostico` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enfermedad_motivo_actual` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_historiaclinica_usuarios_doctor` (`id_doctor`),
  KEY `FK_historiaclinica_usuarios_paciente` (`id_paciente`),
  KEY `FK_historiaclinica_diagnosticos` (`id_diagnosticos`),
  CONSTRAINT `FK_historiaclinica_diagnosticos` FOREIGN KEY (`id_diagnosticos`) REFERENCES `diagnosticos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_historiaclinica_usuarios_doctor` FOREIGN KEY (`id_doctor`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_historiaclinica_usuarios_paciente` FOREIGN KEY (`id_paciente`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.historiaclinica: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `historiaclinica` DISABLE KEYS */;
INSERT INTO `historiaclinica` (`id`, `id_paciente`, `id_doctor`, `id_diagnosticos`, `hora`, `peso`, `estatura`, `perimetro_abdominal`, `presion`, `saturacion`, `signos_vitales_sistolica`, `signos_vitales_diastolica`, `temperatura`, `frecuencia_respiratoria`, `frecuencia_cardiaca`, `valor_hemoglobina`, `glucosa_capilar`, `motivo_consulta`, `observacion_diagnostico`, `enfermedad_motivo_actual`) VALUES
	(23, 7, 2, 24, '2021-04-15 01:53:53', 120, 175, 20, NULL, 17, 12, 13, 14, 15, 16, 22, 21, 'Vino con dolores de estomagos', '123123', 'mocosidad');
/*!40000 ALTER TABLE `historiaclinica` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.medicamentos
CREATE TABLE IF NOT EXISTS `medicamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.medicamentos: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `medicamentos` DISABLE KEYS */;
INSERT INTO `medicamentos` (`id`, `nombre`) VALUES
	(2, 'Paracetamol'),
	(3, 'Ibuprofeno'),
	(4, 'Loratadina'),
	(5, 'Diclofenaco'),
	(6, 'Naproxeno'),
	(7, 'Azitromicina'),
	(8, 'Aspirina'),
	(9, 'Amoxicilina'),
	(10, 'Dicloxicilina'),
	(11, 'Penicilina'),
	(12, 'Benzetacina'),
	(13, 'Losartan');
/*!40000 ALTER TABLE `medicamentos` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `detalle` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla proy_clinica_escuelita.permisos: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` (`id`, `nombre`, `detalle`) VALUES
	(1, 'CrearUsuario', 'Permite Crear Usuarios'),
	(2, 'EditarUsuario', 'Permite Editar Usuarios'),
	(3, 'BorrarUsuario', 'Permite Borrar Usuarios'),
	(4, 'ModificarUsuario', 'Permite Modificar Usuarios'),
	(5, 'CrearRol', 'Permite Crear Roles'),
	(6, 'CrearPermisos', 'Permite Crear Permisos'),
	(7, 'CrearPaciente', 'Permite Crear Paciente');
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.permiso_rol
CREATE TABLE IF NOT EXISTS `permiso_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permiso_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_permiso_rol_permiso_id` (`permiso_id`),
  KEY `FK_permiso_rol_rol_id` (`rol_id`),
  CONSTRAINT `FK_permiso_rol_permiso_id` FOREIGN KEY (`permiso_id`) REFERENCES `permisos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_permiso_rol_rol_id` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla proy_clinica_escuelita.permiso_rol: ~21 rows (aproximadamente)
/*!40000 ALTER TABLE `permiso_rol` DISABLE KEYS */;
INSERT INTO `permiso_rol` (`id`, `permiso_id`, `rol_id`) VALUES
	(1, 1, 17),
	(2, 2, 17),
	(3, 3, 17),
	(4, 4, 17),
	(5, 1, 26),
	(6, 2, 28),
	(7, 3, 28),
	(8, 1, 30),
	(9, 2, 24),
	(10, 3, 24),
	(11, 2, 27),
	(12, 3, 27),
	(13, 2, 25),
	(14, 4, 25),
	(15, 2, 18),
	(16, 1, 19),
	(17, 3, 20),
	(18, 2, 21),
	(19, 2, 22),
	(20, 1, 23),
	(21, 2, 23);
/*!40000 ALTER TABLE `permiso_rol` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.revision_sistema_historia_clinica
CREATE TABLE IF NOT EXISTS `revision_sistema_historia_clinica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_historia_clinica` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `observacion` int(11) DEFAULT NULL,
  `estado_patologia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.revision_sistema_historia_clinica: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `revision_sistema_historia_clinica` DISABLE KEYS */;
/*!40000 ALTER TABLE `revision_sistema_historia_clinica` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla proy_clinica_escuelita.roles: ~14 rows (aproximadamente)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `nombre`) VALUES
	(17, 'Administrador'),
	(18, 'Editor'),
	(19, 'Paciente'),
	(20, 'Doctor'),
	(21, 'Enrique'),
	(22, 'Enrique'),
	(23, 'Enrique'),
	(24, 'Enrique 22226'),
	(25, 'Enrique'),
	(26, 'Enrique'),
	(27, 'Enrique'),
	(28, 'Ecuador'),
	(30, 'Enrique');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.roles_usuarios
CREATE TABLE IF NOT EXISTS `roles_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `roles_usuarios_usuario_id` (`usuario_id`),
  KEY `roles_usuarios_role_id` (`role_id`),
  CONSTRAINT `roles_usuarios_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `roles_usuarios_usuario_id` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla proy_clinica_escuelita.roles_usuarios: ~29 rows (aproximadamente)
/*!40000 ALTER TABLE `roles_usuarios` DISABLE KEYS */;
INSERT INTO `roles_usuarios` (`id`, `usuario_id`, `role_id`) VALUES
	(3, 4, 19),
	(4, 5, 19),
	(7, 7, 19),
	(9, 9, 19),
	(10, 10, 19),
	(11, 11, 19),
	(12, 12, 19),
	(13, 13, 19),
	(14, 14, 19),
	(15, 15, 19),
	(16, 16, 19),
	(17, 17, 19),
	(29, 3, 17),
	(30, 3, 18),
	(31, 3, 19),
	(32, 3, 20),
	(47, 6, 19),
	(53, 8, 19),
	(62, 24, 17),
	(63, 24, 18),
	(68, 2, 17),
	(69, 2, 18),
	(70, 2, 19),
	(71, 2, 20),
	(72, 31, 17),
	(73, 31, 18),
	(74, 31, 20),
	(75, 32, 18),
	(76, 32, 20),
	(79, 36, 20);
/*!40000 ALTER TABLE `roles_usuarios` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.tipo_diagnostico
CREATE TABLE IF NOT EXISTS `tipo_diagnostico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.tipo_diagnostico: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_diagnostico` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_diagnostico` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.tipo_sangre
CREATE TABLE IF NOT EXISTS `tipo_sangre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.tipo_sangre: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_sangre` DISABLE KEYS */;
INSERT INTO `tipo_sangre` (`id`, `nombre`) VALUES
	(1, 'A Rh(+)'),
	(2, 'A Rh(-)');
/*!40000 ALTER TABLE `tipo_sangre` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.tratamiento
CREATE TABLE IF NOT EXISTS `tratamiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_historia_clinica` int(11) NOT NULL DEFAULT '0',
  `id_medicamentos` int(11) NOT NULL DEFAULT '0',
  `id_via_administracion` int(11) DEFAULT NULL,
  `dosis` int(11) DEFAULT NULL,
  `id_unidad_administracion` int(11) DEFAULT NULL,
  `id_frecuencia` int(11) DEFAULT NULL,
  `inicio_tratamiento` timestamp NULL DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `observacion` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `advertencia` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__historiaclinica` (`id_historia_clinica`),
  KEY `FK_tratamiento_medicamentos` (`id_medicamentos`),
  KEY `FK_tratamiento_tratamiento_via_administracion` (`id_via_administracion`),
  KEY `FK_tratamiento_tratamiento_unidad_administracion` (`id_unidad_administracion`),
  KEY `FK_tratamiento_tratamiento_frecuencia` (`id_frecuencia`),
  CONSTRAINT `FK__historiaclinica` FOREIGN KEY (`id_historia_clinica`) REFERENCES `historiaclinica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tratamiento_medicamentos` FOREIGN KEY (`id_medicamentos`) REFERENCES `medicamentos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_tratamiento_tratamiento_frecuencia` FOREIGN KEY (`id_frecuencia`) REFERENCES `tratamiento_frecuencia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tratamiento_tratamiento_unidad_administracion` FOREIGN KEY (`id_unidad_administracion`) REFERENCES `tratamiento_unidad_administracion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_tratamiento_tratamiento_via_administracion` FOREIGN KEY (`id_via_administracion`) REFERENCES `tratamiento_via_administracion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.tratamiento: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tratamiento` DISABLE KEYS */;
INSERT INTO `tratamiento` (`id`, `id_historia_clinica`, `id_medicamentos`, `id_via_administracion`, `dosis`, `id_unidad_administracion`, `id_frecuencia`, `inicio_tratamiento`, `cantidad`, `observacion`, `advertencia`) VALUES
	(3, 23, 4, 2, 1, 2, 1, '2021-04-21 17:30:00', 10, '', ''),
	(5, 23, 3, 1, 1, 3, 3, '2021-04-15 18:45:00', 10, '', '');
/*!40000 ALTER TABLE `tratamiento` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.tratamiento_frecuencia
CREATE TABLE IF NOT EXISTS `tratamiento_frecuencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.tratamiento_frecuencia: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `tratamiento_frecuencia` DISABLE KEYS */;
INSERT INTO `tratamiento_frecuencia` (`id`, `nombre`) VALUES
	(1, 'cada 12h'),
	(2, 'cada 8h'),
	(3, 'cada 6h'),
	(4, 'cada 24h');
/*!40000 ALTER TABLE `tratamiento_frecuencia` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.tratamiento_unidad_administracion
CREATE TABLE IF NOT EXISTS `tratamiento_unidad_administracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.tratamiento_unidad_administracion: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `tratamiento_unidad_administracion` DISABLE KEYS */;
INSERT INTO `tratamiento_unidad_administracion` (`id`, `nombre`) VALUES
	(1, 'Unidad'),
	(2, 'Pastilla'),
	(3, 'Suero'),
	(4, 'Cucharada'),
	(5, 'Inyección'),
	(6, 'Gotas');
/*!40000 ALTER TABLE `tratamiento_unidad_administracion` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.tratamiento_via_administracion
CREATE TABLE IF NOT EXISTS `tratamiento_via_administracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.tratamiento_via_administracion: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `tratamiento_via_administracion` DISABLE KEYS */;
INSERT INTO `tratamiento_via_administracion` (`id`, `nombre`) VALUES
	(1, 'oral'),
	(2, 'subcutanea'),
	(3, 'inyectable'),
	(4, 'intramuscular'),
	(5, 'intravenosa');
/*!40000 ALTER TABLE `tratamiento_via_administracion` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `discapacidad` bit(1) DEFAULT NULL,
  `identificacion` varchar(13) DEFAULT NULL,
  `nickname` varchar(60) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `apellido` varchar(60) DEFAULT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_actualizacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sexo` int(11) DEFAULT NULL,
  `id_tipo_sangre` int(11) DEFAULT NULL,
  `id_grupo_prioritario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_usuarios_tipo_sangre` (`id_tipo_sangre`),
  KEY `FK_usuarios_grupo_prioritario` (`id_grupo_prioritario`),
  CONSTRAINT `FK_usuarios_grupo_prioritario` FOREIGN KEY (`id_grupo_prioritario`) REFERENCES `grupo_prioritario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_usuarios_tipo_sangre` FOREIGN KEY (`id_tipo_sangre`) REFERENCES `tipo_sangre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla proy_clinica_escuelita.usuarios: ~31 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `email`, `password`, `fecha_nacimiento`, `discapacidad`, `identificacion`, `nickname`, `nombre`, `apellido`, `estado`, `fecha_creado`, `fecha_actualizacion`, `sexo`, `id_tipo_sangre`, `id_grupo_prioritario`) VALUES
	(2, 'enrique_e449@hotmail.com', '$2y$10$It/Axzm0AOYBxvrQY9o9MOyWyuBDB4Rt3/vfPbYwY8Hg9uq8gxHeS', '1986-03-27', NULL, '0921197067', 'usuario_id_2', 'Enrique Emilio', 'Bonifaz Gutierrez', b'0', '2021-03-20 12:58:39', '2021-04-06 11:32:29', NULL, NULL, NULL),
	(3, 'test3@dev.com2', '$2y$10$EHZ9wfJGH48eSo9kJdUBQ.Da5XvWHo0xGu5WRtguWdaXrqoNOUYke', '1986-03-27', NULL, '0921197032', 'test32222222', 'testname32', 'testapellido32', b'0', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(4, 'test4@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '2021-02-27', NULL, '092119704', 'test4', 'testname4', 'testapellido4', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(5, 'test5@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '1986-12-26', b'1', '092119705', 'test5', 'testname5', 'testapellido5', b'1', '2021-03-20 12:58:36', '2021-04-15 18:12:21', 1, 2, 3),
	(6, 'test6@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '2021-04-09', b'1', '092119706', 'test6', 'testname6', 'testapellido6', b'1', '2021-03-20 12:58:36', '2021-04-15 02:14:36', 0, 2, NULL),
	(7, 'test7@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '1986-12-26', b'1', '092119707', 'test7', 'testname7', 'testapellido7', b'1', '2021-03-20 12:58:36', '2021-04-15 02:07:20', 1, 1, 3),
	(8, 'test8@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '2020-03-27', NULL, '092119708', 'test8', 'testname8', 'testapellido8', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(9, 'test9@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '1986-12-26', b'0', '092119709', 'test9', 'testname9', 'testapellido9', b'1', '2021-03-20 12:58:36', '2021-04-15 18:11:38', 0, 1, 2),
	(10, 'test10@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '2010-01-27', NULL, '0921197010', 'test10', 'testname10', 'testapellido10', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(11, 'test11@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '2014-07-27', NULL, '0921197011', 'test11', 'testname11', 'testapellido11', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(12, 'test12@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '1984-01-27', NULL, '0921197012', 'test12', 'testname12', 'testapellido12', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(13, 'test13@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '1984-01-27', NULL, '0921197013', 'test13', 'testname13', 'testapellido13', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(14, 'test14@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '1984-01-26', NULL, '0921197014', 'test14', 'testname14', 'testapellido14', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(15, 'test15@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '1984-01-24', NULL, '0921197015', 'test15', 'testname15', 'testapellido15', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(16, 'test16@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '1984-01-26', NULL, '0921197016', 'test16', 'testname16', 'testapellido16', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(17, 'test17@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '1984-02-25', NULL, '0921197017', 'test17', 'testname17', 'testapellido17', b'1', '2021-03-20 12:58:36', '2021-03-20 12:58:37', NULL, NULL, NULL),
	(19, 'enrique_e449@hotmail.com', '$2y$10$0dUxu9WAJ9rruD2yzMrq4u2eyvIHNKaWGjo537AB33xJznO2hlFPi', NULL, NULL, '0921197067', 'andres', 'Enrique', 'Bonifaz', b'1', '2021-04-03 00:13:39', '2021-04-03 00:13:39', NULL, NULL, NULL),
	(20, 'enrique_e449@hotmail.com', '$2y$10$zmbTyq9h8mRlndUVvlhAFO861YJck6Z6hiZy0rhQIIqwH6S3/gFcO', NULL, NULL, '0921197067', 'andres', 'Enrique', 'Bonifaz', b'1', '2021-04-03 00:14:18', '2021-04-03 00:14:18', NULL, NULL, NULL),
	(21, 'enrique_e449@hotmail.com', '$2y$10$zgJZobPRppqc3CoYQE.dxeSiF6bBirfbcVeAeIWhCo7JeLKe4O1rG', NULL, NULL, '0921197067', 'andres', 'Enrique', 'Bonifaz', b'1', '2021-04-03 09:12:35', '2021-04-03 09:12:35', NULL, NULL, NULL),
	(22, 'enrique_e449@hotmail.com', '$2y$10$bARA4mD66QT8drqRRaTU.eVLUnqzILI8WZ8ghnY8SXLkeaxBARP7W', NULL, NULL, '0921197067', 'andres111', 'Enrique', 'Bonifaz', b'1', '2021-04-03 09:54:03', '2021-04-03 09:54:03', NULL, NULL, NULL),
	(24, 'enrique_e4492@hotmail.com2', '$2y$10$GUgcY161MrPEI5LZBzB9F.Ej/3h2hUh7rphJy89fDM5DNiBkg4jIC', NULL, NULL, '0921197067', 'andres22', 'Enrique', 'Bonifaz', b'1', '2021-04-04 11:54:37', '2021-04-04 11:54:37', NULL, NULL, NULL),
	(25, 'enrique_e449@hotmail.com', '$2y$10$CTSj0eVQpWP54NxY7jU9DetO5/bBwTQTdPe2wh.1FJxNntNc39FmG', NULL, NULL, '0921197067', 'andres', 'Enrique', 'Bonifaz', b'1', '2021-04-04 23:03:23', '2021-04-04 23:03:23', NULL, NULL, NULL),
	(26, 'enrique_e449@hotmail.com', '$2y$10$FGhxnu2rhI7pm1cjIm3NL.Jg0FOjJZT/xbOmDFM0kHL.o3TbY1EPm', NULL, NULL, '0921197067', 'andres', 'Enrique', 'Bonifaz', b'1', '2021-04-04 23:03:46', '2021-04-04 23:03:46', NULL, NULL, NULL),
	(27, 'enrique_e449@hotmail.com', '$2y$10$Vch5/bJuSkXtOTyljbGIvOGm.RlqRJ7bieDVMWkb62mhoYa3su58K', NULL, NULL, '0921197067', 'andres', 'Enrique', 'Bonifaz', b'1', '2021-04-04 23:05:07', '2021-04-04 23:05:07', NULL, NULL, NULL),
	(28, 'enrique_e449@hotmail.com', '$2y$10$T9NlnTQ/VGomdZBAiX2UgeQMsMvNQ9kkdg/iSXTlpRbICWD8FFIr2', NULL, NULL, '0921197067', 'andres', 'Enrique', 'Bonifaz', b'1', '2021-04-04 23:05:58', '2021-04-04 23:05:58', NULL, NULL, NULL),
	(30, 'enrique_e449@hotmail.com', '$2y$10$RcLUBBWkCDIHdF2d86B98u9ayNCdV9iX0Y.o8ac/BG/OKvvLjGgCW', NULL, NULL, '0921197067', 'andres', 'Enrique', 'Bonifaz', b'1', '2021-04-04 23:08:26', '2021-04-04 23:08:26', NULL, NULL, NULL),
	(31, 'enrique_e449@hotmail.com', '$2y$10$vamF7L00Edni3bGoAzNOTuHjilhWChzWLgbrn0c7xxjem8umzB06i', NULL, NULL, '0921197067', 'andres', 'Enrique', 'Bonifaz', b'1', '2021-04-04 23:09:06', '2021-04-04 23:09:06', NULL, NULL, NULL),
	(32, 'telmo', '$2y$10$HW4ohH.gRwSHKWwj8u/Hdu/uhD6KGiVhtnZ/7hglex/FYy4J8XcP.', NULL, NULL, '0921197067', 'telmo', 'Enrique', 'Bonifaz', b'1', '2021-04-04 23:09:36', '2021-04-04 23:09:36', NULL, NULL, NULL),
	(33, 'test5@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', NULL, NULL, '092119705', 'test5', 'testname5', 'testapellido5', b'1', '2021-04-15 00:38:13', '2021-04-15 00:38:13', NULL, NULL, NULL),
	(34, 'test5@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '2021-04-07', b'0', '092119705', 'test5', 'testname5', 'testapellido5', b'1', '2021-04-15 00:54:09', '2021-04-15 02:14:39', 0, NULL, NULL),
	(35, 'test5@dev.com', '$2y$10$/IKYMpshIx8rPOtDIEnkReKb2x4WIrAUgQxJSxZc6cOd.I5cIHYeS', '2021-04-07', b'0', '092119705', 'test5', 'testname5', 'testapellido5', b'1', '2021-04-15 00:54:37', '2021-04-15 02:14:40', 0, NULL, NULL),
	(36, '111diseno@gmail.com', '$2y$10$YPOjdvET34Ay0/yGBXSjxe3SK6.YrxbDUv2XPAZ6f/T1ibIg0iGRa', NULL, NULL, '0921197067', '111diseno', 'Enrique', 'Bonifaz', b'1', '2021-04-15 18:47:32', '2021-04-15 18:47:32', NULL, NULL, NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

-- Volcando estructura para tabla proy_clinica_escuelita.usuarios_antecedentes
CREATE TABLE IF NOT EXISTS `usuarios_antecedentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `id_antecedente` int(11) DEFAULT NULL,
  `observacion` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__usuarios` (`id_usuario`),
  KEY `FK__antecedentes` (`id_antecedente`),
  CONSTRAINT `FK__antecedentes` FOREIGN KEY (`id_antecedente`) REFERENCES `antecedentes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK__usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla proy_clinica_escuelita.usuarios_antecedentes: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios_antecedentes` DISABLE KEYS */;
INSERT INTO `usuarios_antecedentes` (`id`, `id_usuario`, `id_antecedente`, `observacion`) VALUES
	(11, 7, 12, '12312312'),
	(13, 7, 14, 'alegica'),
	(16, 7, 5, 'Tuvo cálculos '),
	(31, 7, 5, 'padre muerto por enfermedad renal'),
	(46, 7, 13, 'Está es otra observación'),
	(47, 9, 15, 'problemas');
/*!40000 ALTER TABLE `usuarios_antecedentes` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
