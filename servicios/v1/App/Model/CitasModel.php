<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once "UserModel.php";
require_once "EstadoCita.php";



class CitasModel extends Model
{
	public $id;
	public $hora_solicitada;
	public $id_paciente;
	public $id_doctor;
	public $id_estado_cita;
	public $observacion;
     
    protected $table = "cita";

    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'hora_solicitada' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ], 
        'id_paciente' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'id_doctor' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'id_estado_cita' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'observacion' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ],
    ]; 
    public function __contruct()
    {
        parent::__contruct();
    }

    public function cargarCitas()
    {
        $conexion = new BaseDatos();
        $limite_paginacion = LIMITE_PAGINACION;
        $router = new Router();
        $page = $router->getPage();
        $paginacion = ($page - 1) * ($this->paginacion);
        $query = "SELECT 
            c.id,
            c.hora_solicitada,
            c.observacion,
            CONCAT(p.nombre,' ',p.apellido) AS paciente,
            CONCAT(d.nombre,' ',d.apellido) AS doctor,
            ec.nombre AS estado,
            ec.tipo AS estado_tipo,
            c.id_historia_clinica
        FROM cita c
        INNER JOIN usuarios p ON p.id = c.id_paciente
        INNER JOIN usuarios d ON d.id = c.id_doctor
        INNER JOIN estado_cita ec ON ec.id = c.id_estado_cita  LIMIT {$paginacion},{$limite_paginacion}";

        return $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
    }

    public function numeroCitas()
    {
        $conexion = new BaseDatos();
        $query = "SELECT 
            count(*) as num
        FROM cita c
        INNER JOIN usuarios p ON p.id = c.id_paciente
        INNER JOIN usuarios d ON d.id = c.id_doctor
        INNER JOIN estado_cita ec ON ec.id = c.id_estado_cita"; 

        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
        return $resultado[0]['num'];
    } 

    public function listado_citas()
    {
        $conexion = new BaseDatos();
        $query = "SELECT 

        CONCAT( 'Paciente: ', p.nombre , ' ', p.apellido) AS title,
        CONCAT(  'Doctor: ', d.nombre , ' ', d.apellido,', Hora: ' , DATE_FORMAT(c.hora_solicitada, '%H:%i') ) AS description,
        DATE_FORMAT(c.hora_solicitada, '%Y-%m-%dT%H:%i:%sZ') AS 'start',
        DATE_FORMAT( DATE_ADD(c.hora_solicitada, INTERVAL 1 HOUR) , '%Y-%m-%dT%H:%i:%sZ') AS 'end',
        '#3A87AD' AS color,
        '#ffffff' AS textColor
        FROM cita c
        INNER JOIN usuarios p ON p.id = c.id_paciente
        INNER JOIN usuarios d ON d.id = c.id_doctor"; 

        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC);  
        return $resultado;
    }

    public function listado_citas_by_doctor($id_doctor)
    {
        $conexion = new BaseDatos();
        $query = "SELECT 

        CONCAT( 'Paciente: ', p.nombre , ' ', p.apellido) AS title,
        CONCAT(  'Doctor: ', d.nombre , ' ', d.apellido,', Hora: ' , DATE_FORMAT(c.hora_solicitada, '%H:%i') ) AS description,
        DATE_FORMAT(c.hora_solicitada, '%Y-%m-%dT%H:%i:%sZ') AS 'start',
        DATE_FORMAT( DATE_ADD(c.hora_solicitada, INTERVAL 1 HOUR) , '%Y-%m-%dT%H:%i:%sZ') AS 'end',
        '#3A87AD' AS color,
        '#ffffff' AS textColor
        FROM cita c
        INNER JOIN usuarios p ON p.id = c.id_paciente
        INNER JOIN usuarios d ON d.id = c.id_doctor
        WHERE d.id = {$id_doctor}"; 

        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC);  
        return $resultado;
    }

    public function getPaciente()
    {
        $usuario = new UserModel;
        $usuario->get($this->id_paciente);
         
        return $usuario;
    }


    public function getDoctor()
    {
        $usuario = new UserModel;
        $usuario->get($this->id_doctor);
         
        return $usuario;
    }

    public function getEstado()
    {
        $estado_cita = new EstadoCita;
        $estado_cita->get($this->id_estado_cita);
         
        return $estado_cita;
    }

    public function hasPacienteById( $id_paciente )
    {
        return $id_paciente == $this->id_paciente;
    }

    public function hasDoctorById( $id_doctor )
    {
        return $id_doctor == $this->id_doctor;
    }

    public function hasCitaById( $id_estado_cita )
    {
        return $id_estado_cita == $this->id_estado_cita;
    }


    public function getFecha()
    {
        
        if( $this->hora_solicitada == "" );{
            return date("Y-m-d"); 
        }
        return date("Y-m-d", strtotime($this->hora_solicitada)); 
    }

    public function getHora()
    {
        if( $this->hora_solicitada == "" );{
            return date("H:m:s"); 
        }
        return date("H:m", strtotime($this->hora_solicitada)); 
    }
    
    public function validateID( $valor, $nombre)
    {
        $conexion = new BaseDatos();

        $sql = "SELECT nombre  FROM roles_usuarios  
        left JOIN roles ON roles_usuarios.role_id = roles.id 
        WHERE roles_usuarios.usuario_id = {$valor}"; 

        $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC);
        foreach ($result as $rol) {
            $nombre_rol = strtolower($rol['nombre']);
            if ( $nombre_rol == $nombre){
                return true;
            }
        }
        return  false;
    }

    public function validateEstado( $id_estado, $id_doctor)
    {
        $name_estdo = $this->nameEstado($id_estado);
        
        $conexion = new BaseDatos();

        $sql = "SELECT *, TIME_FORMAT(TIME(hora_solicitada), '%H')  AS hora FROM cita 
        LEFT JOIN estado_cita ON cita.id_estado_cita = estado_cita.id 
        WHERE nombre = '{$name_estdo}' AND id_doctor = {$id_doctor}"; 

        $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC);
        
        return  $result;
    }

    public function nameEstado($valor)
    {
        $conexion = new BaseDatos();

        $sql = "SELECT nombre FROM estado_cita WHERE id = {$valor} "; 

        $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC);
        return  $result[0]['nombre'];
    }

    public function validateIdEstado($valor)
    {
        $conexion = new BaseDatos();

        $sql = "SELECT count(*) AS num FROM	cita 
        LEFT JOIN	estado_cita ON cita.id_estado_cita = estado_cita.id 
        WHERE estado_cita.id = {$valor}"; 

        $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC);
        
        return  $result[0];
    }
}