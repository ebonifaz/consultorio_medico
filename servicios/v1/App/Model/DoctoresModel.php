<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class DoctoresModel extends Model
{

    public function __contruct()
    {
        parent::__contruct();
    }

    public function cargarDoctores()
    {
        $conexion = new BaseDatos();
        $limite_paginacion = LIMITE_PAGINACION;
        $router = new Router();
        $page = $router->getPage();
        $paginacion = ($page - 1) * ($this->paginacion);
        $query = "SELECT 
        usuarios.id,
        email,
        fecha_nacimiento,
        identificacion,
        nickname,
        nombre,
        apellido,
        discapacidad,
        estado,
        u_cita.*
        FROM usuarios
        INNER JOIN roles_usuarios ON roles_usuarios.usuario_id = usuarios.id
        LEFT JOIN (
            SELECT id_paciente, peso, estatura, presion, saturacion, hora as tiempo
            FROM historiaclinica
            INNER JOIN (
                SELECT id_paciente as id_paci,max(hora) as fecha
                FROM historiaclinica 
                GROUP BY id_paciente
            ) ultima_cita on 
            (
                ultima_cita.id_paci=historiaclinica.id_paciente
                AND
                ultima_cita.fecha=historiaclinica.hora 
            )
        ) u_cita ON u_cita.id_paciente = usuarios.id
        WHERE roles_usuarios.role_id = ".ID_ROL_DOCTOR. " LIMIT {$paginacion},{$limite_paginacion}";
 

        return $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
    }

    
    public function cargarDoctorByCampos($campos)
    {
        $conexion = new BaseDatos();
        $query = "SELECT 
        {$campos}
        FROM usuarios
        INNER JOIN roles_usuarios ON roles_usuarios.usuario_id = usuarios.id
        WHERE roles_usuarios.role_id = ".ID_ROL_DOCTOR;
        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
        return $resultado;
    }

    public function numeroDoctores()
    {
        $conexion = new BaseDatos();
        $limite_paginacion = LIMITE_PAGINACION;
        $router = new Router();
        $page = $router->getPage();
        $paginacion = ($page - 1) * ($this->paginacion);
        $query = "SELECT 
        count(*) as num
        FROM usuarios
        INNER JOIN roles_usuarios ON roles_usuarios.usuario_id = usuarios.id
        LEFT JOIN (
            SELECT id_paciente, peso, estatura, presion, saturacion, hora as tiempo
            FROM historiaclinica
            INNER JOIN (
                SELECT id_paciente as id_paci,max(hora) as fecha
                FROM historiaclinica 
                GROUP BY id_paciente
            ) ultima_cita on 
            (
                ultima_cita.id_paci=historiaclinica.id_paciente
                AND
                ultima_cita.fecha=historiaclinica.hora 
            )
        ) u_cita ON u_cita.id_paciente = usuarios.id
        WHERE roles_usuarios.role_id = ".ID_ROL_DOCTOR. " LIMIT {$paginacion},{$limite_paginacion}";
 

        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
        return $resultado[0]['num'];
    }
     
}
