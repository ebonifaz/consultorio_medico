<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class CondicionDiagnosticoModel extends Model{

    public $id = "";
    public $nombre = ""; 
    
    // protected $campos = ['id','nombre','detalle'];
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'nombre' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ],
    ]; 
    protected $table = "condicion_diagnostico";
    
} 