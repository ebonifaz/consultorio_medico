<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class TipoSangreModel extends Model{

    public $id = "";
    public $nombre = "";   
    
    // protected $campos = ['id','nombre','detalle'];
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'nombre' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ], 
    ]; 
    protected $table = "diagnosticos";
    
    public function cargarTipoSangre()
    {
        $conexion = new BaseDatos();
        $query = 'SELECT id, nombre
        FROM tipo_sangre'; 

        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC);  
        return $resultado;
    }

}