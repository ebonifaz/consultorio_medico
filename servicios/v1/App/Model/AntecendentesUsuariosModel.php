<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class AntecendentesUsuariosModel extends Model{

    public $id = "";
    public $id_usuario = "";  
    public $id_antecedente = "";  
    public $observacion = "";  
    
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'id_usuario' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'id_antecedente' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'observacion' => [
            "tipo" => "text",
            "validaciones" => []
        ],
    ]; 
    protected $table = "usuarios_antecedentes";
     
}
