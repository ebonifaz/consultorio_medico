
<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class EstadoCita extends Model{

    public $id = "";
    public $nombre = "";  
    public $tipo = "";  
    
    // protected $campos = ['id','nombre','tipo'];
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'nombre' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ],
        'tipo' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ], 
    ]; 
    protected $table = "estado_cita";
    
    public function cargarEstadoCitaByCampos($campos)
    {
        $conexion = new BaseDatos();
        $query = "SELECT 
        {$campos}
        FROM {$this->table}"; 
        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
        return $resultado;
    }
}