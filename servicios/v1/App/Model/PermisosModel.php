<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class PermisosModel extends Model{

    public $id = "";
    public $nombre = "";  
    public $detalle = "";  
    
    // protected $campos = ['id','nombre','detalle'];
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'nombre' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ],
        'detalle' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ], 
    ]; 
    protected $table = "permisos";
    
}