<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class DiagnosticoModel extends Model{

    public $id = "";
    public $nombre = "";   
    
    // protected $campos = ['id','nombre','detalle'];
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'nombre' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ],
        'codigo_cie' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
    ]; 
    protected $table = "diagnosticos";
    
    public function cargarHistorias()
    {
        $conexion = new BaseDatos();
        $query = 'SELECT 
        id, 
        CONCAT("[",d.codigo_cie,"] ",d.nombre) AS nombre
        FROM diagnosticos d        
        '; 

        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC);  
        return $resultado;
    }

}