<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once "RolesModel.php";

class UserModel extends Model{

    public $id = "";
    public $email = "";
    public $password = "";
    public $nickname = "";
    public $apellido = "";
    public $nombre = "";    
    public $estado = "";
    public $roles = [];
    public $identificacion = ""; 
    public $fecha_creado = "";
    public $fecha_actualizacion = ""; 
    public $id_tipo_sangre = ""; 
    public $id_grupo_prioritario = "";
    public $fecha_nacimiento = "";
    
    // protected $campos = ['id','email','nickname','nombre','apellido','estado','identificacion'];


    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
            // que sea numero
        ],
        'email' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
            // que sea unico
            // que tenga formato
            // requerido
            
        ],
        'nickname' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
            // que sea unico
            // que sea texto UTF8
            // requerido
        ],
        'nombre' => [
            "tipo" => "text",
            "validaciones" => ["requerido"] 
            // que tenga formato
            // requerido
        ],
        'apellido' => [
            "tipo" => "text",
            "validaciones" => ["requerido"] 
            // que tenga formato
            // requerido
        ],
        'estado' => [
            "tipo" => "checkbox",
            "validaciones" => ["requerido"]
        ],
        'identificacion' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
            // que sea unico
            // que tenga formato de cédula
            // codigo de niños (profe)
            // requerido
        ],
        'sexo' => [
            "tipo" => "text",
            "validaciones" => ["requerido"] 
        ],
        'fecha_nacimiento' => [
            "tipo" => "text",
            "validaciones" => ["requerido"] 
        ],
        'discapacidad' => [
            "tipo" => "int",
            "validaciones" => [] 
        ],
        'id_tipo_sangre' => [
            "tipo" => "int",
            "validaciones" => [] 
        ],
        'id_grupo_prioritario' => [
            "tipo" => "int",
            "validaciones" => [] 
        ],
        'password' => [
            "tipo" => "password"
        ]
    ];
 
    

    protected $table = "usuarios";

    public function Ingreso($email){
        $bd = new BaseDatos();
        $email = $bd->db->real_escape_string($email);
        $sql = "SELECT * FROM usuarios WHERE email='{$email}'"; 
        return $bd->db->query( $sql );
    } 
    
    public function getRoles()
    { 
        $conexion = new BaseDatos();
        $sql = "SELECT role_id FROM roles_usuarios WHERE usuario_id='{$this->id}'"; 
        $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC);
        foreach ($result as $rol) {
            $rol_id = $rol['role_id'];
            $rol = new RolesModel;
            $rol->get( $rol_id );
            array_push($this->roles, $rol);
        }        
    } 
    
    public function setRolesYUSuarios( $roles )
    {
        // Remover Roles 
        $conexion = new BaseDatos();
        $sql = "DELETE FROM roles_usuarios WHERE roles_usuarios.usuario_id = {$this->id}";  
        $result = $conexion->db->query( $sql );

        $valores_ingresar = [];
        foreach ($roles as $rol) {
            $valores_ingresar[] = "({$this->id},{$rol})";
        }
        $valores_ingresar_text = implode(",",$valores_ingresar);
        
        $sql = "INSERT INTO roles_usuarios (usuario_id, role_id) VALUES {$valores_ingresar_text}";
        $result = $conexion->db->query( $sql ); 
    }

    public function hasRoles($rol)
    { 
        foreach ($this->roles as $rolitem) {
            if( $rolitem->id == $rol->id ){
                return true;
            }
        }
        return false;
    }


    public function all_con_roles()
    {
        return parent::all_callback('getRoles'); 
    }

    public function get_edad()
    {
        $fecha_nacimiento = new DateTime($this->fecha_nacimiento);
        $hoy = new DateTime();
        $edad = $hoy->diff($fecha_nacimiento);
 

        $texto = "Años: {$edad->y}";
        if( $edad->m ){
            $texto .= " Mes: {$edad->m}";
            
        }
        if( $edad->d ){
            $texto .= " Días: {$edad->d}";
            
        }

        return $texto;
    }
    
    public function getTipoSangre()
    {        
        $conexion = new BaseDatos(); 
        $sql = "SELECT * FROM tipo_sangre WHERE id={$this->id_tipo_sangre}"; 
        $resultado = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC);  
        return $resultado[0]['nombre'];
    }

    public function set_contrasenia($contrasenia)
    {
        if( $contrasenia == "" ) return;
        $this->password = password_hash( $contrasenia, PASSWORD_DEFAULT ); 
        return $this->password;
    }
    
}
