<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once "PermisosModel.php";

class RolesModel extends Model{

    public $id = "";
    public $nombre = "";  
    public $permisos = [];
    
    // protected $campos = ['id','nombre'];
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'nombre' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ], 
    ]; 
    protected $table = "roles";
    
    public function all_con_permisos()
    {
        return parent::all_callback('getPermisos'); 
    } 
    public function getPermisos()
    {
        $conexion = new BaseDatos();
        $sql = "SELECT permiso_id FROM permiso_rol WHERE rol_id='{$this->id}'"; 
        $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC);
        
        foreach ($result as $rol) {
            $permiso_id = $rol['permiso_id'];
            $rol = new PermisosModel;
            $rol->get( $permiso_id );
            array_push($this->permisos, $rol);
        }        
    } 

    public function hasPermisos($permiso)
    {  
        foreach ($this->permisos as $permisoitem) {
            if( $permisoitem->id == $permiso->id ){
                return true;
            }
        }
        return false;
    }
    
    public function setPermisosYRoles( $permisos )
    {
        // Remover Roles 

        $conexion = new BaseDatos();
        $sql = "DELETE FROM permiso_rol WHERE permiso_rol.rol_id = {$this->id}"; 
        $result = $conexion->db->query( $sql );
        
        $valores_ingresar = [];
        foreach ($permisos as $permiso) {
            $valores_ingresar[] = "({$this->id},{$permiso})";
        }
        $valores_ingresar_text = implode(",",$valores_ingresar);
                
        $sql = "INSERT INTO permiso_rol (rol_id, permiso_id) VALUES {$valores_ingresar_text}";
        $result = $conexion->db->query( $sql ); 
    }
}