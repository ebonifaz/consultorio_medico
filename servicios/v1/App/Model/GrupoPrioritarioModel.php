<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class GrupoPrioritarioModel extends Model{

    public $id = "";
    public $nombre = "";  
    
    // protected $campos = ['id','nombre','detalle'];
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'nombre' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ],
    ]; 
    protected $table = "grupo_prioritario";
    
    public function cargarByCampos()
    {
        $conexion = new BaseDatos();
        $query = "SELECT id, nombre
        FROM {$this->table}"; 
        
        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC);  
        return $resultado;
    }
}
