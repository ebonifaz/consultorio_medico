<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class HistorialclinicaModel extends Model
{
    public $id;
	public $id_paciente;
	public $id_doctor;
	public $id_diagnosticos;
	public $discapacidad;
	public $hora;
	public $peso;
	public $estatura;
	public $perimetro_abdominal;
	public $presion;
	public $saturacion;
	public $signos_vitales_sistolica;
	public $signos_vitales_diastolica;
	public $temperatura;
	public $frecuencia_respiratoria;
	public $frecuencia_cardiaca;
	public $glucosa_capilar;
	public $valor_hemoglobina;
	public $motivo_consulta;
	public $examen_fisico_regional;
	public $enfermedad_motivo_actual;
    public $observacion_diagnostico;
	public $id_tipo_diagnostico;
	public $id_condicion_diagnostico;
	public $id_cronologia;
    
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'id_paciente' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'id_doctor' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ], 
        'hora' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ],         
        'enfermedad_motivo_actual' => [
            "tipo" => "text",
            "validaciones" => []
        ],
        'motivo_consulta' => [
            "tipo" => "text",
            "validaciones" => []
        ],
        'signos_vitales_sistolica' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'signos_vitales_diastolica' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'temperatura' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'frecuencia_respiratoria' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'frecuencia_cardiaca' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'saturacion' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'peso' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'estatura' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'perimetro_abdominal' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'glucosa_capilar' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'valor_hemoglobina' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'id_diagnosticos' => [
            "tipo" => "int",
            "validaciones" => []
        ], 
		'observacion_diagnostico' => [
            "tipo" => "text",
            "validaciones" => []
        ], 
        

    ]; 
    protected $table = "historiaclinica";

    public function __contruct()
    {
        parent::__contruct();
    }

    /*
    * Array Parametros position 0 = id_user
    */ 

    public function cargarHistorias()
    {
        $conexion = new BaseDatos();
        $limite_paginacion = LIMITE_PAGINACION;
        $router = new Router();
        $page = $router->getPage();
        $paginacion = ($page - 1) * ($this->paginacion);
        $query = "SELECT 
            h.id,
            hora AS hora_atencion,
            p.discapacidad,
            peso,
            estatura,
            CONCAT(h.signos_vitales_sistolica,'/',h.signos_vitales_diastolica) as presion,
            saturacion,
            ts.nombre AS tipo_sangre,
            CONCAT(p.nombre,' ',p.apellido) AS paciente,
            CONCAT(d.nombre,' ',d.apellido) AS doctor,
            dg.nombre AS diagnostico
            FROM historiaclinica h
            INNER JOIN usuarios p ON p.id = h.id_paciente
            INNER JOIN usuarios d ON d.id = h.id_doctor
            LEFT JOIN tipo_sangre ts ON ts.id = p.id_tipo_sangre
            LEFT JOIN diagnosticos dg ON dg.id = h.id_diagnosticos LIMIT {$paginacion},{$limite_paginacion}";
 
        return $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
    }

    public function numeroHistorias()
    {
        $conexion = new BaseDatos();
        $query = "SELECT 
            count(*) as num 
            FROM historiaclinica h
            INNER JOIN usuarios p ON p.id = h.id_paciente
            INNER JOIN usuarios d ON d.id = h.id_doctor
            LEFT JOIN tipo_sangre ts ON ts.id = p.id_tipo_sangre
            LEFT JOIN diagnosticos dg ON dg.id = h.id_diagnosticos ";
 

        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
        return $resultado[0]['num'];
    } 

    public function buscarHistoriaClinica($parametros){
        
        $conexion = new BaseDatos(); 
        $where = [];
        if( true ){
            $where[] = "`u`.`identificacion` like '%{$parametros['identificacion']}%'";
        }
        if( true ){
            $where[] = "`u`.`nombre` like '%{$parametros['nombre']}%'";
        }
        if( true ){
            $where[] = "`u`.`apellido` like '%{$parametros['apellido']}%'";
        } 
        
        if( $parametros['estado'] != "" ){
            $where[] = "`u`.`estado` = {$parametros['estado']}";
        }

        if( $parametros['estado'] != "" ){
            $where[] = "`role_id` = ".ID_ROL_DOCTOR."";
        }
        $where_text = implode(" and ", $where);
        $query = "SELECT u.id,
        u.email,
        u.identificacion,
        u.nombre,
        u.apellido,
        u.estado 
        FROM `usuarios` u
        INNER JOIN roles_usuarios on u.id = roles_usuarios.usuario_id
        WHERE {$where_text} ";  
        return $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
    }
    public function getPaciente()
    {
        $usuario = new UserModel;
        $usuario->get($this->id_paciente);
         
        return $usuario;
    }

    public function getDoctor()
    {
        $usuario = new UserModel;
        $usuario->get($this->id_doctor);
         
        return $usuario;
    }

    public function getFecha()
    {
        
        if( $this->hora == "" );{
            return date("Y-m-d"); 
        }
        return date("Y-m-d", strtotime($this->hora)); 
    }

    public function getHora()
    {
        if( $this->hora == "" );{
            return date("H:m:s"); 
        }
        return date("H:m", strtotime($this->hora)); 
    }
    
    public function getImc()
    {
        if( ! $this->peso ) return;
        if( ! $this->estatura ) return;
        return round( ( $this->peso * 10000 )/( $this->estatura * $this->estatura ) , 2 ); 
    }
                                                        
}
