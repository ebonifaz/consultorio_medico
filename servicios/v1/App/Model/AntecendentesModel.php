<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class AntecendentesModel extends Model{

    public $id = "";
    public $nombre = "";  
    public $id_antecedente_categoria = "";  
    
    // protected $campos = ['id','nombre','detalle'];
    protected $campos = [
        'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
        'nombre' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ],
        'id_antecedente_categoria' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
    ]; 
    protected $table = "antecedentes";
    
    public function listado()
    {
        $conexion = new BaseDatos();
        $query = 'SELECT a.id, CONCAT("[",c.nombre, "] ",a.nombre) as nombre
        FROM antecedentes a
        INNER JOIN antecedente_categoria c ON a.id_antecedente_categoria = c.id
        ORDER BY a.id_antecedente_categoria DESC'; 

        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC);  
        return $resultado;
    }
}
