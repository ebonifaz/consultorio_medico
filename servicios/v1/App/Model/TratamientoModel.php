<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class TratamientoModel extends Model{

    public $id = "";
    public $id_historia_clinica = "";
    public $id_medicamentos = "";
    public $id_via_administracion = "";
    public $dosis = "";
    public $id_unidad_administracion = "";
    public $id_frecuencia = "";
    public $inicio_tratamiento = "";
    public $cantidad = "";
    public $observacion = "";
    public $advertencia = "";
    
    // protected $campos = ['id','nombre','detalle'];
    protected $campos = [
		'id' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
		'id_historia_clinica' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
		'id_medicamentos' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
		'id_via_administracion' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
		'dosis' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
		'id_unidad_administracion' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
		'id_frecuencia' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
		'inicio_tratamiento' => [
            "tipo" => "text",
            "validaciones" => ["requerido"]
        ],
		'cantidad' => [
            "tipo" => "int",
            "validaciones" => ["requerido"]
        ],
		'observacion' => [
            "tipo" => "text",
            "validaciones" => []
        ],
		'advertencia' => [
            "tipo" => "text",
            "validaciones" => []
        ],
    ]; 
    protected $table = "tratamiento";
    
    public function listado()
    {
        $conexion = new BaseDatos();
       
        $query = "SELECT id, nombre
        FROM {$this->table}"; 

        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC);  
        return $resultado;
    }
    
    public function getListadoPorHistoriaClinica($id_historia_clinica)
    {
        $conexion = new BaseDatos();
        $query = "SELECT 
        t.id,
        m.nombre,
        t.cantidad,
        t.inicio_tratamiento, 
        CONCAT(t.dosis,' ',un.nombre,' vía: ',va.nombre,' ',f.nombre) as administracion
        FROM tratamiento t
        INNER JOIN medicamentos m ON m.id = t.id_medicamentos
        INNER JOIN tratamiento_frecuencia f ON f.id = t.id_frecuencia
        INNER JOIN tratamiento_unidad_administracion un ON un.id = t.id_unidad_administracion
        INNER JOIN tratamiento_via_administracion va ON va.id = t.id_via_administracion
            WHERE t.id_historia_clinica={$id_historia_clinica}";  
        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC);  
        return $resultado;
    }
}