<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class PacienteModel extends Model
{
    public function __contruct()
    {
        parent::__contruct();
    }

    public function cargarPacientes()
    {
        $conexion = new BaseDatos();
        $limite_paginacion = LIMITE_PAGINACION;
        $router = new Router();
        $page = $router->getPage();
        $paginacion = ($page - 1) * ($this->paginacion);
        $query = "SELECT 
        usuarios.id,
        email,
        fecha_nacimiento,
        identificacion,
        nickname,
        nombre,
        apellido,
        discapacidad,
        estado,
        u_cita.*
        FROM usuarios
        INNER JOIN roles_usuarios ON roles_usuarios.usuario_id = usuarios.id
        LEFT JOIN (
            SELECT id_paciente, peso, estatura, presion, saturacion, hora as tiempo
            FROM historiaclinica
            INNER JOIN (
                SELECT id_paciente as id_paci,max(hora) as fecha
                FROM historiaclinica 
                GROUP BY id_paciente
            ) ultima_cita on 
            (
                ultima_cita.id_paci=historiaclinica.id_paciente
                AND
                ultima_cita.fecha=historiaclinica.hora 
            )
        ) u_cita ON u_cita.id_paciente = usuarios.id
        WHERE roles_usuarios.role_id = ".ID_ROL_PACIENTE. " LIMIT {$paginacion},{$limite_paginacion}";
        

        return $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
    }


    public function numeroPacientes()
    {
        $conexion = new BaseDatos();
        $limite_paginacion = LIMITE_PAGINACION;
        $router = new Router();
        $page = $router->getPage();
        $paginacion = ($page - 1) * ($this->paginacion);
        $query = "SELECT 
        count(*) as num
        FROM usuarios
        INNER JOIN roles_usuarios ON roles_usuarios.usuario_id = usuarios.id
        LEFT JOIN (
            SELECT id_paciente, peso, estatura, presion, saturacion, hora as tiempo
            FROM historiaclinica
            INNER JOIN (
                SELECT id_paciente as id_paci,max(hora) as fecha
                FROM historiaclinica 
                GROUP BY id_paciente
            ) ultima_cita on 
            (
                ultima_cita.id_paci=historiaclinica.id_paciente
                AND
                ultima_cita.fecha=historiaclinica.hora 
            )
        ) u_cita ON u_cita.id_paciente = usuarios.id
        WHERE roles_usuarios.role_id = ".ID_ROL_PACIENTE;
 
        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
        return $resultado[0]['num'];
    }
    
    public function cargarPacienteByCampos($campos)
    {
        $conexion = new BaseDatos();
        $query = "SELECT 
        {$campos}
        FROM usuarios
        INNER JOIN roles_usuarios ON roles_usuarios.usuario_id = usuarios.id
        WHERE roles_usuarios.role_id = ".ID_ROL_PACIENTE; 
        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
        return $resultado;
    }

    public function getAntecentes($id)
    {
        $conexion = new BaseDatos();
        $query = "SELECT 
            p.id, 
            CONCAT('[',c.nombre, '] ',a.nombre) AS nombre,
            p.observacion AS observacion
            FROM usuarios_antecedentes p
            INNER JOIN antecedentes a ON p.id_antecedente = a.id
            INNER JOIN antecedente_categoria c ON a.id_antecedente_categoria = c.id
            WHERE p.id_usuario = {$id}
            ORDER BY a.id_antecedente_categoria DESC";  
        $resultado = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
        return $resultado;
    }

    public function buscarPaciente($parametros){
         
        $conexion = new BaseDatos();
        $limite_paginacion = LIMITE_PAGINACION;
        $router = new Router();
        $page = $router->getPage();
        $paginacion = ($page - 1) * ($this->paginacion);
        $where = [];
        if( true ){
            $where[] = "`identificacion` like '%{$parametros['identificacion']}%'";
        }
        if( true ){
            $where[] = "`nombre` like '%{$parametros['nombre']}%'";
        }
        if( true ){
            $where[] = "`apellido` like '%{$parametros['apellido']}%'";
        } 
        
        if( $parametros['estado'] != "" ){
            $where[] = "`estado` = {$parametros['estado']}";
        }
        $where_text = implode(" and ", $where);
        $query = "SELECT id,email,identificacion,nombre,apellido,estado FROM `usuarios` WHERE {$where_text} LIMIT {$paginacion},{$limite_paginacion}"; 
        return $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC); 
    }

    public function buscarPacienteCount($parametros)
    {
        $conexion = new BaseDatos();
        $limite_paginacion = LIMITE_PAGINACION;
        $router = new Router();
        $page = $router->getPage();
        $paginacion = ($page - 1) * ($this->paginacion);
        $where = [];
        if( true ){
            $where[] = "`identificacion` like '%{$parametros['identificacion']}%'";
        }
        if( true ){
            $where[] = "`nombre` like '%{$parametros['nombre']}%'";
        }
        if( true ){
            $where[] = "`apellido` like '%{$parametros['apellido']}%'";
        } 
        
        if( $parametros['estado'] != "" ){
            $where[] = "`estado` = {$parametros['estado']}";
        }
        $where_text = implode(" and ", $where);
        $query = "SELECT count(*) as num FROM `usuarios` WHERE {$where_text}"; 
        $result = $conexion->db->query( $query )->fetch_all(MYSQLI_ASSOC);
        return $result[0]['num'];
    }
}
