<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "PermisosModel.php" );


class PermisosController extends Controller
{ 
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 
    public function exec($route, $params )
    {   
        $permiso = new PermisosModel();
        $permisos = $permiso->all(); 
        $numeroElementos = $permiso->numeroElementos(); 
        $page = $route->getPage();
        $params = array(
            "permisos" => $permisos,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params);
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR
 
    public function crear( $route, $params )
    {
        if( $route->metodo == "GET" ){ 
            $params = [];
            return $this->show(__FUNCTION__ , $params);
        }

        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido'],
                'detalle' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $permiso = new PermisosModel();
                $permiso->cargar_de_array($params); 
                $id_permiso = $permiso->save();
                 
                $this->session->setflash("Permiso se creo exitosamente","success");
                header("Location: /servicios/v1/permisos/");
                exit();
            }else{
                $permiso = new PermisosModel();
                $permiso->cargar_de_array($params); 

                $params = array(
                    "permiso" => $permiso,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }
        $this->show(__FUNCTION__, $params);
    }

    public function ver( $route, $params )
    { 
        $id_permiso = $params[0];
        $permiso = new PermisosModel();
        $permiso->get($id_permiso);  

        $params = array(
            "permiso" => $permiso
        ); 

        return $this->show(__FUNCTION__ , $params);  
    }

    public function editar($route, $params)
    {   

        if( $route->metodo == "GET" ){  
            $id_permiso = $params[0]; 
            $permiso = new PermisosModel();
            $permiso->get($id_permiso);  

            $params = array(
                "permiso" => $permiso,
            );

            return $this->show(__FUNCTION__ , $params);
        }

        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido'],
                'detalle' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $permiso = new PermisosModel();
                $permiso->cargar_de_array($params); 
                $id_permiso = $permiso->update();
                 
                $this->session->setflash("El permiso se actualizó exitosamente","success");
                header("Location: /servicios/v1/permisos/");
                exit();
            }else{
                $permiso = new PermisosModel();
                $permiso->cargar_de_array($params); 

                $params = array(
                    "permiso" => $permiso,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }        

    }    
    public function borrar($route, $params)
    {
        if( $route->metodo == "GET" ){   
            $id_permiso = $params[0]; 
            $permiso = new PermisosModel();
            $permiso->get($id_permiso);  

            $params = array(
                "permiso" => $permiso
            );
            return $this->show(__FUNCTION__ , $params);  
        }

        if( $route->metodo == "POST" ){
            $permiso = new PermisosModel();
            $permiso->get($params['id']);
            $permiso->delete();

            $this->session->setflash("El permiso se eliminó exitosamente","success");
            header("Location: /servicios/v1/permisos/");
            exit();

        }
    }
}
