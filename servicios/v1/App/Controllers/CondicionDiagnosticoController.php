<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "CondicionDiagnosticoModel.php" );


class CondicionDiagnosticoController extends Controller
{ 
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 
    public function exec($route, $params )
    {   
        $cdiagnostico = new CondicionDiagnosticoModel();
        $cdiagnosticos = $cdiagnostico->all(); 
        $numeroElementos = $cdiagnostico->numeroElementos(); 
        $page = $route->getPage();
        $params = array(
            "condiciondiagnosticos" => $cdiagnosticos,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params);
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR
 
    public function crear( $route, $params )
    {
        if( $route->metodo == "GET" ){ 
            $params = [];
            return $this->show(__FUNCTION__ , $params);
        }

        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $cdiagnostico = new CondicionDiagnosticoModel();
                $cdiagnostico->cargar_de_array($params); 
                $id_cdiagnostico = $cdiagnostico->save();
                 
                $this->session->setflash("Se creo exitosamente","success");
                header("Location: /servicios/v1/condiciondiagnostico/");
                exit();
            }else{
                $cdiagnostico = new CondicionDiagnosticoModel();
                $cdiagnostico->cargar_de_array($params); 

                $params = array(
                    "condiciondiagnostico" => $cdiagnostico,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }
        $this->show(__FUNCTION__, $params);
    }

    public function ver( $route, $params )
    { 
        $id_cdiagnostico = $params[0];
        $cdiagnostico = new CondicionDiagnosticoModel();
        $cdiagnostico->get($id_cdiagnostico);  

        $params = array(
            "condiciondiagnostico" => $cdiagnostico
        ); 

        return $this->show(__FUNCTION__ , $params);  
    }

    public function editar($route, $params)
    {   

        if( $route->metodo == "GET" ){  
            $id_cdiagnostico = $params[0]; 
            $cdiagnostico = new CondicionDiagnosticoModel();
            $cdiagnostico->get($id_cdiagnostico);  

            $params = array(
                "condiciondiagnostico" => $cdiagnostico,
            );

            return $this->show(__FUNCTION__ , $params);
        }



        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $cdiagnostico = new CondicionDiagnosticoModel();
                $cdiagnostico->cargar_de_array($params); 
                $id_cdiagnostico = $cdiagnostico->update();
                 
                $this->session->setflash("Se actualizó exitosamente","success");
                header("Location: /servicios/v1/condiciondiagnostico/");
                exit();
            }else{
                $cdiagnostico = new CondicionDiagnosticoModel();
                $cdiagnostico->cargar_de_array($params); 

                $params = array(
                    "condiciondiagnostico" => $cdiagnostico,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }        

    }    
    public function borrar($route, $params)
    {
        if( $route->metodo == "GET" ){   
            $id_cdiagnostico = $params[0]; 
            $cdiagnostico = new CondicionDiagnosticoModel();
            $cdiagnostico->get($id_cdiagnostico);  

            $params = array(
                "condiciondiagnostico" => $cdiagnostico
            );

            return $this->show(__FUNCTION__ , $params);  
        }

        if( $route->metodo == "POST" ){
            $cdiagnostico= new CondicionDiagnosticoModel();
            $cdiagnostico->get($params['id']);
            $cdiagnostico->delete();

            $this->session->setflash("Se eliminó exitosamente","success");
            header("Location: /servicios/v1/condiciondiagnostico/");
            exit();

        }
    }
}
