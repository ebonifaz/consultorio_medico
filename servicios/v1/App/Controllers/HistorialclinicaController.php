<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "RolesModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "HistorialclinicaModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "DiagnosticoModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "GrupoPrioritarioModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "TipoSangreModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "PacienteModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "DoctoresModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "UserModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "AntecendentesModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "AntecendentesUsuariosModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "MedicamentosModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "TratamientoViaAdministracion.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "TratamientoFrecuenciaModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "TratamientoUnidadAdministracionModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "TratamientoModel.php" );


class HistorialclinicaController extends Controller
{ 
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 
    public function exec($route, $params )
    {
        $historias = (new HistorialclinicaModel)->cargarHistorias();
        $numeroElementos = (new HistorialclinicaModel)->numeroHistorias(); 
        $page = $route->getPage(); 

        $params = array(
            "historias" => $historias,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params); 
        
    }

    public function buscar($route, $params )
    {   
        
        if( $route->metodo == "GET" ){ 
            $params = array();
            $this->show(__FUNCTION__, $params);
        }
        if( $route->metodo == "POST" ){ 
            $validaciones_historia_clinica = [ 
                // 'identificacion' => ['requerido']
            ];
            $validacion = new Validaciones($validaciones_historia_clinica,$params); 
            if( $validacion->estado ){
                // Presento la busqueda
                $historia_clinica = new HistorialclinicaModel;
                $listado_pacientes = $historia_clinica->buscarHistoriaClinica($params); 
                
                $params = array(
                    "listado_pacientes" => $listado_pacientes,
                    "parametros"        => $params
                );
            }else{
                // muestro el error
                echo "no voy a buscar ";
                return;
            }
            $this->show(__FUNCTION__, $params);
        }
        
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR
    public function crear($route, $params)
    {
        // $listado_historias = (new DiagnosticoModel)->cargarHistorias();
        $listado_tipo_sangre = (new TipoSangreModel)->cargarTipoSangre();        
        $listado_pacientes = (new PacienteModel)->cargarPacienteByCampos("usuarios.id,nombre");
        $listado_doctores = (new DoctoresModel)->cargarDoctorByCampos("usuarios.id,nombre");  
        $listado_grupo_prioritario = (new GrupoPrioritarioModel)->cargarByCampos("id,nombre");  

        if( $route->metodo == "GET" ){
            $params = array(
                // "listado_historias" => $listado_historias,
                "listado_tipo_sangre" => $listado_tipo_sangre,
                "listado_pacientes" => $listado_pacientes,
                "listado_doctores" => $listado_doctores,
                "listado_grupo_prioritario" => $listado_grupo_prioritario,                
                "parametros"        => $params
            );
            return $this->show(__FUNCTION__, $params);

        }
        if( $route->metodo == "POST" ){
            $historia_clinica_obj = new HistorialclinicaModel();
            $historia_clinica_obj->id_paciente = $params['id_paciente'];
            $historia_clinica_obj->id_doctor = $params['id_doctor'];
            $historia_clinica_obj->hora = $params['hora_solicitada_fecha'] . " " . $params['hora_solicitada_hora'];
            $id = $historia_clinica_obj->save();

            $paciente = new UserModel();
            $paciente->get( $params['id_paciente'] );
            $paciente->sexo = $params['sexo'];
            $paciente->fecha_nacimiento = $params['fecha_nacimiento'];
            $paciente->discapacidad = $params['discapacidad'];
            $paciente->id_tipo_sangre = $params['tipo_sangre'];
            $paciente->id_grupo_prioritario = $params['grupo_prioritario'];            
            $paciente->update(); 
 
            header("Location: /servicios/v1/historialclinica/editar/{$id}");
            exit();
        }        
        //$this->show(__FUNCTION__, $params);
    }
    
    public function editar($route, $params){

        $listado_antecedente = (new AntecendentesModel)->listado();
        $listado_diagnostico = (new DiagnosticoModel)->cargarHistorias();  
        $listado_medicamentos = (new MedicamentosModel)->listado();  
        $listado_viaadministracion = (new TratamientoViaAdministracion)->listado();  
        $listado_frecuenciamodel = (new TratamientoFrecuenciaModel)->listado();  
        $listado_unidadadministracionmodel = (new TratamientoUnidadAdministracionModel)->listado();  
        
        if( $route->metodo == "GET" ){
            $id_historia = $params[0];
            $historia_clinica_obj = new HistorialclinicaModel();
            $historia_clinica_obj->get( $id_historia );
 
            $params = array(
                "historia_clinica_obj" => $historia_clinica_obj,
                "listado_antecedente" => $listado_antecedente, 
                "listado_diagnostico" => $listado_diagnostico,
                "listado_medicamentos" => $listado_medicamentos,
                "listado_viaadministracion" => $listado_viaadministracion,
                "listado_frecuenciamodel" => $listado_frecuenciamodel,
                "listado_unidadadministracionmodel" => $listado_unidadadministracionmodel,                
            );
            return $this->show(__FUNCTION__, $params);
        }
        
        if( $route->metodo == "POST" ){
            if( array_key_exists("submit_motivo_consulta", $params ) ){
                $historia_clinica_obj = new HistorialclinicaModel();
                $historia_clinica_obj->get( $params['id'] );
                $historia_clinica_obj->motivo_consulta = $params['motivo_consulta'];
                $historia_clinica_obj->enfermedad_motivo_actual = $params['enfermedad_motivo_actual'];
                $historia_clinica_obj->update();
                $params = array(
                    "historia_clinica_obj" => $historia_clinica_obj, 
                    "listado_antecedente" => $listado_antecedente,
                    "listado_diagnostico" => $listado_diagnostico,
                    "listado_medicamentos" => $listado_medicamentos,
                    "listado_viaadministracion" => $listado_viaadministracion,
                    "listado_frecuenciamodel" => $listado_frecuenciamodel,
                    "listado_unidadadministracionmodel" => $listado_unidadadministracionmodel,          
                );
                $this->session->setflash("Se actualizó exitosamente","success");
                header("Location: /servicios/v1/historialclinica/editar/{$historia_clinica_obj->id}");
                exit();  
                
            }

            if( array_key_exists("submit_antecedente", $params ) ){
                $historia_clinica_obj = new HistorialclinicaModel();
                $historia_clinica_obj->get( $params['id'] );

                $nuevo_antecedente = new AntecendentesUsuariosModel();
                $nuevo_antecedente->id_usuario = $historia_clinica_obj->id_paciente;
                $nuevo_antecedente->id_antecedente = $params['id_antecedente'];
                if( array_key_exists("observacion", $params ) ){
                    $nuevo_antecedente->observacion = $params['observacion'];
                }
                $nuevo_antecedente->save();
  
                $params = array(
                    "historia_clinica_obj" => $historia_clinica_obj, 
                    "listado_antecedente" => $listado_antecedente,
                    "listado_diagnostico" => $listado_diagnostico,
                    "listado_medicamentos" => $listado_medicamentos,
                    "listado_viaadministracion" => $listado_viaadministracion,
                    "listado_frecuenciamodel" => $listado_frecuenciamodel,
                    "listado_unidadadministracionmodel" => $listado_unidadadministracionmodel,      
                );
                header("Location: /servicios/v1/historialclinica/editar/{$historia_clinica_obj->id}");
                exit();  
            }

            if( array_key_exists("submit_antecedente_eliminar", $params ) ){ 
                $historia_clinica_obj = new HistorialclinicaModel();
                $historia_clinica_obj->get( $params['id_historia'] );

                $nuevo_antecedente = new AntecendentesUsuariosModel();
                try {
                    $nuevo_antecedente->get( $params['id'] ); 
                    $nuevo_antecedente->delete();
                } catch (\Throwable $th) {
                    //throw $th;
                }

                $params = array(
                    "historia_clinica_obj" => $historia_clinica_obj, 
                    "listado_antecedente" => $listado_antecedente,
                    "listado_diagnostico" => $listado_diagnostico,
                    "listado_medicamentos" => $listado_medicamentos,
                    "listado_viaadministracion" => $listado_viaadministracion,
                    "listado_frecuenciamodel" => $listado_frecuenciamodel,
                    "listado_unidadadministracionmodel" => $listado_unidadadministracionmodel,           
                );
                header("Location: /servicios/v1/historialclinica/editar/{$historia_clinica_obj->id}");
                exit();  
            }

            if( array_key_exists("submit_signos_vitales", $params ) ){
                $historia_clinica_obj = new HistorialclinicaModel();
                $historia_clinica_obj->get( $params['id'] );
                $historia_clinica_obj->signos_vitales_sistolica = $params['signos_vitales_sistolica'];
                $historia_clinica_obj->signos_vitales_diastolica = $params['signos_vitales_diastolica'];
                $historia_clinica_obj->temperatura = $params['temperatura'];
                $historia_clinica_obj->frecuencia_respiratoria = $params['frecuencia_respiratoria'];
                $historia_clinica_obj->frecuencia_cardiaca = $params['frecuencia_cardiaca'];
                $historia_clinica_obj->saturacion = $params['saturacion'];
                $historia_clinica_obj->peso = $params['peso'];
                $historia_clinica_obj->estatura = $params['estatura'];
                $historia_clinica_obj->perimetro_abdominal = $params['perimetro_abdominal'];
                $historia_clinica_obj->glucosa_capilar = $params['glucosa_capilar'];
                $historia_clinica_obj->valor_hemoglobina = $params['valor_hemoglobina'];
                $historia_clinica_obj->update();
                

                header("Location: /servicios/v1/historialclinica/editar/{$historia_clinica_obj->id}");
                exit();  
            }

            if( array_key_exists("submit_diagnostico", $params ) ){
                $historia_clinica_obj = new HistorialclinicaModel();
                $historia_clinica_obj->get( $params['id'] );
                $historia_clinica_obj->observacion_diagnostico = $params['observacion_diagnostico'];
                $historia_clinica_obj->id_diagnosticos = $params['id_diagnosticos'];
                $historia_clinica_obj->update();

                header("Location: /servicios/v1/historialclinica/editar/{$historia_clinica_obj->id}");
                exit();  

            }

            if( array_key_exists("submit_tratamientos", $params ) ){

                $historia_clinica_obj = new HistorialclinicaModel();
                $historia_clinica_obj->get( $params['id'] );

                $tratamientomodel_obj = new TratamientoModel(); 
                $tratamientomodel_obj->id_historia_clinica = $params['id'];
                $tratamientomodel_obj->id_medicamentos = $params['id_medicamentos'];
                $tratamientomodel_obj->id_via_administracion = $params['id_via_administracion'];
                $tratamientomodel_obj->dosis = $params['dosis'];
                $tratamientomodel_obj->id_unidad_administracion = $params['id_unidad_administracion'];
                $tratamientomodel_obj->id_frecuencia = $params['id_frecuencia'];
                $tratamientomodel_obj->inicio_tratamiento = $params['dosis_unitaria_fecha'] . " " . $params['dosis_unitaria_hora'];
                $tratamientomodel_obj->cantidad = $params['cantidad'];
                $tratamientomodel_obj->observacion = $params['observacion'];
                $tratamientomodel_obj->advertencia = $params['advertencia'];
                $tratamientomodel_obj->save();
            
                header("Location: /servicios/v1/historialclinica/editar/{$historia_clinica_obj->id}");
                exit();  
            } 

            if( array_key_exists("submit_tratamiento_eliminar", $params ) ){ 
                $historia_clinica_obj = new HistorialclinicaModel();
                $historia_clinica_obj->get( $params['id_historia'] );

                $tratamientomodel_obj = new TratamientoModel(); 
                $tratamientomodel_obj->get( $params['id'] ); 
                $tratamientomodel_obj->delete();

                header("Location: /servicios/v1/historialclinica/editar/{$historia_clinica_obj->id}");
                exit();  

            }

            
        }
    }

    public function borrar( $route,$params )
    {
        if( $route->metodo == "GET" ){
            $id_historia = $params[0];
            $historia_clinica_obj = new HistorialclinicaModel();
            $historia_clinica_obj->get( $id_historia );
            $params = array( 
                "historia_clinica_obj" => $historia_clinica_obj
            );
            return $this->show(__FUNCTION__, $params);
        }
        if( $route->metodo == "POST" ){ 
            $historia_clinica_obj = new HistorialclinicaModel();
            $historia_clinica_obj->get( $params['id'] );
            $historia_clinica_obj->delete(); 

            $this->session->setflash("Historia Clinica se borro exitosamente","success");
            header("Location: /servicios/v1/historialclinica/");
            exit();
        }
    }
}
