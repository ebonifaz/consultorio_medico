<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "CronologiaModel.php" );


class CronologiaController extends Controller
{ 
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 
    public function exec($route, $params )
    {   
        $cronologia = new CronologiaModel();
        $cronologias = $cronologia->all(); 
        $numeroElementos = $cronologia->numeroElementos(); 
        $page = $route->getPage();
        $params = array(
            "cronologia" => $cronologias,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params);
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR
 
    public function crear( $route, $params )
    {
        if( $route->metodo == "GET" ){ 
            $params = [];
            return $this->show(__FUNCTION__ , $params);
        }

        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $cronologia = new CronologiaModel();
                $cronologia->cargar_de_array($params); 
                $id_cronologia = $cronologia->save();
                 
                $this->session->setflash("Cronología se creo exitosamente","success");
                header("Location: /servicios/v1/cronologia/");
                exit();
            }else{
                $cronologia = new CronologiaModel();
                $cronologia->cargar_de_array($params); 

                $params = array(
                    "cronologia" => $cronologia,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }
        $this->show(__FUNCTION__, $params);
    }

    public function ver( $route, $params )
    { 
        $id_cronologia = $params[0];
        $cronologia = new CronologiaModel();
        $cronologia->get($id_cronologia);  

        $params = array(
            "cronologia" => $cronologia
        ); 

        return $this->show(__FUNCTION__ , $params);  
    }

    public function editar($route, $params)
    {   

        if( $route->metodo == "GET" ){  
            $id_cronologia = $params[0]; 
            $cronologia = new CronologiaModel();
            $cronologia->get($id_cronologia);  

            $params = array(
                "cronologia" => $cronologia,
            );

            return $this->show(__FUNCTION__ , $params);
        }



        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $cronologia = new CronologiaModel();
                $cronologia->cargar_de_array($params); 
                $id_cronologia = $cronologia->update();
                 
                $this->session->setflash("La cronología se actualizó exitosamente","success");
                header("Location: /servicios/v1/cronologia/");
                exit();
            }else{
                $cronologia = new CronologiaModel();
                $cronologia->cargar_de_array($params); 

                $params = array(
                    "cronologia" => $cronologia,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }        

    }    
    public function borrar($route, $params)
    {
        if( $route->metodo == "GET" ){   
            $id_cronologia = $params[0]; 
            $cronologia = new CronologiaModel();
            $cronologia->get($id_cronologia);  

            $params = array(
                "cronologia" => $cronologia
            );

            return $this->show(__FUNCTION__ , $params);  
        }

        if( $route->metodo == "POST" ){
            $cronologia= new CronologiaModel();
            $cronologia->get($params['id']);
            $cronologia->delete();

            $this->session->setflash("La cronología se eliminó exitosamente","success");
            header("Location: /servicios/v1/cronologia/");
            exit();

        }
    }
}
