<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "MedicamentosModel.php" );


class MedicamentosController extends Controller
{ 
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 
    public function exec($route, $params )
    {   
        $medicamento = new MedicamentosModel();
        $medicamentos = $medicamento->all(); 
        $numeroElementos = $medicamento->numeroElementos(); 
        $page = $route->getPage();
        $params = array(
            "medicamentos" => $medicamentos,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params);
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR
 
    public function crear( $route, $params )
    {
        if( $route->metodo == "GET" ){ 
            $params = [];
            return $this->show(__FUNCTION__ , $params);
        }

        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $medicamento = new MedicamentosModel();
                $medicamento->cargar_de_array($params); 
                $id_medicamento = $medicamento->save();
                 
                $this->session->setflash("El medicamento ha sido registrado exitosamente","success");
                header("Location: /servicios/v1/medicamentos/");
                exit();
            }else{
                $medicamento = new MedicamentosModel();
                $medicamento->cargar_de_array($params); 

                $params = array(
                    "medicamento" => $medicamento,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }
        $this->show(__FUNCTION__, $params);
    }

    public function ver( $route, $params )
    { 
        $id_medicamento = $params[0];
        $medicamento = new MedicamentosModel();
        $medicamento->get($id_medicamento);  

        $params = array(
            "medicamento" => $medicamento
        ); 

        return $this->show(__FUNCTION__ , $params);  
    }

    public function editar($route, $params)
    {   

        if( $route->metodo == "GET" ){  
            $id_medicamento = $params[0]; 
            $medicamento = new MedicamentosModel();
            $medicamento->get($id_medicamento);  

            $params = array(
                "medicamento" => $medicamento,
            );

            return $this->show(__FUNCTION__ , $params);
        }



        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $medicamento = new MedicamentosModel();
                $medicamento->cargar_de_array($params); 
                $id_medicamento = $medicamento->update();
                 
                $this->session->setflash("El medicamento se actualizó correctamente","success");
                header("Location: /servicios/v1/medicamentos/");
                exit();
            }else{
                $medicamento = new MedicamentosModel();
                $medicamento->cargar_de_array($params); 

                $params = array(
                    "medicamento" => $medicamento,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }        

    }    
    public function borrar($route, $params)
    {
        if( $route->metodo == "GET" ){   
            $id_medicamento = $params[0]; 
            $medicamento = new MedicamentosModel();
            $medicamento->get($id_medicamento);  

            $params = array(
                "medicamento" => $medicamento
            );

            return $this->show(__FUNCTION__ , $params);  
        }

        if( $route->metodo == "POST" ){
            $medicamento= new MedicamentosModel();
            $medicamento->get($params['id']);
            $medicamento->delete();

            $this->session->setflash("El medicamento se eliminó correctamente","success");
            header("Location: /servicios/v1/medicamentos/");
            exit();

        }
    }
}
