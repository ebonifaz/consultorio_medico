<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "DiagnosticoModel.php" );


class DiagnosticosController extends Controller
{ 
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 
    public function exec($route, $params )
    {   
        $diagnostico = new DiagnosticoModel();
        $diagnosticos = $diagnostico->all(); 
        $numeroElementos = $diagnostico->numeroElementos(); 
        $page = $route->getPage();
        $params = array(
            "diagnostico" => $diagnosticos,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params);
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR
 
    public function crear( $route, $params )
    {
        if( $route->metodo == "GET" ){ 
            $params = [];
            return $this->show(__FUNCTION__ , $params);
        }

        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido'],
                'codigo_cie' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $diagnostico = new DiagnosticoModel();
                $diagnostico->cargar_de_array($params); 
                $id_diagnostico = $diagnostico->save();
                 
                $this->session->setflash("Cronología se creo exitosamente","success");
                header("Location: /servicios/v1/diagnosticos/");
                exit();
            }else{
                $diagnostico = new DiagnosticoModel();
                $diagnostico->cargar_de_array($params); 

                $params = array(
                    "diagnostico" => $diagnostico,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }
        $this->show(__FUNCTION__, $params);
    }

    public function ver( $route, $params )
    { 
        $id_diagnostico = $params[0];
        $diagnostico = new DiagnosticoModel();
        $diagnostico->get($id_diagnostico);  

        $params = array(
            "diagnostico" => $diagnostico
        ); 

        return $this->show(__FUNCTION__ , $params);  
    }

    public function editar($route, $params)
    {   

        if( $route->metodo == "GET" ){  
            $id_diagnostico = $params[0]; 
            $diagnostico = new DiagnosticoModel();
            $diagnostico->get($id_diagnostico);  

            $params = array(
                "diagnostico" => $diagnostico,
            );

            return $this->show(__FUNCTION__ , $params);
        }



        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido'],
                'codigo_cie' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $diagnostico = new DiagnosticoModel();
                $diagnostico->cargar_de_array($params); 
                $id_diagnostico = $diagnostico->update();
                 
                $this->session->setflash("La diagnostico se actualizó exitosamente","success");
                header("Location: /servicios/v1/diagnosticos/");
                exit();
            }else{
                $diagnostico = new DiagnosticoModel();
                $diagnostico->cargar_de_array($params); 

                $params = array(
                    "diagnostico" => $diagnostico,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }        

    }    
    public function borrar($route, $params)
    {
        if( $route->metodo == "GET" ){   
            $id_diagnostico = $params[0]; 
            $diagnostico = new DiagnosticoModel();
            $diagnostico->get($id_diagnostico);  

            $params = array(
                "diagnostico" => $diagnostico
            );

            return $this->show(__FUNCTION__ , $params);  
        }

        if( $route->metodo == "POST" ){
            $diagnostico= new DiagnosticoModel();
            $diagnostico->get($params['id']);
            $diagnostico->delete();

            $this->session->setflash("El diagnostico se eliminó exitosamente","success");
            header("Location: /servicios/v1/diagnosticos/");
            exit();

        }
    }
}
