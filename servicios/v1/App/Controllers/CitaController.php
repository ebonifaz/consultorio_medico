<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "CitasModel.php" ); 
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "PacienteModel.php" ); 
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "DoctoresModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "EstadoCita.php" );

class CitaController extends Controller{
    
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    }  

    public function exec( $route, $params )
    {    
        $citas = (new CitasModel)->cargarCitas();
        $numeroElementos = (new CitasModel)->numeroCitas(); 
        $page = $route->getPage();

        $params = array(
            "citas" => $citas,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params); 
    } 

    public function crear( $route, $params )
    {
        $pacientes = (new PacienteModel)->cargarPacienteByCampos("usuarios.id,nombre");
        $doctores = (new DoctoresModel)->cargarDoctorByCampos("usuarios.id,nombre"); 
        $estadocita = (new EstadoCita)->cargarEstadoCitaByCampos("id,nombre"); 
 
        if( $route->metodo == "GET" ){   

            $params = array( 
                "listado_pacientes" => $pacientes,
                "listado_doctores" => $doctores,
                "listado_estadocita" => $estadocita,
            ); 

            return $this->show(__FUNCTION__ , $params);
        }
        
        if ($route->metodo == "POST") {
            $validaciones_editar = [
                'id_paciente' => ['requerido', 'paciente', 'numerico'],
                'hora_solicitada_fecha' => ['requerido', 'fecha'],
                'id_doctor' => ['requerido', 'doctor', 'numerico'],
                'hora_solicitada_hora' => ['requerido', 'hora'],
                'id_estado_cita' => ['requerido' , 'estado', 'agendada' , 'numerico']
            ]; 
            $validacion = new Validaciones($validaciones_editar,$params);

            if ($validacion->estado) {

                
                $cita = new CitasModel();
                $cita->cargar_de_array($params);  
                $cita->hora_solicitada = $params['hora_solicitada_fecha'] . ' '. $params['hora_solicitada_hora']; 
                $cita->save();

                $this->session->setflash("La cita se creo exitosamente","success");
                header("Location: /servicios/v1/cita/");
                exit(); 

            }else{ 
                $cita = new CitasModel();
                $cita->cargar_de_array($params); 
                $cita->hora_solicitada = $params['hora_solicitada_fecha'] . ' '. $params['hora_solicitada_hora']; 

                $params = array(
                    "cita" => $cita,
                    "validacion" => $validacion, 
                    "listado_pacientes" => $pacientes,
                    "listado_doctores" => $doctores,
                    "listado_estadocita" => $estadocita,
                );
                return $this->show(__FUNCTION__ , $params);
            } 
        }

    }
    public function editar( $route, $params )
    {
        $pacientes = (new PacienteModel)->cargarPacienteByCampos("usuarios.id,nombre");
        $doctores = (new DoctoresModel)->cargarDoctorByCampos("usuarios.id,nombre"); 
        $estadocita = (new EstadoCita)->cargarEstadoCitaByCampos("id,nombre"); 
 

        if( $route->metodo == "GET" ){  
            $id_cita = $params[0];
 
            $cita = new CitasModel();
            $cita->get($id_cita);

            $params = array(
                "cita" => $cita,
                "listado_pacientes" => $pacientes,
                "listado_doctores" => $doctores,
                "listado_estadocita" => $estadocita,
            ); 

            return $this->show(__FUNCTION__ , $params);
        }

        if( $route->metodo == "POST" ){  
            $validaciones_editar = [
                'id' => ['requerido', 'numerico'],
                'id_paciente' => ['requerido', 'paciente', 'numerico'],
                'hora_solicitada_fecha' => ['requerido', 'fecha'],
                'id_doctor' => ['requerido', 'doctor', 'numerico'],
                'hora_solicitada_hora' => ['requerido', 'hora'],
                'id_estado_cita' => ['requerido' , 'estado', 'agendada' , 'numerico']
            ]; 
            $validacion = new Validaciones($validaciones_editar,$params); 
            
            if( $validacion->estado ){
                $cita = new CitasModel();
                $cita->cargar_de_array($params);  
                $cita->hora_solicitada = $params['hora_solicitada_fecha'] . ' '. $params['hora_solicitada_hora']; 
                $cita->update();
                
                $this->session->setflash("El rol se actualizó exitosamente","success");
                header("Location: /servicios/v1/cita/");
                exit();
            }else{
                $cita = new CitasModel();
                $cita->cargar_de_array($params); 
                $cita->hora_solicitada = $params['hora_solicitada_fecha'] . ' '. $params['hora_solicitada_hora']; 

                $params = array(
                    "cita" => $cita,
                    "validacion" => $validacion, 
                    "listado_pacientes" => $pacientes,
                    "listado_doctores" => $doctores,
                    "listado_estadocita" => $estadocita,
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }
    }

    public function ver ( $route, $params )
    {
        if( $route->metodo == "GET" ){  
            $id_cita = $params[0];
 
            $cita = new CitasModel();
            $cita->get($id_cita);
            

            $params = array(
                "cita" => $cita
            ); 

            return $this->show(__FUNCTION__ , $params);
        
        }
    }

    public function borrar($route, $params)
    {
        if( $route->metodo == "GET" ){   
            $id_cita = $params[0];
 
            $cita = new CitasModel();
            $cita->get($id_cita);

            $params = array(
                "cita" => $cita
            );
            return $this->show(__FUNCTION__ , $params);  
        }

        if( $route->metodo == "POST" ){
            $cita = new CitasModel();
            $cita->get($params['id']);
            $cita->delete();

            $this->session->setflash("Cita se eliminó exitosamente","success");
            header("Location: /servicios/v1/cita/");
            exit();

        }
    }

    public function listadojson( $route,$params )
    {
        if( $route->metodo == "GET" ){

            $listado_citas = (new CitasModel())->listado_citas();
            echo json_encode( $listado_citas );
            exit();
        }
    }

    public function agenda( $route, $params )
    {
        return $this->show(__FUNCTION__ , $params);  
    }

    public function doctor( $route, $params )
    {
        $id_doctor = $params[0];
        $params = array(
            "id_doctor" => $id_doctor
        );
        return $this->show(__FUNCTION__ , $params);  
    }
    public function listadobydoctorjson($route, $params)
    {
        if( $route->metodo == "GET" ){

            $atr_id_doctor = $params[0]; 
            $id_doctor = explode("?",$atr_id_doctor); 
            $params = array(
                "id_doctor" => $id_doctor[0]
            );
            $listado_citas = (new CitasModel())->listado_citas_by_doctor($id_doctor[0]);
            echo json_encode( $listado_citas );
            exit();
        }
    }
}
