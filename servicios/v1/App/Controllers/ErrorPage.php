<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "HomeModel.php" );


class ErrorPage extends Controller
{
	private $model;
    public function __construct()
    {
        $this->model = new HomeModel();
    }

    public function show($funcion, $params)
    {
        $params = array("parametros" => $params);
		$this->render(__CLASS__, $funcion , $params);
    }
    public function exec( $route, $params )
    {    
        $this->show('404', $params);
    }
}
