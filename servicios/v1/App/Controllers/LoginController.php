<?php
defined('BASEPATH') or exit('No se permite acceso directo');

// require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . "App/Controllers/HomeController.php");
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "HomeModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "UserModel.php" ); 
require_once( LIBS_ROUTE . "Session.php" ); 

class LoginController extends Controller
{
	private $model;
    private $session;
    private $response = array(
        'response'=>false,
        'mensaje' => "",
        'ruta' => ""
    );
    public function __construct()
    {
        $this->model = new UserModel();
        $this->session = new Session();
    } 

    public function show($funcion, $params)
    {
        $params = array("parametros" => $params);
		$this->render(__CLASS__, $funcion , $params);
    }

    public function ingreso( $route, $params )
    { 
        if( $this->validarCampos($params) ){  
            return $this->renderErrorMensaje($route, "El email y password son obligatorios");  // Valida que los datos están ingresados
        }
        $email = $params['correo'];  
        $result = $this->model->ingreso( $email );
        if( ! $result->num_rows ){ 
            return $this->renderErrorMensaje($route, "El email {$email} no fue encontrado"); // Valida que sea un mail válido
        } 
        $usuario = $result->fetch_object(); 
        if( ! password_verify($params['contrasenia'], $usuario->password) ){ 
            return $this->renderErrorMensaje($route, "Contraseña inválida"); // Valida que sea un mail válido
        }
        
        return $this->renderSuccessMensaje($route,$usuario, "Ingreso exitoso");

    }

    public function validarCampos($params){
        return empty($params['correo']) OR empty($params['contrasenia']);
    }
    
    public function renderErrorMensaje($route, $mensaje)
    {
        if( $route->metodo == "POST"){
            $this->response['mensaje'] = $mensaje;
            $this->response['response'] = false;
            header('Content-Type: application/json');
            echo json_encode( $this->response );
            return;
        }else{
            $params = array("mensaje_error" => $mensaje); 
            $this->render("HomeController","exec",$params);
        } 
    }
    
    public function renderSuccessMensaje($route,$usuario, $mensaje)
    {
        $this->session->init();
        $this->session->add(USER_EMAIL,$usuario->email);
        $this->session->add(USER_NICKNAME,$usuario->nickname);
        $this->session->add(USER_ID,$usuario->id);
        if( $route->metodo == "POST"){
            $this->response['mensaje'] = $mensaje;
            $this->response['response'] = true;
            $this->response['ruta'] = PATH_DASHBOARD;
            $this->response['USER_NICKNAME'] = $this->session->get(USER_NICKNAME);
            header('Content-Type: application/json');
            echo json_encode( $this->response );
            return;
        }else{
            header('location: /servicios/v1/dashboard'); 
        }
    }

    public function exec( $route, $params )
    {    
        $this->show(__FUNCTION__, $params);
    }
}
