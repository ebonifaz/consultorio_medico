<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "UserModel.php" );


class UsuariosController extends Controller
{
	private $model;
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 

    public function getuser($route,$id)
    {
        $user = $this->model->getUser($id); 
        $this->show('exec', $user);
    }

    public function show($funcion, $params)
    {
        $params = array("parametros" => $params);
		$this->render(__CLASS__, $funcion , $params);
    }
    
    public function exec($route, $params )
    {   
        $usuario = new UserModel();
        $usuarios = $usuario->all_con_roles(); 
        $numeroElementos = $usuario->numeroElementos(); 
        $page = $route->getPage();
        $params = array(
            "usuarios" => $usuarios,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params);
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR

    // Primer parametro siempre es el id del usuario
    public function editar($route, $params)
    { 
        $roles = new RolesModel();
        $listado_roles = $roles->all();

        if( $route->metodo == "GET" ){  
            $id_user = $params[0];

            // traer información del usuario 
            $usuario = new UserModel();
            $usuario->get($id_user); 
            $usuario->getRoles();  
 
            // Generar parametros a enviar a la vista
            $params = array(
                "usuario" => $usuario,
                "listado_roles" => $listado_roles,
            );

            return $this->show(__FUNCTION__ , $params);
        }
        if( $route->metodo == "POST" ){ 
            $validaciones_editar = [
                'email' => ['requerido','email' ],
                'nickname' => ['requerido'],
                'nombre' => [''],
                'apellido' => [''],
                'estado' => ['requerido'],
                'identificacion' => ['requerido']
            ]; 
            $params['estado'] = ( array_key_exists('estado',$params) )? $params['estado'] : 0;
            $validacion = new Validaciones($validaciones_editar,$params);
  
            if( $validacion->estado ){
                $usuario = new UserModel();
                $usuario->cargar_de_array($params);
                $usuario->set_contrasenia($params['contrasenia']); 
                $usuario->update();

                $roles = $usuario->roles; 
                $usuario->setRolesYUSuarios( $params['roles'] ); 

                
                $this->session->setflash("Usuario se actualizó exitosamente","success");
                header("Location: http://consultorioclinico.test/servicios/v1/usuarios");
                exit();
            }else{ 
                $usuario = new UserModel();
                $usuario->cargar_de_array($params);
    
                if( array_key_exists('roles',$params) ){
                    foreach ($params['roles'] as $rol) {
                        $rolitem = new RolesModel();
                        $rolitem->get($rol);
                        $usuario->roles[] = $rolitem;
                    }
                }

                $params = array(
                    "usuario" => $usuario,
                    "validacion" => $validacion, 
                    "listado_roles" => $listado_roles,
                );
            } 

            return $this->show(__FUNCTION__ , $params); 
        } 
        
    }

    public function ver($route, $params)
    {
        $id_user = $params[0];

        // traer información del usuario 
        $usuario = new UserModel();
        $usuario->get($id_user); 
        $usuario->getRoles();  

        $params = array(
            "usuario" => $usuario
        ); 

        return $this->show(__FUNCTION__ , $params);  
    }

    public function borrar($route, $params)
    {
        if( $route->metodo == "GET" ){   
            $id_user = $params[0];
     
            $usuario = new UserModel();
            $usuario->get($id_user); 

            $params = array(
                "usuario" => $usuario
            );
            return $this->show(__FUNCTION__ , $params);  
        }

        if( $route->metodo == "POST" ){
            $usuario = new UserModel();
            $usuario->get($params['id']);
            $usuario->delete();

            $this->session->setflash("Usuario se eliminó exitosamente","success");
            header("Location: http://consultorioclinico.test/servicios/v1/usuarios");
            exit();

        }
    }

    public function crear($route, $params)
    {
        $roles = new RolesModel();
        $listado_roles = $roles->all();
        if( $route->metodo == "GET" ){  
 
            // Generar parametros a enviar a la vista
            $params = array( 
                "listado_roles" => $listado_roles,
            );

            return $this->show(__FUNCTION__ , $params);
        }
        if( $route->metodo == "POST" ){
            $validaciones_editar = [
                'email' => ['requerido',''],
                'nickname' => ['requerido'],
                'nombre' => [''],
                'apellido' => [''],
                'estado' => ['requerido'],
                'identificacion' => ['requerido']
            ];
            
            $params['estado'] = ( array_key_exists('estado',$params) )? $params['estado'] : 0;
            $validacion = new Validaciones($validaciones_editar,$params); 

            if( $validacion->estado ){
                $usuario = new UserModel();
                $usuario->cargar_de_array($params);
                $usuario->set_contrasenia($params['contrasenia']); 
                $id_user = $usuario->save();

                // $roles = $usuario->roles;  
                $usuario->get($id_user);
                $usuario->setRolesYUSuarios( $params['roles'] ); 
                $usuario->getRoles(); 
                $params = array(
                    "usuario" => $usuario,
                    "success" => "1",
                    "success_mensaje" => "El usuario se actualizó existosamente",
                    "listado_roles" => $listado_roles,
                );

                $this->session->setflash("Usuario se creo exitosamente","success");
                header("Location: http://consultorioclinico.test/servicios/v1/usuarios");
                exit();
            }else{ 
                $usuario = new UserModel();
                $usuario->cargar_de_array($params);
    
                if( array_key_exists('roles',$params) ){
                    foreach ($params['roles'] as $rol) {
                        $rolitem = new RolesModel();
                        $rolitem->get($rol);
                        $usuario->roles[] = $rolitem;
                    }
                }

                $params = array(
                    "usuario" => $usuario,
                    "validacion" => $validacion, 
                    "listado_roles" => $listado_roles,
                );
            } 

            return $this->show(__FUNCTION__ , $params); 
            echo "holi";
        }

    }
}
