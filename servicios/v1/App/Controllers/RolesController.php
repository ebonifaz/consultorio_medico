<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "RolesModel.php" );
require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "PermisosModel.php" );


class RolesController extends Controller
{ 
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 
    public function exec( $route, $params )
    {   
        $rol = new RolesModel();
        $roles = $rol->all_con_permisos(); 
        $numeroElementos = $rol->numeroElementos(); 
        $page = $route->getPage();
        $params = array(
            "roles" => $roles,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params);
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR
    public function crear( $route, $params )
    {
        $permisos = new PermisosModel();
        $listado_permisos = $permisos->all();

        if( $route->metodo == "GET" ){ 
            $params = array( 
                "listado_permisos" => $listado_permisos,
            ); 
            return $this->show(__FUNCTION__ , $params);
        }
        //$this->show(__FUNCTION__, $params);
        if ($route->metodo == "POST") {
            $validaciones_editar = [
                'nombre' => ['requerido']
            ];
            
            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado) {
                $rol = new RolesModel();
                $rol->cargar_de_array($params); 
                $id_rol = $rol->save();
                 
                $permisos = $rol->permisos;
                $rol->get($id_rol);

                $rol->setPermisosYRoles($params['permisos']);
                 
                $this->session->setflash("Rol se creo exitosamente","success");
                header("Location: /servicios/v1/roles/");
                exit();
            }else{ 
                $rol = new RolesModel();
                $rol->cargar_de_array($params); 
    
                if( array_key_exists('permisos',$params) ){
                    foreach ($params['permisos'] as $rol) {
                        $rolitem = new RolesModel();
                        $rolitem->get($rol);
                        $rol->roles[] = $rolitem;
                    }
                }

                $params = array(
                    "rol" => $rol,
                    "validacion" => $validacion, 
                    "listado_permisos" => $listado_permisos,
                );
                return $this->show(__FUNCTION__ , $params);
            } 
        }

    }
    public function ver( $route, $params )
    { 
        $id_rol = $params[0];
        // traer información del usuario 
        $rol = new RolesModel();
        $rol->get($id_rol); 
        $rol->getPermisos();  

        $params = array(
            "rol" => $rol
        ); 

        return $this->show(__FUNCTION__ , $params);  
    }

    public function editar($route, $params)
    { 
        $permisos = new PermisosModel();
        $listado_permisos = $permisos->all();

        if( $route->metodo == "GET" ){  
            $id_rol = $params[0];
 
            $rol = new RolesModel();
            $rol->get($id_rol); 
            $rol->getPermisos();  

            $params = array(
                "rol" => $rol,
                "listado_permisos" => $listado_permisos
            );

            return $this->show(__FUNCTION__ , $params);
        }

        if( $route->metodo == "POST" ){  
            $validaciones_editar = [
                'nombre' => ['requerido']
            ]; 
            $validacion = new Validaciones($validaciones_editar,$params); 
            
            if( $validacion->estado ){
                $rol = new RolesModel();
                $rol->cargar_de_array($params); 
                $rol->update();
                
                $rol->setPermisosYRoles($params['permisos']);
                 
                $this->session->setflash("El rol se actualizó exitosamente","success");
                header("Location: /servicios/v1/roles/");
                exit();
            }else{
                $rol = new RolesModel();
                $rol->cargar_de_array($params); 
    
                if( array_key_exists('permisos',$params) ){
                    foreach ($params['permisos'] as $rol) {
                        $rolitem = new RolesModel();
                        $rolitem->get($rol);
                        $rol->roles[] = $rolitem;
                    }
                }

                $params = array(
                    "rol" => $rol,
                    "validacion" => $validacion, 
                    "listado_permisos" => $listado_permisos,
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }
    }    
    public function borrar($route, $params)
    {
        if( $route->metodo == "GET" ){   
            $id_rol = $params[0];
     
            $rol = new RolesModel();
            $rol->get($id_rol); 

            $params = array(
                "rol" => $rol
            );
            return $this->show(__FUNCTION__ , $params);  
        }

        if( $route->metodo == "POST" ){
            $rol = new RolesModel();
            $rol->get($params['id']);
            $rol->delete();

            $this->session->setflash("Rol se eliminó exitosamente","success");
            header("Location: /servicios/v1/roles/");
            exit();

        }
    }
}