<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "GrupoPrioritarioModel.php" );


class GrupoPrioritarioController extends Controller
{ 
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 
    public function exec($route, $params )
    {   
        $grupop = new GrupoPrioritarioModel();
        $gruposp = $grupop->all(); 
        $numeroElementos = $grupop->numeroElementos(); 
        $page = $route->getPage();
        $params = array(
            "gruposprioritario" => $gruposp,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params);
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR
 
    public function crear( $route, $params )
    {
        if( $route->metodo == "GET" ){ 
            $params = [];
            return $this->show(__FUNCTION__ , $params);
        }

        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $grupop = new GrupoPrioritarioModel();
                $grupop->cargar_de_array($params); 
                $id_grupop = $grupop->save();
                 
                $this->session->setflash("Grupo Prioritario se creó exitosamente","success");
                header("Location: /servicios/v1/grupoprioritario/");
                exit();
            }else{
                $grupop = new GrupoPrioritarioModel();
                $grupop->cargar_de_array($params); 

                $params = array(
                    "grupoprioritario" => $grupop,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }
        $this->show(__FUNCTION__, $params);
    }

    public function ver( $route, $params )
    { 
        $id_grupop = $params[0];
        $grupop = new GrupoPrioritarioModel();
        $grupop->get($id_grupop);  

        $params = array(
            "grupoprioritario" => $grupop
        ); 

        return $this->show(__FUNCTION__ , $params);  
    }

    public function editar($route, $params)
    {   

        if( $route->metodo == "GET" ){  
            $id_grupop = $params[0]; 
            $grupop = new GrupoPrioritarioModel();
            $grupop->get($id_grupop);  

            $params = array(
                "grupoprioritario" => $grupop,
            );

            return $this->show(__FUNCTION__ , $params);
        }



        if ($route->metodo == "POST"){
            $validaciones_editar = [
                'nombre' => ['requerido']
            ];

            $validacion = new Validaciones($validaciones_editar,$params); 
            if ($validacion->estado){
                $grupop = new GrupoPrioritarioModel();
                $grupop->cargar_de_array($params); 
                $id_grupop = $grupop->update();
                 
                $this->session->setflash("El grupo prioritario se actualizó exitosamente","success");
                header("Location: /servicios/v1/grupoprioritario/");
                exit();
            }else{
                $grupop = new GrupoPrioritarioModel();
                $grupop->cargar_de_array($params); 

                $params = array(
                    "grupoprioritario" => $grupop,
                    "validacion" => $validacion,  
                );
                return $this->show(__FUNCTION__ , $params);
            }
        }        

    }    
    public function borrar($route, $params)
    {
        if( $route->metodo == "GET" ){   
            $id_grupop = $params[0]; 
            $grupop = new GrupoPrioritarioModel();
            $grupop->get($id_grupop);  

            $params = array(
                "grupoprioritario" => $grupop
            );

            return $this->show(__FUNCTION__ , $params);  
        }

        if( $route->metodo == "POST" ){
            $grupop= new GrupoPrioritarioModel();
            $grupop->get($params['id']);
            $grupop->delete();

            $this->session->setflash("El grupo prioritario se eliminó exitosamente","success");
            header("Location: /servicios/v1/grupoprioritario/");
            exit();

        }
    }
}
