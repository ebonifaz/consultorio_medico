<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once( LIBS_ROUTE . "Session.php" ); 


class DashboardController extends Controller{
    
    private $session;

    public function __construct()
    { 
        $this->session = new Session();

        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 

    public function show($funcion, $params)
    {
        $params = array("parametros" => $params);
		$this->render(__CLASS__, $funcion , $params);
    }

    public function exec( $route, $params )
    {    
        $params = array("email" => $this->session->get(USER_EMAIL));
        $listado_doctores = "";
        $listado_pacientes = "";

        $this->show(__FUNCTION__, $params);
    }

    public function agenda( $route, $params )
    {
        $params = array("email" => $this->session->get(USER_EMAIL));
        $this->show(__FUNCTION__, $params);
    }
}
