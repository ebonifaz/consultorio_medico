<?php
defined('BASEPATH') or exit('No se permite acceso directo');


require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "DoctoresModel.php" );


class DoctoresController extends Controller
{ 
    private $session;

    public function __construct()
    { 
        $this->session = new Session();
        $this->session->init();
        if( $this->session->getStatus() === 1 OR empty($this->session->get(USER_EMAIL))){
            exit("Acceso Denegado");
        }
    } 
    public function exec($route, $params )
    { 
        $doctor = new DoctoresModel;
        $doctores = $doctor->cargarDoctores();
        $numeroElementos = $doctor->numeroDoctores();
        $page = $route->getPage();

        $params = array(
            "doctores" => $doctores,
            "page" => $page,
            "limite_paginacion" => LIMITE_PAGINACION,
            "numeroElementos" => $numeroElementos,
        );
        $this->show(__FUNCTION__, $params); 
    }

    public function buscar($route, $params )
    {   
        
        if( $route->metodo == "GET" ){ 
            $params = array();
            $page = $route->getPage();
            if( $page > 1 ){

                // Presento la busqueda
                $paciente = new PacienteModel;
                $listado_pacientes = $paciente->buscarPaciente($params); 
                $numeroElementos = $paciente->buscarPacienteCount($params);
                $page = $route->getPage();
                
                $params = array(
                    "listado_pacientes" => $listado_pacientes,
                    "parametros"        => $params,
                    "numeroElementos"   => $numeroElementos,
                    "limite_paginacion" => LIMITE_PAGINACION,
                    "page"              => $page
                );
            } 
            $this->show(__FUNCTION__, $params);
        }
        if( $route->metodo == "POST" ){ 
            $validaciones_pacientes = [ 
                // 'identificacion' => ['requerido']
            ];
            $validacion = new Validaciones($validaciones_pacientes,$params); 
            if( $validacion->estado ){
                // Presento la busqueda
                $paciente = new PacienteModel;
                $listado_pacientes = $paciente->buscarPaciente($params); 
                $numeroElementos = $paciente->buscarPacienteCount($params);
                $page = $route->getPage();
                
                $params = array(
                    "listado_pacientes" => $listado_pacientes,
                    "parametros"        => $params,
                    "numeroElementos"   => $numeroElementos,
                    "limite_paginacion" => LIMITE_PAGINACION,
                    "page"              => $page
                );
            }else{
                // muestro el error
                echo "no voy a buscar ";
                return;
            }
            $this->show(__FUNCTION__, $params);
        }
        
    }
    // GET -> Solitar información
    // POST -> CREAR 
    // PUT -> MODIFICAR
    // DELETE -> BORRAR
 
}
