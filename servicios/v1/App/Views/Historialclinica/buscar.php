<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<?php 
	$listado_pacientes = (isset($this->params['parametros']['listado_pacientes']))?$this->params['parametros']['listado_pacientes']:[];
	$parametros = (isset($this->params['parametros']['parametros']))?$this->params['parametros']['parametros']:[];

	$identificacion =  (array_key_exists('identificacion',$parametros))?$parametros['identificacion']:"";
	$nombre =  (array_key_exists('nombre',$parametros))?$parametros['nombre']:"";
	$apellido =  (array_key_exists('apellido',$parametros))?$parametros['apellido']:"";
	$estado =  (array_key_exists('estado',$parametros))?$parametros['estado']:"";
	$estado_nombre = "";
	switch ($estado) {
		case '1':
			$estado_nombre = "Activo";
			break;
		case '0':
			$estado_nombre = "Desactivado";
			break;
		default:
			$estado_nombre = "";
			break;
	}	
?>


<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

  <div class="content-wrapper">
	<?php echo cargar_header('Buscar Historias Clinicas');?> 

	<div class="content">
	  <div class="container-fluid"> 
		<div class="row"> 
			<div class="col-md-12">
				<form class="col-12 form-inline"  action="" method="POST">  
						<div class="form-group mr-2">
							<label class="mr-1" for="cedula_identificacion_historia_clinica">Cédula de identidad: </label>
							<input type="text" class="form-control" id="cedula_identificacion_historia_clinica" name="identificacion" placeholder="Cédula de identidad" value="<?php echo $identificacion; ?>">
						</div>
						<div class="form-group mr-2">
							<label class="mr-1" for="nombre_historia_clinica">Nombre: </label>
							<input type="text" class="form-control" id="nombre_historia_clinica" name="nombre" placeholder="Nombre" value="<?php echo $nombre; ?>">
						</div>
						<div class="form-group mr-2">
							<label class="mr-1" for="nombre_historia_clinica">Apellido: </label>
							<input type="text" class="form-control" id="nombre_historia_clinica" name="apellido" placeholder="Apellido" value="<?php echo $apellido; ?>">
						</div> 
						<div class="form-group mr-2"> 
							<label class="mr-1" for="estado_form_editar_user">Estado</label>
							<select name="estado" class="custom-select" id="estado_form_editar_user">
								<option value="">Seleccionar</option>
								<option value="1" <?php echo ($estado=='1')?"selected":""; ?>>Activo</option>
								<option value="0" <?php echo ($estado=='0')?"selected":""; ?>>Desactivado</option>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">Buscar</button> 
				</form>	
			</div>
			<div class="col-12">
				<div class="card-body">
					<?php if(empty($listado_pacientes)){ ?>
						<div>
							<h3>No hay Historias Clinicas </h3>
						</div>
					<?php }else{ ?>
						<div class="filtros mb-3">
							<h3>Criterios de Búsqueda</h3>
							<div>Identificación: <span class="badge bg-blue"><?php echo $identificacion; ?></span></div>
							<div>Nombre: <span class="badge bg-blue"><?php echo $nombre; ?></span></div>
							<div>Apellido: <span class="badge bg-blue"><?php echo $apellido; ?></span></div>
							<div>Estado: <span class="badge bg-blue"><?php echo $estado_nombre; ?></span></div>
						</div>
						<table class="table table-bordered">
							<tbody>
								<tr>
									<th style="width: 15px">#</th>
									<th>Identificacion</th>
									<th>Nombre</th> 
									<th>Apellido</th> 
									<th>Estado</th> 
									<th style="width: 40px">Acciones</th>
								</tr> 
								<?php $indice = 1?>
								<?php foreach ($listado_pacientes as $paciente) { ?>
									<tr>
										<td><?php echo $indice; ?>.</td>
										<td><?php echo $paciente['identificacion']; ?></td>  
										<td><?php echo $paciente['nombre']; ?></td>  
										<td><?php echo $paciente['apellido']; ?></td>  
										<td>
											<?php if( $paciente['estado'] ){ ?>
												<i class="fa fa-fw fa-check-circle"></i>
											<?php }else{ ?>
												<i class="fa fa-fw fa-ban"></i>
											<?php } ?>
										</td>  
										<td>
											<div style="display: flex;">
												<a class="btn btn-default mr-1" data-toggle="tooltip" title="Historias Clinicas" href="#"><i class="fa fa-fw fa-book"></i></a>
												<a class="btn btn-default mr-1" data-toggle="tooltip" title="Editar" href="#"><i class="fa fa-edit"></i></a>
												<a class="btn btn-default mr-1" data-toggle="tooltip" title="Ver" href="#"><i class="fa fa-eye"></i></a>
												<a class="btn btn-default mr-1" data-toggle="tooltip" title="Eliminar" href="#"><i class="fa fa-trash"></i></a>
											</div>
										</td>
									</tr> 
									<?php $indice += 1; ?>
								<?php } ?>
							</tbody>
						</table> 
					<?php } ?>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>
