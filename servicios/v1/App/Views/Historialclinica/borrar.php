<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

	<?php 
		$historia_clinica = (array_key_exists('historia_clinica_obj',$this->params['parametros']))?$this->params['parametros']['historia_clinica_obj']:new HistorialclinicaModel;
        
        if( array_key_exists("validacion",$this->params['parametros']) ){
            $validacion = $this->params['parametros']['validacion'];
            $estado_validacion = $validacion->estado;
        }else{
            $estado_validacion = 1;
        }

        if( array_key_exists("success",$this->params['parametros']) ){
            $estado_success = $this->params['parametros']['success'];
            $mensaje_success = $this->params['parametros']['success_mensaje']; 
        }else{
            $estado_success = 0;
        }
	?>
    <div class="content-wrapper">
	<?php echo cargar_header("Historia Clinica");?> 

	<div class="content">
	    <div class="container-fluid"> 

		<div class="row">      
            <div class="card col-12">
                <div class="card-header ">
                    <div class="d-flex justify-content-between">
                        <div><strong>Eliminar medicamento:</strong> <?php echo $historia_clinica->id;?></div>
                        <a href="/servicios/v1/historialclinica/" class="btn btn-primary">Todos las historias</a>
                    </div>
                </div>
                <div class="card-body"> 
                    <div class="card-text">
                        <div class="row mb-1"> 
                            <div class="col-12">Seguro que desea eliminar el <?php echo $historia_clinica->id; ?></div> 
                        </div> 
                    </div>
                </div>
                <div class="card-footer">
                    <form class="col-12" method="POST" action="">
                        <input type="hidden" value="<?php echo $historia_clinica->id ?>" name="id">
                        <button type="submit" class="btn btn-primary">Si, deseo eliminar</button>
                    </form>
                </div>
            </div>
	    </div>
        </div>
    </div>
    </div>




    <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>