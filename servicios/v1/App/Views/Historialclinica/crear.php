<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>
 
	<?php 
		$listado_tipo_sangre_template = (array_key_exists('listado_tipo_sangre',$this->params['parametros']))?$this->params['parametros']['listado_tipo_sangre']:[];
		$listado_pacientes = (array_key_exists('listado_pacientes',$this->params['parametros']))?$this->params['parametros']['listado_pacientes']:[]; 
		$listado_doctores = (array_key_exists('listado_doctores',$this->params['parametros']))?$this->params['parametros']['listado_doctores']:[]; 
		$listado_grupo_prioritario = (array_key_exists('listado_grupo_prioritario',$this->params['parametros']))?$this->params['parametros']['listado_grupo_prioritario']:[]; 


    if( array_key_exists("validacion",$this->params['parametros']) ){
      $validacion = $this->params['parametros']['validacion'];
      $estado_validacion = $validacion->estado;
    }else{
      $estado_validacion = 1;
    }

    if( array_key_exists("success",$this->params['parametros']) ){
      $estado_success = $this->params['parametros']['success'];
      $mensaje_success = $this->params['parametros']['success_mensaje']; 
    }else{
      $estado_success = 0;
    } 
	?>
  <div class="content-wrapper">
	<?php echo cargar_header("Historia Clinica");?> 

	<div class="content">
	  <div class="container-fluid"> 
        <div class="row">      
            <div class="card col-12">
              <div class="card-header ">
                  <div class="d-flex justify-content-between">
                      <div><strong>Crear Historia Clínica:</strong></div>
                      <a href="/servicios/v1/historialclinica/" class="btn btn-primary">Todos las historias</a>
                  </div>
              </div>
              <form class="col-12" method="POST" action=""> 
                <div class="card-body">  
                  <?php 
                    if( ! $estado_validacion ){
                    mostrar_errores($validacion);
                    }
                    if( $estado_success ){
                    mostrar_success($mensaje_success);
                    }
                  ?> 
                  <div class="row">
                    <div class="form-group col-6">
                      <label>Paciente</label> 
                      <select class="form-control select2" name="id_paciente">
                        <option value="">Seleccione un paciente</option>
                        <?php foreach ($listado_pacientes as $paciente) { ?>
                          <option value="<?php echo($paciente['id']) ?>" <?php //echo ($cita_template->hasPacienteById($paciente['id']))?"selected":"";?>><?php echo($paciente['nombre']) ?></option>
                        <?php }?>
                      </select>
                    </div> 

                    <div class="form-group col-6">
                      <label>Doctor</label> 
                      <select  class="form-control select2" name="id_doctor"  style="width: 100%;" >
                        <option value="">Seleccione un doctor</option>
                        <?php foreach ($listado_doctores as $doctor) { ?>
                          <option value="<?php echo($doctor['id']) ?>" <?php //echo ($cita_template->hasDoctorById($doctor['id']))?"selected":"";?>><?php echo($doctor['nombre']) ?></option>
                        <?php }?>
                      </select>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label for="box-calendar">Fecha</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="box-calendar-fecha-lb"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input name="hora_solicitada_fecha" type="text" class="form-control" id="box-calendar-fecha" aria-describedby="box-calendar-fecha" value="">
                      </div>
                    </div>
                    
                    <div class="form-group col-6">
                      <label for="box-calendar">Hora</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="box-calendar-hora"><i class="fa fa-clock"></i></span>
                        </div>
                        <input name="hora_solicitada_hora" type="text" class="form-control box-calendar-time" id="box-calendar-time" aria-describedby="box-calendar-hora" value="">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label>Tipo de Sangre</label> 
                      <select  class="form-control select2" name="tipo_sangre"  style="width: 100%;" >
                        <option value="">Seleccione un tipo de sangre</option>
                        <?php foreach ($listado_tipo_sangre_template as $tipo_de_sangre) { ?>
                          <option value="<?php echo($tipo_de_sangre['id']) ?>" <?php //echo ($cita_template->hasDoctorById($doctor['id']))?"selected":"";?>><?php echo($tipo_de_sangre['nombre']) ?></option>
                        <?php }?>
                      </select>
                    </div>
                    
                    <div class="form-group col-6">
                      <label>Sexo</label> 
                      <select  class="form-control select2" name="sexo"  style="width: 100%;" >
                        <option value="">Seleccione el sexo </option>
                        <option value="0" >Masculino</option>
                        <option value="1" >Femenino</option>
                      </select>
                    </div>                  
                  </div>
                  
                  <div class="row">
                  
                  <div class="form-group col-6">
                      <label>Discapacidad</label> 
                      <select  class="form-control select2" name="discapacidad"  style="width: 100%;" >
                        <option value="">¿Tiene?</option>
                        <option value="1" >Si</option>
                        <option value="0" >No</option>
                      </select>
                    </div>    
                    <div class="form-group col-6">
                      <label for="box-calendar">Fecha de nacimiento</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="box-calendar-fecha-lb"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input name="fecha_nacimiento" type="text" class="form-control" id="box-calendar-nacimiento" aria-describedby="box-calendar-nacimiento" value="">
                      </div>
                    </div>

                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label>Grupo Prioritario</label> 
                      <select  class="form-control select2" name="grupo_prioritario"  style="width: 100%;" >
                        <option value="">Seleccione un tipo de sangre</option>
                        <?php foreach ($listado_grupo_prioritario as $grupo) { ?>
                          <option value="<?php echo($grupo['id']) ?>" <?php //echo ($cita_template->hasDoctorById($doctor['id']))?"selected":"";?>><?php echo($grupo['nombre']) ?></option>
                        <?php }?>
                      </select>
                    </div>
                  
                  </div>

                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
              </form>
            </div>
        </div>
	  </div>
	</div>
  </div>
  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 

<script>
    $('#box-calendar-fecha').datepicker({
      autoclose: true,
      daysOfWeekDisabled: "0,1,2,3,4,5",
      format: 'yyyy-mm-dd',
      startDate: '+0d'
    })
    $('#box-calendar-nacimiento').datepicker({
      autoclose: true, 
      format: 'yyyy-mm-dd',
      endDate: '+0d'
    })
    //Timepicker
    $('#box-calendar-time').timepicker({
      showInputs: false,
      showSeconds: true,
      showMeridian: false
    })
    $('.select2').select2({
      theme: 'bootstrap4'
    })
</script>

</body>
</html>