<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>
 
	<?php 
		$historia_clinica_obj = (array_key_exists('historia_clinica_obj',$this->params['parametros']))?$this->params['parametros']['historia_clinica_obj']:[]; 
		$listado_antecedente = (array_key_exists('listado_antecedente',$this->params['parametros']))?$this->params['parametros']['listado_antecedente']:[]; 
		$listado_diagnostico = (array_key_exists('listado_diagnostico',$this->params['parametros']))?$this->params['parametros']['listado_diagnostico']:[];        
		$listado_medicamentos = (array_key_exists('listado_medicamentos',$this->params['parametros']))?$this->params['parametros']['listado_medicamentos']:[];    
		$listado_viaadministracion = (array_key_exists('listado_viaadministracion',$this->params['parametros']))?$this->params['parametros']['listado_viaadministracion']:[];    
		$listado_frecuenciamodel = (array_key_exists('listado_frecuenciamodel',$this->params['parametros']))?$this->params['parametros']['listado_frecuenciamodel']:[];    
		$listado_unidadadministracionmodel = (array_key_exists('listado_unidadadministracionmodel',$this->params['parametros']))?$this->params['parametros']['listado_unidadadministracionmodel']:[];        




        $paciente = $historia_clinica_obj->getPaciente();
        $doctor = $historia_clinica_obj->getDoctor();

    if( array_key_exists("validacion",$this->params['parametros']) ){
      $validacion = $this->params['parametros']['validacion'];
      $estado_validacion = $validacion->estado;
    }else{
      $estado_validacion = 1;
    }

    if( array_key_exists("success",$this->params['parametros']) ){
      $estado_success = $this->params['parametros']['success'];
      $mensaje_success = $this->params['parametros']['success_mensaje']; 
    }else{
      $estado_success = 0;
    } 
	?>
  <div class="content-wrapper">
	<?php echo cargar_header("Historia Clinica");?> 

	<div class="content">
	  <div class="container-fluid"> 
        <div class="row">      
            <div class="card col-12">
                <div class="card-header ">
                    <div class="d-flex justify-content-between">
                        <div><strong> Historia Clínica</strong></div>
                        <!-- <a href="/servicios/v1/historialclinica/" class="btn btn-primary">Todos las historias</a> -->
                    </div>
                </div>
              
                <div class="card-body">  
                    <?php 
                    if( ! $estado_validacion ){
                    mostrar_errores($validacion);
                    }
                    if( $estado_success ){
                    mostrar_success($mensaje_success);
                    }
                    ?>  
                    <div>
                        <h4>
                            <i class="fas fa-globe"></i> Historia Clinica #<?php echo $historia_clinica_obj->id; ?>
                            <small class="float-right">Date: <?php echo $historia_clinica_obj->getFecha() ?></small>
                        </h4>
                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <div><strong>Paciente:</strong> <?php echo $paciente->nombre; ?> <?php echo $paciente->apellido; ?></div>
                                <div><strong>Edad:</strong> <?php echo( $paciente->get_edad() ); ?></div>
                                <div><strong>Fecha Nacimiento:</strong> <?php echo $paciente->fecha_nacimiento; ?></div> 
                            </div>
                            <div class="col-sm-6"> 
                                <div><strong>Sexo: </strong><?php echo ($paciente->sexo)?"Masculino":"Femenino"; ?></div>
                                <div><strong>Discapacidad: </strong><?php echo ($paciente->discapacidad)?"Si":"No"; ?></div>
                                <div><strong>Grupo Sanguineo: </strong><?php echo $paciente->getTipoSangre(); ?></div>
                            </div>                    
                        </div>
                    </div>
                    
                    <div>
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Motivo de la consulta</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Antecedentes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Signos Vitales</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-settings-tab" data-toggle="pill" href="#custom-tabs-one-settings" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">Diagnostico</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-tratamiento-tab" data-toggle="pill" href="#custom-tabs-one-tratamiento" role="tab" aria-controls="custom-tabs-one-tratamiento" aria-selected="false">Tratamiento</a>
                                </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                    <form class="col-12" method="POST" action="">
                                        <input type="hidden" name="id" value="<?php echo $historia_clinica_obj->id; ?>">
                                        <div class="row">
                                            <div class="form-group col-6">
                                                <label>Motivo de la consulta</label> 
                                                <textarea name="motivo_consulta" id="motivo_consulta" cols="20" rows="5" class="form-control"><?php echo ($historia_clinica_obj->motivo_consulta)?$historia_clinica_obj->motivo_consulta:""; ?></textarea>
                                            </div>  
                                            <div class="form-group col-6">
                                                <label>Enfermedad o motivo actual</label> 
                                                <textarea name="enfermedad_motivo_actual" id="enfermedad_motivo_actual" cols="20" rows="5" class="form-control"><?php echo ($historia_clinica_obj->enfermedad_motivo_actual)?$historia_clinica_obj->enfermedad_motivo_actual:""; ?></textarea>
                                            </div>                                          
                                        </div>       
                                            <div class="form-group col-12">
                                                <button type="submit" class="btn btn-primary" name="submit_motivo_consulta">Enviar</button>
                                            </div>                                   
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                    <div class="row">
                                        <?php $indice = 1; ?>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 15px">#</th>
                                                    <th>Antecedente</th> 
                                                    <th>Observación</th> 
                                                    <th style="width: 40px">Acciones</th>
                                                </tr>  
                                                <?php foreach ((new PacienteModel)->getAntecentes($paciente->id)  as $antecedente) { ?>
                                                    <tr>
                                                        <td><?php echo $indice; ?>.</td>
                                                        <td><?php echo $antecedente['nombre']; ?></td>
                                                        <td><?php echo $antecedente['observacion']; ?></td> 
                                                        <td>
                                                            <div style="display: flex;"> 
                                                                <form class="col-12" method="POST" action="">
                                                                    <input type="hidden" name="id" value="<?php echo $antecedente['id'] ?>">
                                                                    <input type="hidden" name="id_historia" value="<?php echo $historia_clinica_obj->id; ?>">
                                                                    <button type="submit" class="btn btn-default mr-1" name="submit_antecedente_eliminar" data-toggle="tooltip" title="Borrar"><i class="fa fa-trash"></i></button>
                                                                </form> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $indice += 1; ?>
                                                <?php } ?>
                                            </tbody>
                                        </table> 
                                    </div>

                                    <form class="col-12" method="POST" action="">
                                        <input type="hidden" name="id" value="<?php echo $historia_clinica_obj->id; ?>">
                                        <div class="row">
                                            <div class="form-group col-6">
                                                <label>Antecedentes</label>  
                                                <select  class="form-control select2" name="id_antecedente"  style="width: 100%;" >
                                                    <option value="">Seleccione un antecedente</option>
                                                    <?php foreach ($listado_antecedente as $antecedente) { ?>
                                                    <option value="<?php echo($antecedente['id']) ?>"><?php echo($antecedente['nombre']) ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="form-group col-6">
                                                <label>Observación</label> 
                                                <textarea name="observacion" id="observacion" cols="20" rows="5" class="form-control"><?php //echo ($historia_clinica_obj->motivo_consulta)?$historia_clinica_obj->motivo_consulta:""; ?></textarea>
                                            </div>  
                                                                                     
                                        </div>       
                                            <div class="form-group col-12">
                                                <button type="submit" class="btn btn-primary" name="submit_antecedente">Agregar</button>
                                            </div>                                   
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                                    <div class="card">
                                        <form class="col-12" method="POST" action="">
                                            <input type="hidden" name="id" value="<?php echo $historia_clinica_obj->id; ?>">
                                            <div class="card-body"> 
                                                <h5 class="card-title">Signos vitales</h5>
                                                <div class="card-text"> 
                                                    <div class="row">
                                                        <div class="form-group col-4">
                                                            <label for="signos_vitales_sistolica">Presión arterial sistólica</label>
                                                            <input type="text" class="form-control" id="signos_vitales_sistolica" name="signos_vitales_sistolica" placeholder="Presión arterial sistólica" value="<?php echo ($historia_clinica_obj->signos_vitales_sistolica)?$historia_clinica_obj->signos_vitales_sistolica:""; ?>">
                                                        </div> 
                                                        <div class="form-group col-4">
                                                            <label for="signos_vitales_diastolica">Presión arterial diastólica</label>
                                                            <input type="text" class="form-control" id="signos_vitales_diastolica" name="signos_vitales_diastolica" placeholder="Presión arterial diastólica" value="<?php echo ($historia_clinica_obj->signos_vitales_diastolica)?$historia_clinica_obj->signos_vitales_diastolica:""; ?>">
                                                        </div> 
                                                        <div class="form-group col-4">
                                                            <label for="temperatura">Temperatura</label>
                                                            <input type="text" class="form-control" id="temperatura" name="temperatura" placeholder="temperatura" value="<?php echo ($historia_clinica_obj->temperatura)?$historia_clinica_obj->temperatura:""; ?>">
                                                        </div>  
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-4">
                                                            <label for="frecuencia_respiratoria">Frecuencia respiratoria</label>
                                                            <input type="text" class="form-control" id="frecuencia_respiratoria" name="frecuencia_respiratoria" placeholder="Frecuencia respiratoria" value="<?php echo ($historia_clinica_obj->frecuencia_respiratoria)?$historia_clinica_obj->frecuencia_respiratoria:""; ?>">
                                                        </div> 
                                                        <div class="form-group col-4">
                                                            <label for="frecuencia_cardiaca">Frecuencia cardíaca</label>
                                                            <input type="text" class="form-control" id="frecuencia_cardiaca" name="frecuencia_cardiaca" placeholder="Frecuencia cardíaca" value="<?php echo ($historia_clinica_obj->frecuencia_cardiaca)?$historia_clinica_obj->frecuencia_cardiaca:""; ?>">
                                                        </div> 
                                                        <div class="form-group col-4">
                                                            <label for="saturacion">Saturación de oxigeno</label>
                                                            <input type="text" class="form-control" id="saturacion" name="saturacion" placeholder="%" value="<?php echo ($historia_clinica_obj->saturacion)?$historia_clinica_obj->saturacion:""; ?>">
                                                        </div>  
                                                    </div>   
                                                </div>
                                            </div>
                                            <div class="card-body"> 
                                                <h5 class="card-title">Datos Antropométricos</h5>
                                                <div class="card-text">
                                                    <div class="row">
                                                        <div class="form-group col-3">
                                                            <label for="antro_peso">Peso</label>
                                                            <input type="text" class="form-control" id="peso" name="peso" placeholder="Kg" value="<?php echo ($historia_clinica_obj->peso)?$historia_clinica_obj->peso:""; ?>">
                                                        </div> 
                                                        <div class="form-group col-3">
                                                            <label for="estatura">Talla/estatura</label>
                                                            <input type="text" class="form-control" id="estatura" name="estatura" placeholder="Cm" value="<?php echo ($historia_clinica_obj->estatura)?$historia_clinica_obj->estatura:""; ?>">
                                                        </div> 
                                                        <div class="form-group col-3">
                                                            <label for="perimetro_abdominal">Perímetro abdominal</label>
                                                            <input type="text" class="form-control" id="perimetro_abdominal" name="perimetro_abdominal" placeholder="Cm" value="<?php echo ($historia_clinica_obj->perimetro_abdominal)?$historia_clinica_obj->perimetro_abdominal:""; ?>">
                                                        </div>  
                                                        <div class="form-group col-3">
                                                            
                                                            <label for="antro_peso">IMC</label> 
                                                            <input type="text" class="form-control" id="peso" name="peso" placeholder="Kg" value="<?php echo $historia_clinica_obj->getImc(); ?>" disabled>
                                                        </div> 

                                                    </div> 

                                                </div>
                                            </div>
                                            <div class="card-body"> 
                                                <h5 class="card-title">Mediciones Capilares</h5>
                                                <div class="card-text">
                                                    <div class="row">
                                                        <div class="form-group col-4">
                                                            <label for="glucosa_capilar">Glucosa capilar</label>
                                                            <input type="text" class="form-control" id="glucosa_capilar" name="glucosa_capilar" placeholder="mg/dl" value="<?php echo ($historia_clinica_obj->glucosa_capilar)?$historia_clinica_obj->glucosa_capilar:""; ?>">
                                                        </div> 
                                                        <div class="form-group col-4">
                                                            <label for="valor_hemoglobina">Valor de hemoglobina</label>
                                                            <input type="text" class="form-control" id="valor_hemoglobina" name="valor_hemoglobina" placeholder="g/dl" value="<?php echo ($historia_clinica_obj->valor_hemoglobina)?$historia_clinica_obj->valor_hemoglobina:""; ?>">
                                                        </div>  
                                                    </div> 

                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <div class="form-group col-12">
                                                    <button type="submit" class="btn btn-primary" name="submit_signos_vitales">Cambiar</button>
                                                </div>  
                                            </div>
                                        </form>  
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">
                                    
                                <div class="card">
                                    <form class="col-12" method="POST" action="">
                                        <input type="hidden" name="id" value="<?php echo $historia_clinica_obj->id; ?>">
                                        <div class="card-body">  
                                            <div class="form-group ">
                                                <label>Diagnostico</label>   
                                                <select  class="form-control select2" name="id_diagnosticos"  style="width: 100%;" >
                                                    <option value="">Seleccione un diagnostico</option>
                                                    <?php foreach ($listado_diagnostico as $diagnostico) { ?>
                                                    <option value="<?php echo($diagnostico['id']) ?>" <?php echo ($diagnostico['id']==$historia_clinica_obj->id_diagnosticos)?"selected":"";?> ><?php echo($diagnostico['nombre']) ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <label>Observación</label> 
                                                <textarea name="observacion_diagnostico" id="observacion_diagnostico" cols="20" rows="5" class="form-control"><?php echo ($historia_clinica_obj->observacion_diagnostico)?$historia_clinica_obj->observacion_diagnostico:""; ?></textarea>
                                            </div>  
                                        </div>                                            
                                        <div class="card-footer">
                                            <div class="form-group col-12">
                                                <button type="submit" class="btn btn-primary" name="submit_diagnostico">Guardar</button>
                                            </div>  
                                        </div>
                                    </form>
                                </div>
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-tratamiento" role="tabpanel" aria-labelledby="custom-tabs-one-tratamiento-tab">
                                    
                                    <div class="row">

                                    <?php $indice = 1; ?>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 15px">#</th>
                                                    <th>Medicamento</th> 
                                                    <th>Cantidad</th> 
                                                    <th>Inicio tratamiento</th> 
                                                    <th>Administración</th> 
                                                    <th style="width: 40px">Acciones</th>
                                                </tr>  
                                                <?php foreach ((new TratamientoModel)->getListadoPorHistoriaClinica( $historia_clinica_obj->id ) as $tratamiento) { ?>
                                                    <tr>
                                                        <td><?php echo $indice; ?>.</td>
                                                        <td><?php echo $tratamiento['nombre']; ?></td>
                                                        <td><?php echo $tratamiento['cantidad']; ?></td> 
                                                        <td><?php echo $tratamiento['inicio_tratamiento']; ?></td> 
                                                        <td><?php echo $tratamiento['administracion']; ?></td> 
                                                        <td>
                                                            <div style="display: flex;"> 
                                                                <form class="col-12" method="POST" action="">
                                                                    <input type="hidden" name="id" value="<?php echo $tratamiento['id'] ?>">
                                                                    <input type="hidden" name="id_historia" value="<?php echo $historia_clinica_obj->id; ?>">
                                                                    <button type="submit" class="btn btn-default mr-1" name="submit_tratamiento_eliminar" data-toggle="tooltip" title="Borrar"><i class="fa fa-trash"></i></button>
                                                                </form> 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php $indice += 1; ?>
                                                <?php } ?>
                                            </tbody>
                                        </table>                                     
                                    </div>
                                    
                                    <form class="col-12" method="POST" action="">
                                        <input type="hidden" name="id" value="<?php echo $historia_clinica_obj->id; ?>">
                                        <div class="card-body">  
                                            <div class="row">
                                                <div class="form-group col-6">
                                                    <label>Medicamentos</label>  
                                                    <select  class="form-control select2" name="id_medicamentos"  style="width: 100%;" required>
                                                        <option value="">Seleccione un medicamento</option>
                                                        <?php foreach ($listado_medicamentos as $medicamento) { ?>
                                                        <option value="<?php echo($medicamento['id']) ?>"><?php echo($medicamento['nombre']) ?></option>
                                                        <?php }?>
                                                    </select>
                                                </div> 
                                                <div class="form-group col-6">
                                                    <label for="cantidad">Cantidad</label>
                                                    <input type="text" class="form-control" id="cantidad" name="cantidad" placeholder="Cantidad" value="" required>
                                                </div>                                              
                                            </div>
                                            
                                            <div class="row">
                                                <div class="form-group col-4">
                                                    <label>Vía administración</label>  
                                                    <select  class="form-control select2" name="id_via_administracion"  style="width: 100%;" required>
                                                        <option value="">Seleccione un vía</option>
                                                        <?php foreach ($listado_viaadministracion as $via_admin) { ?>
                                                        <option value="<?php echo($via_admin['id']) ?>"><?php echo($via_admin['nombre']) ?></option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                                        
                                                <div class="form-group col-4">
                                                    <label for="dosis">Dosis Unitaria</label>
                                                    <input type="text" class="form-control" id="dosis" name="dosis" placeholder="Dosis Unitaria" value="" required>
                                                </div> 
                                                <div class="form-group col-4">
                                                    <label>U. Administración</label>  
                                                    <select  class="form-control select2" name="id_unidad_administracion"  style="width: 100%;" required>
                                                        <option value="">Seleccione una unidad</option>
                                                        <?php foreach ($listado_unidadadministracionmodel as $uadministracion) { ?>
                                                        <option value="<?php echo($uadministracion['id']) ?>"><?php echo($uadministracion['nombre']) ?></option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="form-group col-4">
                                                    <label>Frecuencia</label>  
                                                    <select  class="form-control select2" name="id_frecuencia"  style="width: 100%;" required>
                                                        <option value="">Seleccione un Frecuencia</option>
                                                        <?php foreach ($listado_frecuenciamodel as $frecuencia) { ?>
                                                        <option value="<?php echo($frecuencia['id']) ?>"><?php echo($frecuencia['nombre']) ?></option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                                        
                                                <div class="form-group col-4">
                                                    <label for="inicio_tratamiento_fecha">Inicio tratamiento fecha</label>
                                                    <input type="text" class="form-control" id="dosis_unitaria_fecha" name="dosis_unitaria_fecha" placeholder="fecha" value="" required>
                                                </div>  
                                                
                                                <div class="form-group col-4">
                                                    <label for="inicio_tratamiento_hora">Inicio tratamiento hora</label>
                                                    <input type="text" class="form-control" id="dosis_unitaria_hora" name="dosis_unitaria_hora" placeholder="hora" value="" required>
                                                </div>  
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-6">
                                                    <label>Observación</label> 
                                                    <textarea name="observacion" id="observacion" cols="20" rows="5" class="form-control"></textarea>
                                                </div>  
                                                
                                                <div class="form-group col-6">
                                                    <label>Advertencia</label> 
                                                    <textarea name="advertencia" id="v" cols="20" rows="5" class="form-control"></textarea>
                                                </div>  
                                            </div>
                                             
                                        </div>                                            
                                        <div class="card-footer">
                                            <div class="form-group col-12">
                                                <button type="submit" class="btn btn-primary" name="submit_tratamientos">Guardar</button>
                                            </div>  
                                        </div>
                                    </form>
                                </div>
                                </div>
                            </div> 
                        </div>                
                    </div>  
                </div>
        </div>
	  </div>
	</div>
  </div>
  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 

<script>
    $('#dosis_unitaria_fecha').datepicker({
      autoclose: true, 
      format: 'yyyy-mm-dd',
      startDate: '+0d'
    }) 
    //Timepicker
    $('#dosis_unitaria_hora').timepicker({
      showInputs: false,
      showSeconds: false,
      showMeridian: false
    })
    $('.select2').select2({
      theme: 'bootstrap4'
    })
</script>

</body>
</html>