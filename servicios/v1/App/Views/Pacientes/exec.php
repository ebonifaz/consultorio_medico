<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<?php  
$pacientes = (array_key_exists("pacientes", $this->params['parametros']))?$this->params['parametros']['pacientes']:[]; 
$page = $this->params['parametros']['page']; 
$limite_paginacion = $this->params['parametros']['limite_paginacion']; 
$indice = (($page-1) * $limite_paginacion) + 1;
$numeroElementos = $this->params['parametros']['numeroElementos']; 

?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

  <div class="content-wrapper">
	<?php echo cargar_header('Pacientes');?> 

	<div class="content">
	  <div class="container-fluid">
		<div class="row"> 
			<div class="card col-12">
				<div class="card-header ">
					<div class="d-flex justify-content-between">
						<div>Listado</div> 
						<div class="nuevo">
							<a class='btn btn-default mr-1' data-toggle='tooltip' title='' href='/servicios/v1/usuarios/crear' data-original-title='Nuevo'><i class='fa fa-plus'></i></a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<?php mensaje_flash_view() ?>
		
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th style="width: 15px">#</th>
								<th>Paciente</th> 
								<th>Identificación</th>
								<th>Correo</th>
								<th>Signos última cita</th>
								<th>Estado</th>
								<th style="width: 40px">Acciones</th>
							</tr>  
							<?php foreach ($pacientes as $paciente) { ?>
								<tr>
									<td><?php echo $indice; ?>.</td>
									<td><?php echo ($paciente['nombre'])?$paciente['nombre']:""; ?> <?php echo ($paciente['apellido'])?$paciente['apellido']:""; ?></td>
									<td><?php echo ($paciente['identificacion'])?$paciente['identificacion']:""; ?></td>
									<td><?php echo ($paciente['email'])?$paciente['email']:""; ?></td>

									<td>
										<strong>Discapacidad:</strong> <?php echo (($paciente['discapacidad'])?$paciente['discapacidad']:"")?'Si':'No'; ?><br>
										<strong>Peso:</strong> <?php echo ($paciente['peso'])?$paciente['peso']:""; ?> Kg<br>
										<strong>Altura:</strong> <?php echo ($paciente['estatura'])?$paciente['estatura']:""; ?> cm<br>
										<strong>Presión:</strong> <?php echo ($paciente['presion'])?$paciente['presion']:""; ?><br>
										<strong>Saturación:</strong> <?php echo ($paciente['saturacion'])?$paciente['saturacion']:""; ?>
									</td>
									<td>
										<?php if( $paciente['estado'] ){ ?>
											<i class="fa fa-fw fa-check-circle"></i>
										<?php }else{ ?>
											<i class="fa fa-fw fa-ban"></i>
										<?php } ?></td> 
									<td>
										<div style="display: flex;">
											<!-- <a class="btn btn-default mr-1" data-toggle="tooltip" title="Listado Historias Clinicas" href="/servicios/v1/historialclinica/usuario/<?php echo $paciente['id']; ?>"><i class="fa fa-book"></i></a> -->
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Editar" href="/servicios/v1/usuarios/editar/<?php echo $paciente['id']; ?>"><i class="fa fa-edit"></i></a>
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Ver" href="/servicios/v1/usuarios/ver/<?php echo $paciente['id']; ?>"><i class="fa fa-eye"></i></a>
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Borrar"href="/servicios/v1/usuarios/borrar/<?php echo $paciente['id']; ?>"><i class="fa fa-trash"></i></a>
										</div>
									</td>
								</tr>
								<?php $indice += 1; ?>
							<?php } ?>
						</tbody>
					</table> 
				</div>
				<div class="card-footer">
					<?php paginacion($page,$numeroElementos);?> 
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>
