  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> 
  <!-- Font Awesome --> 
  <link rel="stylesheet" href="<?php echo FOLDER_PATH . SEPARADOR_URL .PATH_ASSETS . "plugins/fontawesome-free/css/all.min.css";?>">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo FOLDER_PATH . SEPARADOR_URL .PATH_ASSETS . "plugins/icheck-bootstrap/icheck-bootstrap.min.css";?>">
  <!-- icheck toastr -->
  <link rel="stylesheet" href="<?php echo FOLDER_PATH . SEPARADOR_URL .PATH_ASSETS . "plugins/toastr/toastr.min.css";?>">
  <!-- datepicker style -->
  <link rel="stylesheet" href="<?php echo FOLDER_PATH . SEPARADOR_URL .PATH_ASSETS . "css/dist/bootstrap-datepicker/bootstrap-datepicker.min.css";?>">
  <!-- timepicker style -->
  <link rel="stylesheet" href="<?php echo FOLDER_PATH . SEPARADOR_URL .PATH_ASSETS . "css/dist/timepicker/bootstrap-timepicker.min.css";?>">
  <!-- select2.min style -->
  <link rel="stylesheet" href="<?php echo FOLDER_PATH . SEPARADOR_URL .PATH_ASSETS . "css/dist/select2/select2.min.css";?>">
  <!-- select2.bootstrap4 style -->
  <link rel="stylesheet" href="<?php echo FOLDER_PATH . SEPARADOR_URL .PATH_ASSETS . "css/dist/select2/select2-bootstrap4.min.css";?>">
  <!-- fullcalendar style -->
  <link rel="stylesheet" href="<?php echo FOLDER_PATH . SEPARADOR_URL .PATH_ASSETS . "css/dist/fullcalendar/fullcalendar.min.css";?>"> 
  
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo FOLDER_PATH . SEPARADOR_URL .PATH_ASSETS . "css/dist/css/adminlte.min.css";?>">

