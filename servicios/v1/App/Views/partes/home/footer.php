<!-- jQuery -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "plugins/jquery/jquery.min.js";?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "plugins/bootstrap/js/bootstrap.bundle.min.js";?>"></script>
<!-- toastr -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "plugins/toastr/toastr.min.js";?>"></script>
<!-- bootstrap-datepicker -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "js/dist/bootstrap-datepicker/bootstrap-datepicker.min.js";?>"></script>
<!-- bootstrap-timepicker -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "js/dist/timepicker/bootstrap-timepicker.min.js";?>"></script>
<!-- bootstrap-timepicker -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "js/dist/select2/select2.full.min.js";?>"></script>
<!-- fullcalendar-moment -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "js/dist/fullcalendar/moment.min.js";?>"></script>
<!-- fullcalendar-fullcalendar -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "js/dist/fullcalendar/fullcalendar.min.js";?>"></script>
<!-- fullcalendar-locale-es -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "js/dist/fullcalendar/locale/es.js";?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo FOLDER_PATH . SEPARADOR_URL . PATH_ASSETS . "js/dist/adminlte.min.js";?>"></script>