  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021-2021</strong> Consultorio Clinico Salitre
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.1
    </div>
  </footer>