
<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
    <?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

  <div class="content-wrapper">
    <?php echo cargar_header('Usuarios');?> 

    <div class="content">
      <div class="container-fluid">
        <div class="row"> 
            <!-- Main content -->
        </div>
      </div>
    </div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>
