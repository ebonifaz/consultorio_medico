<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

	<?php 
		$cita_template = (array_key_exists('cita',$this->params['parametros']))?$this->params['parametros']['cita']:new CitasModel;
		$listado_pacientes = (array_key_exists('listado_pacientes',$this->params['parametros']))?$this->params['parametros']['listado_pacientes']:[]; 
		$listado_doctores = (array_key_exists('listado_doctores',$this->params['parametros']))?$this->params['parametros']['listado_doctores']:[]; 
		$listado_estadocita = (array_key_exists('listado_estadocita',$this->params['parametros']))?$this->params['parametros']['listado_estadocita']:[]; 


    if( array_key_exists("validacion",$this->params['parametros']) ){
      $validacion = $this->params['parametros']['validacion'];
      $estado_validacion = $validacion->estado;
    }else{
      $estado_validacion = 1;
    }

    if( array_key_exists("success",$this->params['parametros']) ){
      $estado_success = $this->params['parametros']['success'];
      $mensaje_success = $this->params['parametros']['success_mensaje']; 
    }else{
      $estado_success = 0;
    }
    
    
	?>
    <div class="content-wrapper">
	  <?php echo cargar_header("Cita");?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">   
            <div class="card col-12">
              <div class="card-header ">
                  <div class="d-flex justify-content-between">
                      <div><strong>Editar Cita:</strong> <?php echo $cita_template->id;?></div>
                      <a href="/servicios/v1/cita/" class="btn btn-primary">Todos las citas</a>
                  </div>
              </div>
              <form class="col-12" method="POST" action=""> 
                <div class="card-body">
                  <?php 
                    if( ! $estado_validacion ){
                    mostrar_errores($validacion);
                    }
                    if( $estado_success ){
                    mostrar_success($mensaje_success);
                    }
                  ?>
                  <input type="hidden" value="<?php echo $cita_template->id ?>" name="id">  
                  <div class="form-group">
                    <label>Paciente</label> 
                    <select class="form-control select2" name="id_paciente">
                      <?php foreach ($listado_pacientes as $paciente) { ?>
                        <option value="<?php echo($paciente['id']) ?>" <?php echo ($cita_template->hasPacienteById($paciente['id']))?"selected":"";?>><?php echo($paciente['nombre']) ?></option>
                      <?php }?>
                    </select>
                  </div>   
                  <div class="form-group">
                    <label>Doctor</label> 
                    <select  class="form-control select2" name="id_doctor"  style="width: 100%;" >
                      <?php foreach ($listado_doctores as $doctor) { ?>
                        <option value="<?php echo($doctor['id']) ?>" <?php echo ($cita_template->hasDoctorById($doctor['id']))?"selected":"";?>><?php echo($doctor['nombre']) ?></option>
                      <?php }?>
                    </select>
                  </div>
                  
                  <div class="form-group">
                    <label for="box-calendar">Fecha</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="box-calendar-fecha-lb"><i class="fa fa-calendar"></i></span>
                      </div>
                      <input name="hora_solicitada_fecha" type="text" class="form-control" id="box-calendar-fecha" aria-describedby="box-calendar-fecha" value="<?php echo $cita_template->getFecha(); ?>">
                    </div>
                    
                  </div>
                  
                  <div class="form-group">
                    <label for="box-calendar">Hora</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="box-calendar-hora"><i class="fa fa-clock"></i></span>
                      </div>
                      <input name="hora_solicitada_hora" type="text" class="form-control box-calendar-time" id="box-calendar-time" aria-describedby="box-calendar-hora" value="<?php echo $cita_template->getHora(); ?>">
                    </div>
                  </div>

                  
                  <div class="form-group">
                    <label>Estados</label> 
                    <select   class="form-control select2" name="id_estado_cita"  style="width: 100%;" >
                      <?php foreach ($listado_estadocita as $estadocita) { ?>
                        <option value="<?php echo($estadocita['id']) ?>" <?php echo ($cita_template->hasCitaById($estadocita['id']))?"selected":"";?>><?php echo($estadocita['nombre']) ?></option>
                      <?php }?>
                    </select>
                  </div> 

                  <div class="form-group">
                    <label>Observaciones</label> 
                    <textarea name="observacion" id="box-observacion" cols="30" rows="5" class="form-control"><?php echo $cita_template->observacion; ?></textarea>
                  </div>
 

                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 

<script>
    $('#box-calendar-fecha').datepicker({
      autoclose: true,
      daysOfWeekDisabled: "0,1,2,3,4,5",
      format: 'yyyy-mm-dd',
    })
    //Timepicker
    $('#box-calendar-time').timepicker({
      showInputs: false,
      showSeconds: true,
      showMeridian: false
    })
    $('.select2').select2({
      theme: 'bootstrap4'
    })
</script>
</body>
</html>


