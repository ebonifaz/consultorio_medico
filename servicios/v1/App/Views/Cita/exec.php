<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<?php  
	$citas = (array_key_exists("citas", $this->params['parametros']))?$this->params['parametros']['citas']:[]; 
	$page = $this->params['parametros']['page']; 
	$limite_paginacion = $this->params['parametros']['limite_paginacion']; 
	$indice = (($page-1) * $limite_paginacion) + 1;
	$numeroElementos = $this->params['parametros']['numeroElementos']; 
?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

  <div class="content-wrapper">
	<?php echo cargar_header('Citas');?> 

	<div class="content">
	  <div class="container-fluid">
		<div class="row"> 
			<div class="card col-12">
				<div class="card-header ">
					<div class="d-flex justify-content-between">
						<div>Listado</div> 
						<div class="nuevo">
							<a class='btn btn-default mr-1' data-toggle='tooltip' title='' href='/servicios/v1/cita/crear' data-original-title='Nuevo'><i class='fa fa-plus'></i></a>
						</div>
					</div>
				</div>
				<div class="card-body"> 
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th style="width: 15px">#</th>
								<th>Paciente</th> 
								<th>Doctor</th>
								<th>Fecha</th>
								<th>Hora</th>
								<th>Historia Clínica</th>
								<th>Estado</th>
								<th style="width: 40px">Acciones</th>
							</tr>  
							<?php foreach ($citas as $cita) { ?>
								<tr>
									<td><?php echo $indice; ?>.</td>
									<td><?php echo $cita['paciente']; ?></td>
									<td><?php echo $cita['doctor']; ?></td>
									<td><?php echo date('j M Y', strtotime($cita['hora_solicitada'])); ?></td> 
									<td><?php echo date('H:i', strtotime($cita['hora_solicitada'])); ?></td> 
									<td> 
										<?php if( isset($cita['id_historia_clinica']) ){ ?> 
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Editar Historia Clinica" href="/servicios/v1/historialclinica/editar/<?php echo $cita['id_historia_clinica']; ?>"><i class="fa fa-edit"></i></a>
										<?php }else{ ?>
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Editar Historia Clinica" href="/servicios/v1/historialclinica/crear"><i class="fa fa-plus"></i></a> 
										<?php }?>
									</td>
									<td><small class="badge badge-<?php echo $cita['estado_tipo']; ?>"><?php echo $cita['estado']; ?></small></td>
									<td>
										<div style="display: flex;">
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Editar" href="/servicios/v1/cita/editar/<?php echo $cita['id']; ?>"><i class="fa fa-edit"></i></a>
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Ver" href="/servicios/v1/cita/editar/<?php echo $cita['id']; ?>"><i class="fa fa-eye"></i></a>
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Borrar"href="/servicios/v1/cita/borrar/<?php echo $cita['id']; ?>"><i class="fa fa-trash"></i></a>
										</div>
									</td>
								</tr>
								<?php $indice += 1; ?>
							<?php } ?>
						</tbody>
					</table> 
				</div>
				<div class="card-footer">
					<?php paginacion($page,$numeroElementos);?> 
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>
