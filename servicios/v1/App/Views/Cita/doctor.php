<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

    <?php
        $id_doctor = (array_key_exists('id_doctor',$this->params['parametros']))?$this->params['parametros']['id_doctor']:"";
    ?>

  <div class="content-wrapper">
	<?php echo cargar_header("Agenda");?> 

	<div class="content">
	  <div class="container-fluid"> 
      <div class="row">      
        <div class="card col-12">
            <div class="card-header ">
                <div class="d-flex justify-content-between"> 
                </div>
            </div>
            <div class="card-body"> 
                <div class="container"> 
                    <div class="row">
                        <div id="content" class="col-lg-12">
                            <div id="calendar"></div>
                            <div class="modal fade" id="modal-event" tabindex="-1" role="dialog" aria-labelledby="modal-eventLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="event-title"></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div id="event-description"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div>
            <div class="card-footer"> 
            </div>
        </div>
      </div>
	  </div>
	</div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 

<script>
function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}

var hoy = new Date();
var dd = hoy.getDate();
if(dd<10) {
    dd='0'+dd;
} 
 
if(mm<10) {
    mm='0'+mm;
}

var mm = hoy.getMonth()+1;
var yyyy = hoy.getFullYear();

dd=addZero(dd);
mm=addZero(mm);

$(document).ready(function() {
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        defaultDate: yyyy+'-'+mm+'-'+dd,
        buttonIcons: true, // show the prev/next text
        weekNumbers: true,
        editable: false,
        eventLimit: true, // allow "more" link when too many events 
        events: '/servicios/v1/cita/listadobydoctorjson/<?php echo $id_doctor; ?>',
        dayClick: function (date, jsEvent, view) { 
        }, 
        eventClick: function (calEvent, jsEvent, view) {
            $('#event-title').text(calEvent.title);
            $('#event-description').html(calEvent.description);
            $('#modal-event').modal();
        },  
    });
});

</script>

</body>
</html>