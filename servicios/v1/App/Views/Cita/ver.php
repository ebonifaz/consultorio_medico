<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

	<?php
		$cita_template = (array_key_exists('cita',$this->params['parametros']))?$this->params['parametros']['cita']:new CitasModel;
        $paciente = $cita_template->getPaciente();
        $doctor = $cita_template->getDoctor();
        $estado_cita  = $cita_template->getEstado();
        
    if( array_key_exists("validacion",$this->params['parametros']) ){
      $validacion = $this->params['parametros']['validacion'];
      $estado_validacion = $validacion->estado;
    }else{
      $estado_validacion = 1;
    }

    if( array_key_exists("success",$this->params['parametros']) ){
      $estado_success = $this->params['parametros']['success'];
      $mensaje_success = $this->params['parametros']['success_mensaje']; 
    }else{
      $estado_success = 0;
    }
    
    
	?>
  <div class="content-wrapper">
	<?php echo cargar_header('Cita');?> 

	<div class="content">
	  <div class="container-fluid">  
		<div class="row">      
            <div class="card col-12">
                <div class="card-header ">
                    <div class="d-flex justify-content-between">
                        <div><strong>Ver Cita:</strong> <?php echo $paciente->nickname;?></div>
                        <a href="/servicios/v1/cita/" class="btn btn-primary">Todas las citas</a>
                    </div>
                </div>
                <div class="card-body"> 
                    <div class="card-text">
                        <div class="row mb-1">
                            <div class="col-2"><strong>ID: </strong></div>
                            <div class="col-10"><?php echo($cita_template->id); ?></div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Paciente: </strong></div>
                            <div class="col-10"><?php echo($paciente->nombre); ?> <?php echo($paciente->apellido); ?></div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Doctor: </strong></div>
                            <div class="col-10"><?php echo($doctor->nombre); ?> <?php echo($doctor->apellido); ?></div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Hora de Atención: </strong></div>
                            <div class="col-10"><?php echo($cita_template->getFecha()); ?> <?php echo($cita_template->getHora()); ?></div>
                        </div> 
                        <div class="row mb-1">
                            <div class="col-2"><strong>Estado: </strong></div>
                            <div class="col-10">
                                <small class="badge badge-<?php echo $estado_cita->tipo; ?>"><?php echo $estado_cita->nombre; ?></small>
                            </div>
                        </div> 
                        <div class="row mb-1">
                            <div class="col-2"><strong>Observación: </strong></div>
                            <div class="col-10"><?php echo($cita_template->observacion); ?></div>
                        </div> 
                    </div>
                </div>
            </div>
		</div>
	  </div>
	</div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>