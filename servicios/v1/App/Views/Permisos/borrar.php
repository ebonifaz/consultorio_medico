<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

	<?php 
		$permisos_template = (array_key_exists('permiso',$this->params['parametros']))?$this->params['parametros']['permiso']:new PermisosModel;
        
    if( array_key_exists("validacion",$this->params['parametros']) ){
      $validacion = $this->params['parametros']['validacion'];
      $estado_validacion = $validacion->estado;
    }else{
      $estado_validacion = 1;
    }

    if( array_key_exists("success",$this->params['parametros']) ){
      $estado_success = $this->params['parametros']['success'];
      $mensaje_success = $this->params['parametros']['success_mensaje']; 
    }else{
      $estado_success = 0;
    }
    
    
	?>
  <div class="content-wrapper">
	<?php echo cargar_header("Permisos");?> 

	<div class="content">
	  <div class="container-fluid"> 

		<div class="row">      
            <div class="card col-12">
                <div class="card-header ">
                    <div class="d-flex justify-content-between">
                        <div><strong>Eliminar permiso:</strong> <?php echo $permisos_template->nombre;?></div>
                        <a href="/servicios/v1/permisos/" class="btn btn-primary">Todos los permisos</a>
                    </div>
                </div>
                <div class="card-body"> 
                    <div class="card-text">
                        <div class="row mb-1"> 
                            <div class="col-12">Seguro que desea eliminar el <?php echo($permisos_template->nombre); ?></div> 
                        </div> 
                    </div>
                </div>
                <div class="card-footer">
                    <form class="col-12" method="POST" action="">
                        <input type="hidden" value="<?php echo $permisos_template->id ?>" name="id">
                        <button type="submit" class="btn btn-primary">Si, deseo eliminar</button>
                    </form>
                </div>
            </div>
		</div>
	  </div>
	</div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>