<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<?php 
	$permisos = (isset($this->params['parametros']['permisos']))?$this->params['parametros']['permisos']:[];
	$page = $this->params['parametros']['page']; 
	$limite_paginacion = $this->params['parametros']['limite_paginacion']; 
	$indice = (($page-1) * $limite_paginacion) + 1;
	$numeroElementos = $this->params['parametros']['numeroElementos']; 
?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

  <div class="content-wrapper">
	<?php echo cargar_header('Permisos');?> 

	<div class="content">
	  <div class="container-fluid">
		<div class="row"> 
			<div class="card col-12">
				<div class="card-header ">
					<div class="d-flex justify-content-between">
						<div>Listado</div> 
						<div class="nuevo">
							<a class='btn btn-default mr-1' data-toggle='tooltip' title='' href='/servicios/v1/permisos/crear' data-original-title='Nuevo'><i class='fa fa-plus'></i></a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<?php mensaje_flash_view() ?>
					<table class="table table-bordered">
						<tbody>
							<tr>
							<th style="width: 15px">#</th>
							<th>Nombre</th>
							<th>Descripción</th> 
							<th style="width: 40px">Acciones</th>
							</tr> 
							<?php $indice = 1?>
							<?php foreach ($permisos as $permiso) { ?>
								<tr>
									<td><?php echo $indice; ?>.</td>
									<td><?php echo $permiso->nombre; ?></td> 
									<td><?php echo $permiso->detalle; ?></td>  
									<td>
										<div style="display: flex;">
										<a class="btn btn-default mr-1" data-toggle="tooltip" title="Editar" href="/servicios/v1/permisos/editar/<?php echo $permiso->id; ?>"><i class="fa fa-edit"></i></a>
										<a class="btn btn-default mr-1" data-toggle="tooltip" title="Ver" href="/servicios/v1/permisos/ver/<?php echo $permiso->id; ?>"><i class="fa fa-eye"></i></a>
										<a class="btn btn-default mr-1" data-toggle="tooltip" title="Borrar"href="/servicios/v1/permisos/borrar/<?php echo $permiso->id; ?>"><i class="fa fa-trash"></i></a>
										</div>
									</td>
								</tr>
								<?php $indice += 1; ?>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<div class="card-footer">
					<?php paginacion($page,$numeroElementos);?> 
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>
