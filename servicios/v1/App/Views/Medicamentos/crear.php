<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

	<?php 
		$medicamento_template = (array_key_exists('medicamento',$this->params['parametros']))?$this->params['parametros']['medicamento']:new MedicamentosModel;
 
    if( array_key_exists("validacion",$this->params['parametros']) ){
      $validacion = $this->params['parametros']['validacion'];
      $estado_validacion = $validacion->estado;
    }else{
      $estado_validacion = 1;
    }

    if( array_key_exists("success",$this->params['parametros']) ){
      $estado_success = $this->params['parametros']['success'];
      $mensaje_success = $this->params['parametros']['success_mensaje']; 
    }else{
      $estado_success = 0;
    } 
	?>
    <div class="content-wrapper">
	<?php echo cargar_header("Medicamentos");?> 
	    <div class="content">
	        <div class="container-fluid">
		        <div class="row">   
                    <div class="card col-12">
                        <div class="card-header ">
                            <div class="d-flex justify-content-between">
                                <div><strong>Crear medicamento</strong></div>
                                <a href="/servicios/v1/medicamentos/" class="btn btn-primary">Todos los medicamentos</a>
                            </div>
                        </div>
                        <form class="col-12" method="POST" action=""> 
                            <div class="card-body"> 
                                <?php 
                                    if( ! $estado_validacion ){
                                    mostrar_errores($validacion);
                                    }
                                    if( $estado_success ){
                                    mostrar_success($mensaje_success);
                                    }
                                ?>
                 
                                <div class="form-group">
                                    <label for="nombre_form_crear_medicamento">Nombre</label>
                                    <input type="text" class="form-control" id="nombre_form_crear_medicamento" name="nombre" placeholder="Nombre" value="<?php echo $medicamento_template->nombre ?>">
                                </div>                                
                            </div> 

                            <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </form>
                    </div>
		        </div>
	        </div>
	    </div>
    </div> 
    <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>


