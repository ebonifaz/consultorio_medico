<?php 

function cargar_header($titulo,$url="",$nombre = "Nuevo")
{
		$texto = "
		<div class='content-header'>
			<div class='container-fluid'>
				<div class='row mb-2'>
					<div class='col-sm-6'>
						<h1 class='m-0'>{$titulo}</h1>
					</div>";
					if( $url != "" ){
						$texto .= "<div class='col-sm-6 d-flex justify-content-end'>
					</div>";
					}
					$texto .= "</div>
			</div>
		</div>";
		return $texto;
}

function mostrar_errores($validaciones)
{
?>
		<div class="alert alert-danger col-12" role="alert">
			<ul>
			<?php foreach ( $validaciones->errores as $key => $error) { ?> 
				<li>Error en <strong><?php echo($key); ?></strong>.- <?php echo($error['mensaje']); ?></li> 
			<?php } ?>
			</ul>
      	</div>
<?php
}

function mostrar_success($mensaje_success)
{
?>
		<div class="alert alert-success col-12" role="alert">
			<p class="mb-0"><?php echo $mensaje_success; ?></p>
      	</div>
<?php
}
function mostrar_danger($mensaje_danger)
{
?>
		<div class="alert alert-danger col-12" role="alert">
			<p class="mb-0"><?php echo $mensaje_danger; ?></p>
      	</div>
<?php
}

function mensaje_flash_view()
{
	$session_obj = new Session; 
	$mensaje_flash = $session_obj->getflash();

	if( array_key_exists('mensaje', $mensaje_flash) ){
		switch ($mensaje_flash['tipo']) {
			case 'success':
				mostrar_success($mensaje_flash['mensaje']);
				break;
			
			case 'danger':
				mostrar_danger($mensaje_flash['mensaje']);
				break;
			default:
				# code...
				break;
		} 
	}	
	

	// dd( $mensaje_flash['mensaje'] ); 
	# code...
}

function paginacion($page,$total){  
	$route = new Router;

	$anterior = ( $page > 1 )?true:false;
	$siguiente = ( ($page*LIMITE_PAGINACION) < $total )?true:false;

	if( $anterior ){
		$url_anterior = $route->getUrlByPage( ($page - 1) );
	}

	if( $siguiente ){
		$url_siguiente = $route->getUrlByPage( ($page + 1) );
	}

	$pag_posibles = (int)($total / LIMITE_PAGINACION);
	$pag_posibles = $pag_posibles + (($total % LIMITE_PAGINACION > 0)?1:0);
	
	$maximo_elemem_paginacion = MAX_ELEM_PAGINACION;
	$maximo_elemem_paginacion = ($pag_posibles<$maximo_elemem_paginacion)?$pag_posibles:$maximo_elemem_paginacion;

	$pag_faltante = $pag_posibles - $page; 
	$pag_inicio = $page - (int)($maximo_elemem_paginacion / 2);
	$pag_inicio = ( $pag_inicio <= 0 )?1:$pag_inicio;
	
	if( $pag_faltante < ($maximo_elemem_paginacion / 2)  ){
		$pag_inicio = $pag_posibles - $maximo_elemem_paginacion + 1;
	}

	?>
	<nav aria-label="Page navigation example">
		<ul class="pagination">
			<li class="page-item <?php echo (!$anterior)?'disabled':'';?>">
				<a class="page-link" href="<?php echo ($anterior)?$url_anterior:"#";?>" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>
			<?php 
				for ($i=0; $i < $maximo_elemem_paginacion; $i++) {  
					
			?>
				<li class="page-item <?php echo ($page==$pag_inicio)?"active":"";?>">
					<a class="page-link" href="<?php echo $route->getUrlByPage( $pag_inicio ) ?>">
						<?php echo $pag_inicio ?>
					</a>
				</li>
			<?php 
				$pag_inicio ++;
				}
			?>
			<li class="page-item  <?php echo (!$siguiente)?'disabled':'';?>">
				<a class="page-link" href="<?php echo ($siguiente)?$url_siguiente:"#";?>" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>
		</ul>
	</nav>
	<?php
}