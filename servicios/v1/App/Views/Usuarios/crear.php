<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>


	<?php 
		$user_template = (array_key_exists('usuario',$this->params['parametros']))?$this->params['parametros']['usuario']:new UserModel;
		$listado_roles = $this->params['parametros']['listado_roles'];
 
    if( array_key_exists("validacion",$this->params['parametros']) ){
      $validacion = $this->params['parametros']['validacion'];
      $estado_validacion = $validacion->estado;
    }else{
      $estado_validacion = 1;
    }

    if( array_key_exists("success",$this->params['parametros']) ){
      $estado_success = $this->params['parametros']['success'];
      $mensaje_success = $this->params['parametros']['success_mensaje']; 
    }else{
      $estado_success = 0;
    } 
	?>
  <div class="content-wrapper">
	<?php echo cargar_header("Usuarios");?> 
	  <div class="content">
	    <div class="container-fluid">
		    <div class="row">   
          <div class="card col-12">
            <div class="card-header ">
                <div class="d-flex justify-content-between">
                    <div><strong>Crear usuario</strong></div>
                    <a href="/servicios/v1/usuarios/" class="btn btn-primary">Todos los usuarios</a>
                </div>
            </div>
            <form class="col-12" method="POST" action=""> 
              <div class="card-body"> 
                <?php 
                  if( ! $estado_validacion ){
                  mostrar_errores($validacion);
                  }
                  if( $estado_success ){
                  mostrar_success($mensaje_success);
                  }
                ?>
                <div class="form-group">
                  <label for="email_form_editar_user">Correo electrónico</label>
                  <input type="email" class="form-control" id="email_form_editar_user" name="email" placeholder="Correo electrónico" value="<?php echo $user_template->email ?>">
                </div>
                <div class="form-group">
                  <label for="Usuario_form_editar_user">Usuario</label>
                  <input type="text" class="form-control" id="Usuario_form_editar_user" name="nickname" placeholder="Usuario" value="<?php echo $user_template->nickname ?>">
                </div> 
                <div class="form-group">
                  <label for="contrasenia_form_editar_user">Contaseña</label>
                  <input type="password" class="form-control" id="contrasenia_form_editar_user" name="contrasenia" placeholder="Contaseña" value="">
                </div> 
                <div class="form-group">
                  <label for="confirmar_contrasenia_form_editar_user">Confirmar Contraseña</label>
                  <input type="password" class="form-control" id="confirmar_contrasenia_form_editar_user" name="contrasenia_confirmar" placeholder="Confirmar contaseña" value="">
                </div> 
                <div class="form-group">
                  <label for="identificacion_form_editar_user">Identificación</label>
                  <input type="text" class="form-control" id="identificacion_form_editar_user" name="identificacion" placeholder="Identificación" value="<?php echo $user_template->identificacion ?>">
                </div> 
                <div class="form-group">
                  <label for="nombre_form_editar_user">Nombre</label>
                  <input type="text" class="form-control" id="nombre_form_editar_user" name="nombre" placeholder="Nombre" value="<?php echo $user_template->nombre ?>">
                </div> 
                <div class="form-group">
                  <label for="apellido_form_editar_user">Apellido</label>
                  <input type="text" class="form-control" id="apellido_form_editar_user" name="apellido" placeholder="Apellido" value="<?php echo $user_template->apellido ?>">
                </div>  
                <div class="custom-control custom-switch">
                  <input type="checkbox" class="custom-control-input" id="estado_form_editar_user" name="estado" value="1"
                  <?php echo ( $user_template->estado ) ? 'checked' : '' ;?> >
                  <label class="custom-control-label" for="estado_form_editar_user">Estado</label>
                </div>
                <div class="form-group">
                  <label>Listado de Roles</label> 
                  <select multiple="" class="form-control" name="roles[]">
                    <?php foreach ($listado_roles as $rol) { ?>
                      <option value="<?php echo($rol->id) ?>" <?php echo ($user_template->hasRoles($rol))?"selected":"";?>><?php echo($rol->nombre) ?></option>
                    <?php }?>
                  </select>
                </div>
                              
              </div> 

              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Enviar</button>
              </div>
            </form>
          </div>
		    </div>
	    </div>
	  </div>
  </div> 
  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>


