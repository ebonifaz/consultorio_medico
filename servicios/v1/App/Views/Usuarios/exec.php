<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<?php 
	$usuarios = $this->params['parametros']['usuarios']; 
	$page = $this->params['parametros']['page']; 
	$limite_paginacion = $this->params['parametros']['limite_paginacion']; 
	$indice = (($page-1) * $limite_paginacion) + 1;
	$numeroElementos = $this->params['parametros']['numeroElementos']; 
?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

  <div class="content-wrapper">
	<?php echo cargar_header('Usuarios',"","");?> 

	<div class="content">
	  <div class="container-fluid">
		<div class="row">
			<div class="card col-12">
				<div class="card-header ">
					<div class="d-flex justify-content-between">
						<div>Listado</div> 
						<div class="nuevo">
							<a class='btn btn-default mr-1' data-toggle='tooltip' title='' href='/servicios/v1/usuarios/crear' data-original-title='Nuevo'><i class='fa fa-plus'></i></a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<?php mensaje_flash_view() ?>
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th style="width: 15px">#</th>
								<th>Usuario</th> 
								<th>Identificación</th>
								<th>Correo</th>
								<th>Roles</th>
								<th style="width: 40px">Acciones</th>
							</tr>  
							<?php foreach ($usuarios as $usuario) { ?>
								<tr>
									<td><?php echo $indice; ?>.</td>
									<td><?php echo $usuario->nickname; ?></td> 
									<td><?php echo $usuario->identificacion; ?></td>
									<td><?php echo $usuario->email; ?></td>
									<td><?php foreach ($usuario->roles as $rol) { ?>
											<small class="badge bg-blue"><?php echo ($rol->nombre); ?></small> 
										<?php } ?>
									</td>
									<td>
										<div style="display: flex;">
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Editar" href="/servicios/v1/usuarios/editar/<?php echo $usuario->id; ?>"><i class="fa fa-edit"></i></a>
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Ver" href="/servicios/v1/usuarios/ver/<?php echo $usuario->id; ?>"><i class="fa fa-eye"></i></a>
											<a class="btn btn-default mr-1" data-toggle="tooltip" title="Borrar"href="/servicios/v1/usuarios/borrar/<?php echo $usuario->id; ?>"><i class="fa fa-trash"></i></a>
										</div>
									</td>
								</tr>
								<?php $indice += 1; ?>
							<?php } ?>
						</tbody>
					</table> 
				</div>
				<div class="card-footer">
					<?php paginacion($page,$numeroElementos);?> 
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>
