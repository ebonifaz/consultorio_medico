<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

	<?php
		$user_template = (array_key_exists('usuario',$this->params['parametros']))?$this->params['parametros']['usuario']:new UserModel;
        
    if( array_key_exists("validacion",$this->params['parametros']) ){
      $validacion = $this->params['parametros']['validacion'];
      $estado_validacion = $validacion->estado;
    }else{
      $estado_validacion = 1;
    }

    if( array_key_exists("success",$this->params['parametros']) ){
      $estado_success = $this->params['parametros']['success'];
      $mensaje_success = $this->params['parametros']['success_mensaje']; 
    }else{
      $estado_success = 0;
    }
    
    
	?>
  <div class="content-wrapper">
	<?php echo cargar_header('Usuarios');?> 

	<div class="content">
	  <div class="container-fluid"> 

		<div class="row">      
            <div class="card col-12">
                <div class="card-header ">
                    <div class="d-flex justify-content-between">
                        <div><strong>Ver Usuario:</strong> <?php echo $user_template->nickname;?></div>
                        <a href="/servicios/v1/usuarios/" class="btn btn-primary">Todos los usuarios</a>
                    </div>
                </div>
                <div class="card-body"> 
                    <div class="card-text">
                        <div class="row mb-1">
                            <div class="col-2"><strong>ID: </strong></div>
                            <div class="col-10"><?php echo($user_template->id); ?></div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Correo electrónico: </strong></div>
                            <div class="col-10"><?php echo($user_template->email); ?></div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Usuario: </strong></div>
                            <div class="col-10"><?php echo($user_template->nickname); ?></div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Nombres: </strong></div>
                            <div class="col-10"><?php echo($user_template->nombre); ?></div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Apellidos: </strong></div>
                            <div class="col-10"><?php echo($user_template->apellido); ?></div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Identificación: </strong></div>
                            <div class="col-10"><?php echo($user_template->identificacion); ?></div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Roles: </strong></div>
                            <div class="col-10">
                                <?php foreach ($user_template->roles as $rol) { ?>
                                    <small class="badge bg-blue"><?php echo ($rol->nombre); ?></small> 
                                <?php } ?> 
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-2"><strong>Estado: </strong></div>
                            <div class="col-10">                     
                                <?php if( $user_template->estado ){ ?>
                                    <i class="fa fa-fw fa-check-circle" data-toggle="tooltip" title="Activado" ></i>
                                <?php }else{ ?>
                                    <i class="fa fa-fw fa-ban" data-toggle="tooltip" title="Desactivado" ></i>
                                <?php } ?></td>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	  </div>
	</div>
  </div>




  <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>