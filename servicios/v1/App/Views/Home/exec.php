<!DOCTYPE html>
<html lang="es">
<?php require_once( ROOT . PATH_VIEWS . "partes/home/header.php" ); ?>
<body class="hold-transition login-page">
<div class="login-box">
	<!-- /.login-logo -->
	<div class="card card-outline card-primary">
		<div class="card-header text-center">
			<a href="../../index2.html" class="h1"><b>Consult. </b>SALITRE</a>
		</div>
		<div class="card-body">
			<form action="<?php echo FOLDER_PATH . "/login/ingreso";?>" method="POST" id="form_login">
				<div class="input-group mb-3">
					<input type="email" class="form-control" placeholder="Email" id="correo" name="correo" value="enrique_e449@hotmail.com">
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-envelope"></span>
						</div>
					</div>
				</div>
				<div class="input-group mb-3">
					<input type="password" class="form-control" placeholder="Contraseña" id="contrasenia" name="contrasenia" value="admin">
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-lock"></span>
						</div>
					</div>
				</div>
				<div class="row"> 
					<!-- /.col -->
					<div class="col-12">
						<button type="submit" class="btn btn-primary btn-block">Ingresar</button>
					</div>
					<!-- /.col -->
				</div>
			</form> 

			<p class="mb-1 text-right">
				<a href="forgot-password.html">Olvidé mi contraseña</a>
			</p> 
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>
<!-- /.login-box -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<script>
	const form = document.getElementById('form_login');
	form.addEventListener("submit",function(e){
		e.preventDefault();
				let http = new XMLHttpRequest();
				let url = "<?php echo FOLDER_PATH . "/login/ingreso"; ?>";
				let data = new FormData(form);
				http.open("POST", url, true);
				http.onreadystatechange = function(){
						if( http.readyState == 4 && http.status == 200 ){
				let resp = JSON.parse( http.responseText );
				if( resp.response == true ){
					toastr.success("Ingreso Exitoso. Redirigiendo ..."); 
					window.location.href=resp.ruta;
				}else{
					toastr.error("Ingreso Fallido: " + resp.mensaje );
				}
						} 
				}
				http.send(data);
	});
</script>
</body>
</html>
