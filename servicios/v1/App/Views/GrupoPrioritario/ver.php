<!DOCTYPE html>
<html lang="en">
<?php require_once( ROOT . PATH_VIEWS . "partes/dashboard/header.php" ); ?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/menu.php" ); ?>
	<?php require_once( ROOT . PATH_VIEWS . "partes/general/sidebar.php" ); ?>

	<?php  
		$grupop_template = (array_key_exists('grupoprioritario',$this->params['parametros']))?$this->params['parametros']['grupoprioritario']:new GrupoPrioritarioModel;
        
        if( array_key_exists("validacion",$this->params['parametros']) ){
            $validacion = $this->params['parametros']['validacion'];
            $estado_validacion = $validacion->estado;
        }else{
            $estado_validacion = 1;
        }

        if( array_key_exists("success",$this->params['parametros']) ){
            $estado_success = $this->params['parametros']['success'];
            $mensaje_success = $this->params['parametros']['success_mensaje']; 
        }else{
            $estado_success = 0;
        }
    
    
	?>
    <div class="content-wrapper">
	<?php echo cargar_header('Grupo Prioritario');?> 

	<div class="content">
	    <div class="container-fluid"> 

		<div class="row">      
            <div class="card col-12">
                <div class="card-header ">
                    <div class="d-flex justify-content-between">
                        <div><strong>Ver grupo:</strong> <?php echo $grupop_template->nombre;?></div>
                        <a href="/servicios/v1/grupoprioritario/" class="btn btn-primary">Todos los grupos</a>
                    </div>
                </div>
                <div class="card-body"> 
                    <div class="card-text">
                        <div class="row mb-1">
                            <div class="col-2"><strong>ID: </strong></div>
                            <div class="col-10"><?php echo($grupop_template->id); ?></div>
                        </div> 
                        <div class="row mb-1">
                            <div class="col-2"><strong>Nombre: </strong></div>
                            <div class="col-10"><?php echo($grupop_template->nombre); ?></div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	    </div>
	</div>
    </div>


    <?php include ROOT . PATH_VIEWS . "partes/general/pie_container.php";?>
</div>

<!-- REQUIRED SCRIPTS -->

<?php include ROOT . PATH_VIEWS . "partes/home/footer.php";?>
<?php include ROOT . PATH_VIEWS . "partes/dashboard/footer.php";?> 
</body>
</html>