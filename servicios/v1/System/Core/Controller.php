<?php 
defined('BASEPATH') or exit('No se permite acceso directo');

/**
* Controlador base
*/
abstract class Controller 
{
	private $view;
	
	function __construct()
	{
	}
	
	protected function render( $controller_name = "", $metodo = "", $params = array())
	{
		$this->view = new View($controller_name, $metodo, $params);
	}

	/**
	 * Metodo Default para las rutas
	 */
  	abstract public function exec( $route, $params );
	  
	public function show($funcion, $params)
	{
		$params = array("parametros" => $params);
		$this->render(get_class($this), $funcion , $params);
	}
}