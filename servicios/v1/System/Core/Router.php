<?php 
defined('BASEPATH') or exit('No se permite acceso directo');
 
class Router
{
	public $uri; 
	public $controller;
	public $metodo;
	public $params;
	public $funcion;
	public $params_get = [
		'page' => 1,
		'orderby' => ''
	];

	function __construct()
	{ 
		$this->setUri(); 
		$this->setController();
		$this->setParams(); 
		$this->setMetodo();
		$this->setFuncion();
	}

	/**
	* Asigna la uri completa
	*/
	public function setUri()
	{ 
		$url = str_replace(FOLDER_PATH, "",URI);
		$this->uri = explode('/', $url);
	} 

	/**
	* Asigna el nombre del Controllador
	*/
	public function setController()
	{
		$params_gets = explode('?', $this->uri[1]);


		if( ! empty($params_gets[1]) ){
			$params_get = explode('&', $params_gets[1]);
			foreach ($params_get as $params) {
				$param = explode('=', $params); 
				if( $param[0] == 'page' ){
					($this->params_get)['page'] = !($param[1])?"1":$param[1];
				}
				if( $param[0] == 'orderby' ){
					($this->params_get)['orderby'] = !($param[1])?"":$param[1];
				}
			}
		}

		$this->controller = ! empty($params_gets[0]) ? $params_gets[0] : ''; 
	}

	/**
	* Asigna el nombre del Controllador
	*/
	public function setFuncion()
	{
		if( empty($this->uri[2]) ){
			$this->funcion = 'exec';
			return;
		}
		
		$params_gets = explode('?', $this->uri[2]);
		
		if( ! empty($params_gets[1]) ){
			$params_get = explode('&', $params_gets[1]);
			foreach ($params_get as $params) {
				$param = explode('=', $params); 
				if( $param[0] == 'page' ){
					($this->params_get)['page'] = !($param[1])?"1":$param[1];
				}
				if( $param[0] == 'orderby' ){
					($this->params_get)['orderby'] = !($param[1])?"":$param[1];
				}

			}
			
		}
		$this->funcion = ! empty($params_gets[0]) ? $params_gets[0] : '';  
	}

	/**
	* Asigna el valor de los parametros si existe segun el metodo de peticion
	*/
	public function setParams()
	{  
		if(REQUEST_METHOD === 'POST' ){
		  $this->params = $_POST;
		}
		else if (REQUEST_METHOD === 'GET'){
			if( ! empty($this->uri[2]) ){
				$elementos_url = $this->uri;   
				array_shift($elementos_url); 
				array_shift($elementos_url); 
				array_shift($elementos_url);  
				$this->params = $elementos_url;  
			}   
		}
	}

	/**
	* Asigna el valor del método del Request
	*/
	public function setMetodo()
	{ 
		$this->metodo = REQUEST_METHOD; 
	}

	/**
	* @return $uri
	*/
	public function getUri()
	{
		return $this->uri;
	} 

	public function getPage()
	{
		return ($this->params_get)['page'];
	}
	
	/**
	* @return $controller
	*/
	public function getController()
	{
		return ucfirst($this->controller) . "Controller";
	}

	/**
	* @return $metodo
	*/
	public function getMetodo()
	{
		return $this->metodo;
	}

	/**
	* @return $params
	*/
	public function getParams()
	{
		return $this->params;
	}

	/**
	* @return $funcion
	*/
	public function getFuncion()
	{
		return $this->funcion;
	}

	public function getUrlByPage($id){
		
		$controlador = SEPARADOR_URL . $this->controller;
		$funcion = ( $this->funcion != "" )?SEPARADOR_URL . $this->funcion:"";
		$page = "?page=" . $id ;
		return FOLDER_PATH . $controlador . $funcion. $page;
	}
}