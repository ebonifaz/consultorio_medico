<?php
defined('BASEPATH') or exit('No se permite acceso directo');

abstract class Model { 
    
    protected $campos = [];
    protected $table = "";
    protected $paginacion = LIMITE_PAGINACION;

    public function __construct()
    { 
    }  
    
    public function getCampos()
    {
        return $this->campos;
    }

    protected function cargar_de_array_indice($UsuariosArray,$indice,$campos=false)
    { 
        $UsuariosArrayFirst = $UsuariosArray[$indice];
        $this->cargar_de_array($UsuariosArrayFirst,$campos); 
        return $this;
    }
    
    public function cargar_de_array($UsuariosArray,$campos=false)
    {   
        $campos = (!$campos)?$this->campos:$campos; 
        foreach ($campos as $valcampo => $campo) { 
            if( array_key_exists($valcampo,$UsuariosArray) ){
                $this->$valcampo = $UsuariosArray[$valcampo];
            }
        }
        return $this;
    }

    public function get($id)
    {
        $conexion = new BaseDatos();
        $id = $conexion->db->real_escape_string($id); 
        $sql = "SELECT * FROM {$this->table} WHERE id='{$id}'"; 
        $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC);  
        $this->cargar_de_array_indice( $result, 0 );
    }

    public function all()
    {
        return $this->fun_all_callback();
    }

    public function all_callback($fun_callback=false)
    { 
        return $this->fun_all_callback($fun_callback);
    }

    private function fun_all_callback($fun_callback=false){
        
        $array_user = []; 

        $router = new Router();
        $page = $router->getPage();
        $paginacion = ($page - 1) * ($this->paginacion);
        $campos = implode(",", array_keys($this->campos) ) ; 
        $sql = "SELECT {$campos} FROM {$this->table} LIMIT {$paginacion},{$this->paginacion}"; 
        $bd = new BaseDatos();
        $resultado = $bd->db->query( $sql )->fetch_all(MYSQLI_ASSOC); 

        foreach ($resultado as $objeto) {
            $class_name = get_class($this);
            $objetoObj = new $class_name;
            $objetoObj->cargar_de_array( $objeto );
            if( $fun_callback ){
                $objetoObj->$fun_callback( $objeto );
            }            
            array_push( $array_user, $objetoObj );
        } 
        return $array_user;
    }

    public function numeroElementos()
    {
        $conexion = new BaseDatos(); 
        $sql = "SELECT count(*) as cantidad FROM {$this->table} LIMIT 1"; 
        $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC); 
        return $result[0]['cantidad']; 
    }

    public function update()
    { 
        $id = 0;
        $sets = []; 
        foreach ($this->campos as $keyCampos => $campo) {
            switch ($keyCampos) {
                case 'id':
                    $id = $this->$keyCampos;
                    break; 
                default:
                    $valor = $this->generarTextSetUpdate($keyCampos,$campo);
                    if( $valor != "-1"){
                        $sets[]= $valor;
                    }
                    break;
            } 
        }
        
        $val_sets = implode(",",$sets);
        $conexion = new BaseDatos(); 
        $sql = "UPDATE {$this->table} SET {$val_sets} WHERE  `id`={$id}";   
        // dd( $sql );
        try {
            $result = $conexion->db->query( $sql );
            return true;
        } catch (\Throwable $th) {
            return false;
        }
        return false;
    }

    public function save()
    { 
        $id = 0;
        $sets = []; 
        $vals = []; 
        foreach ($this->campos as $keyCampos => $campo) {
            switch ($keyCampos) {
                case 'id':
                    $id = $this->$keyCampos;
                    break; 
                default:
                    $value = $this->generarTextSetSave($keyCampos,$campo);
                    if(! ($value === '' || $value === "''") ){
                        $sets[]= "`{$keyCampos}`";
                        $vals[]= $this->generarTextSetSave($keyCampos,$campo);
                    }
                    break;
            } 
        } 
        $val_sets = implode(",",$sets);
        $val_vals = implode(",",$vals);
        $conexion = new BaseDatos(); 

        $sql = "INSERT INTO {$this->table} ({$val_sets}) VALUES ({$val_vals});"; 

        try {
            $result = $conexion->db->query( $sql );
            $sql = "SELECT LAST_INSERT_ID() AS id;";  
            $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC); 
            return $result[0]['id'];
        } catch (\Throwable $th) {
            return false;
        }
        return false;
    } 

    public function delete()
    { 
        $conexion = new BaseDatos(); 
        $sql = "DELETE FROM {$this->table} WHERE  `id`={$this->id}";
        try {
            $result = $conexion->db->query( $sql );
            return true;
        } catch (\Throwable $th) {
            return false;
        }
        return false; 
    }

    private function generarTextSetUpdate($keyCampos,$campo){ 
        switch ($campo['tipo']) {
            case 'text': 
                if( ! ($this->$keyCampos === '' || $this->$keyCampos === "''" || is_null($this->$keyCampos))  ){
                    return "`{$keyCampos}`='{$this->$keyCampos}'";
                }
                break;

            case 'password': 
                if( $this->$keyCampos == "") return;
                return "`{$keyCampos}`='{$this->$keyCampos}'";
                break;
                
            case 'int': 
            case 'checkbox': 
                if( ! ($this->$keyCampos === '' || $this->$keyCampos === "''" || is_null($this->$keyCampos))  ){
                    return "`{$keyCampos}`={$this->$keyCampos}"; 
                }
                break;
             
        }
        return "-1";
    }

    private function generarTextSetSave($keyCampos,$campo){
        switch ($campo['tipo']) {
            case 'text': 
                return "'{$this->$keyCampos}'";
                break;

            case 'password': 
                if( $this->$keyCampos == "") return;
                return "'{$this->$keyCampos}'";
                break;
                
            case 'int': 
            case 'checkbox': 
                return "{$this->$keyCampos}";
                break; 
        }
        return ;
    }
    public function __destruct()
    { 
    }
}
    