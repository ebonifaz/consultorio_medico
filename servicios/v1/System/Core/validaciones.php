<?php
defined('BASEPATH') or exit('No se permite acceso directo'); 

require_once( ROOT . FOLDER_PATH . SEPARADOR_URL . PATH_MODEL . "CitasModel.php" ); 


class Validaciones
{
    public $estado = 1;
    public $errores = [];


    function __construct($array_validaciones,$params)
    {
        $esValido = true;
        
        foreach ($array_validaciones as $val => $campos) {
            
            foreach ($campos as $key => $validation) {
                if($validation != ""){
                    $nombre_first = ( explode(":",$validation) )[0];
                    $name_funcion = 'es'. ucfirst( $nombre_first );
                    $result_validacion_funcion = $this->$name_funcion($params, $val,$validation);

                    //aqui van las funciones para validar :) 

                    if( !$result_validacion_funcion['estado'] ){
                        $esValido = false;
                        $this->errores[$val] = [
                            "validation" => $validation,
                            "mensaje" => $result_validacion_funcion['mensaje']                            
                        ];
                    }
                }
            }
        }
        $this->estado = ($esValido)?1:0;
    }

    public function esHoravalidada($params, $val, $validation)
    {
        
    }

    public function esRequerido($params, $val,$validation)
    {
        $valor = $params[$val];  
        $mensaje = "El campos es requerido";
        if( $valor != "" ){return ["estado" => true];}
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }


    public function esUnico($params, $val,$validation)
    {
        $mensaje = "este ".$val." no es único";

        $separar_campos = explode(":",$validation); 
        $nombre_tabla = "";
        if( array_key_exists('1',$separar_campos)){
            $nombre_tabla = $separar_campos[1];
        } 
        $valor = $params[$val];
        
        $conexion = new BaseDatos();
        $sql = "SELECT COUNT(*) AS num FROM {$nombre_tabla} WHERE {$val} = '{$valor}'"; 
        $result = $conexion->db->query( $sql )->fetch_all(MYSQLI_ASSOC);
        
        $validador = $result[0]['num']; 
        if ($validador == 0) {
            return [
                "estado" => true
            ];
        }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }


    public function esNombre($params, $val,$validation)
    {
        $valor = $params[$val]; 
        $mensaje = "No tiene el formato correcto de un nombre";
        $validador = $this->esString($valor);
        if( $validador ){ return ["estado" => true]; }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];

    }

    public function esApellido($params, $val,$validation)
    {
        $valor = $params[$val]; 
        $mensaje = "No tiene el formato correcto de un apellido";
        $validador = $this->esString($valor);
        if( $validador ){ return ["estado" => true]; }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];

    }

    public function esEmail($params, $val,$validation)
    {
        $valor = $params[$val]; 
        $mensaje = "No tiene el formato correcto de un Correo Electrónico";
        $matches = null;
        $format = '/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/';


        //$existsEmail = ;
        $regExp = (1 === preg_match( $format, $valor, $matches));
        $filter_email = filter_var($valor, FILTER_VALIDATE_EMAIL);

        if ($filter_email && $regExp) {
            return ["estado" => true];
        }

        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }
    public function esCedula($params, $val,$validation)
    {
        $valor = $params[$val]; 
        $mensaje = "No tiene el formato correcto de una cédula";
        
        //$existsEmail = ;
        $long = strlen($valor) == 10 ? true : false;
        $numero = $this->esNumerico($params, $val,$validation);
        $algorit = $this->algoritmo21($valor);

        if ($numero["estado"] && $long && $algorit ) {
            return ["estado" => true];
        }

        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }

   public function esNumerico($params, $val,$validation)
    {
        $valor = $params[$val]; 

        $mensaje = "No tiene el formato correcto de un número de cedula";

        $numFloat = floatval($valor);
        $numInt = intval($valor);

        if (is_numeric($valor)) {
            if (is_float($numFloat)) {
                return [
                    "estado" => true,
                    "date" => $numFloat,
                ];
            }else {
                return [
                    "estado" => true,
                    "data" => $numInt
                ];
            }
        }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }

    public function esFecha($params, $val,$validation)
    {
        $valor = $params[$val];
        $mensaje = "No tiene el formato correcto de la fecha";
        $validacion = $this->validateDateTime($valor, 'Y-m-d');
        if ($validacion) {
            return [ "estado"=> true ]; 
        }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }

    public function esHora($params, $val,$validation)
    {
        $valor = $params[$val];
        $mensaje = "No tiene el formato correcto de la hora";
        $validacion = $this->validateDateTime($valor, 'H:i:s');
        if ($validacion) {
            return [ "estado"=> true ]; 
        }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }

    public function esNickname($params, $val,$validation)
    {
        $valor = $params[$val]; 

        $mensaje = "No tiene el formato correcto de un nickname";
        $matches = null;
        $format = "/^([a-zA-Z0-9_-]){1,16}$/";

        $regExp = (1 === preg_match( $format, $valor, $matches));

        if ($regExp) {
            return ["estado" => true];
        }

        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    } 

    public function esPaciente($params, $val,$validation)
    {
        $mensaje ='No es un paciente';
        $valor = $params[$val];
        $model = new CitasModel();
        $validador = $model->validateID( $valor, 'paciente'); 
        

        if ($validador) {
            return [
                "estado" => true
            ];
        }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }

    public function esDoctor($params, $val,$validation)
    {
        $mensaje ='No es un doctor';
        $valor = $params[$val];
        $model = new CitasModel();
        $validador = $model->validateID($valor , 'doctor'); 
        

        if ($validador) {
            return [
                "estado" => true
            ];
        }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }

    public function esEstado($params, $val,$validation)
    {
        $mensaje ='No es un estado';
        $valor = $params[$val];
        $model = new CitasModel();
        $validador = $model->validateIdEstado($valor); 
        

        if ($validador['num'] >= 1) {
            return [
                "estado" => true
            ];
        }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }

    public function esAgendada($params, $val,$validation)
    {
        $mensaje = "Ya tiene una cita agendada en ese horario";
        
        $valor = $params[$val];
        $doctor = $params['id_doctor'];
        $hora_cita = explode(':',  $params['hora_solicitada_hora']);
        $fecha_cita = $params['hora_solicitada_fecha'];

        $model = new CitasModel();
        $lists = $model->validateEstado($valor, $doctor);
        $num_posibilidad = 0;
        foreach ($lists as $key => $value) {
            $nombre_estado = strtolower($value['nombre']);
            $horario = $value['hora'];
            $fecha = explode(' ', $value['hora_solicitada']);
            if ($nombre_estado == 'agendada') {
                $num_posibilidad++;
            }
            if ($horario == $hora_cita[0]) {
                $num_posibilidad++;
            }
            if ($fecha == $fecha_cita) {
                $num_posibilidad++;
            }
        }
        if ($num_posibilidad < 3) {
            return ["estado" => true];
        }
        return [
            "estado" => false,
            "mensaje" => $mensaje
        ];
    }

    



    public function validateDateTime($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        if ($d && $d->format($format) == $date) {
            return true; 
        }
        return false;
    }

    public function esString($valor)
    {
        $matches = null;
        $format = "/^([A-Za-zÑñ]+[áéíóú]?[A-Za-z]*){3,18}\s+([A-Za-zÑñ]+[áéíóú]?[A-Za-z]*){3,36}$/iu";

        $regExp = (1 === preg_match($format, $valor, $matches));

        if ($regExp) { return true; }
        return false;
    }

    public function algoritmo21($valor)
    {       
        $sub_ci = substr($valor,0,-1);
        $ultimo_digito = substr($valor,-1);
        $listado_cedula = str_split($sub_ci);
        $suma = 0;
		for ($i=0; $i < count( $listado_cedula ) ; $i++) { 
			if( $i % 2 == 0 ){
				$digito_i = $listado_cedula[$i] * 2;
				if( $digito_i > 9 ){
					$digito_i = $digito_i - 9;
				}
			}else{
				$digito_i = $listado_cedula[$i];
			}
			$suma = $suma + $digito_i;
		}
		$proxima_decena = ( $ultimo_digito + 1 ) * 10;
		$valor_validador = $proxima_decena - $suma; 
		

		if( substr($valor_validador,-1) ==  $ultimo_digito ){
			return true;
		}

		return false;	
    }

    

}
