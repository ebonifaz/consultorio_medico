<?php
defined('BASEPATH') or exit('No se permite acceso directo');

class BaseDatos{
    public $db;
    
    public function __construct()
    {
        try {
            $this->db = new mysqli(HOST,USER,PASSWORD,DB_NAME);
            $this->db->set_charset("utf8");
        } catch (\Throwable $th) {
            echo $th;
        }
        
        
        /* verificar la conexión */
        if (mysqli_connect_errno()) {
            printf("Falló la conexión failed: %s\n", $this->db->connect_error);
            exit();
        }
    }
    /**
    * Finaliza conexion
    */
    public function __destruct()
    {
      $this->db->close();
    }
}
    