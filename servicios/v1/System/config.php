<?php 
defined('BASEPATH') or exit('No se permite acceso directo');

// Valores de uri
define('URI', $_SERVER['REQUEST_URI']);
define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);
 
// Valores de rutas 

define('SEPARADOR_URL', '/');
define('FOLDER_PATH', SEPARADOR_URL . 'servicios' . SEPARADOR_URL . 'v1');
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('PATH_APP', FOLDER_PATH . SEPARADOR_URL ."App" . SEPARADOR_URL );
define('PATH_VIEWS', FOLDER_PATH . SEPARADOR_URL ."App". SEPARADOR_URL . "Views" . SEPARADOR_URL);
define('PATH_CONTROLLERS', "App" . SEPARADOR_URL . "Controllers" . SEPARADOR_URL);
define('PATH_MODEL', "App" . SEPARADOR_URL . "Model" . SEPARADOR_URL);
define('HELPER_PATH', 'System' . SEPARADOR_URL .'Helpers' . SEPARADOR_URL);
define('LIBS_ROUTE', ROOT . FOLDER_PATH . '/system/libs/');
define('PATH_ASSETS','assets'. SEPARADOR_URL );

define("PATH_DASHBOARD", FOLDER_PATH . SEPARADOR_URL . "dashboard");

// Limite separar contenido
define('LIMITE_PAGINACION',7);
define('MAX_ELEM_PAGINACION',7);
// Valores de core
define('CORE', 'System/Core/');
define('DEFAULT_CONTROLLER', 'Home');

// Valores de base de datos

define('HOST', 'localhost');
define('USER', 'root');
define('PASSWORD', 'root');
define('DB_NAME', 'proy_clinica_escuelita');

define('USER_EMAIL', 'USER_EMAIL');
define('USER_NICKNAME', 'USER_NICKNAME');
define('USER_ID', 'USER_ID');

define('USER_VISIBLE_NAME', USER_NICKNAME);



define('ID_ROL_PACIENTE', 19);
define('ID_ROL_DOCTOR', 20);

// Valores configuracion
define('ERROR_REPORTING_LEVEL', -1);