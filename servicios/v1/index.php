<?php 
define('BASEPATH', true);
 
require 'System/config.php';
require 'System/Core/autoload.php'; 
 
/*
* Nivel de notificaciones de errores
*/
error_reporting(ERROR_REPORTING_LEVEL);
date_default_timezone_set('America/Guayaquil');
function dd($variable)  
{
  echo "<pre>";
  print_r($variable);
  echo "</pre>";
  exit();
}

/*
* Revisa la ruta que deseas ingresar
*/
$router = new Router();
$controlador = $router->getController();
$metod = $router->getFuncion();
$parametros = $router->getParams();


/**
 * Validaciones e inclusión del controlador y el metodo 
 */
if(!CoreHelper::validateController($controlador)){
	$controlador = 'ErrorPage'; 
}
 
// Petición del controller requerido
require PATH_CONTROLLERS . "{$controlador}.php";

if(!CoreHelper::validateMethodController($controlador, $metod)){ 
	$controlador = 'ErrorPage';
	$metod = 'exec';
	// Petición del controller requerido
	// require PATH_CONTROLLERS . "{$controlador}.php";
}


$controller = new $controlador();
$controller->$metod($router, $parametros );
 